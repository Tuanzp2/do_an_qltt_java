package g1.quanlythuoctay.kinhdoanhkiemkho.Controller;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController.PhieuXuatKhoChiTietController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuXuatKhoModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class PhieuXuatKhoController implements Initializable {
    public TableView<PhieuXuatKhoModel> tbXuatKho;
    public TableColumn colMaPhieuXuat;
    public TableColumn colMaNhanVien;
    public TableColumn colMaKho;
    public TableColumn colNgayXuat;

    private final Repository repository = Repository.getInstance();
    public TextField txtPhieuXuatKho;
    public TextField txtMaKho;
    public DatePicker dpNgayXuatKho;
    public TextField txtSearch;
    public TextField txtMaNhanVien;
    private ObservableList<PhieuXuatKhoModel> phieuXuatKhoData;
    private static String maNhanVien;
    private static String maKho;

    public static void setMaKho(String maKho){
        PhieuXuatKhoController.maKho = maKho;
    }

    // setter maNhanVien
    public static void setMaNhanVien(String maNhanVien) {
    	PhieuXuatKhoController.maNhanVien = maNhanVien;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        txtMaNhanVien.setText(maNhanVien);
        txtMaKho.setText(maKho);


        try {
            phieuXuatKhoData = getAllData();



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        showOnTable();
    }

    private void showOnTable() {
        colMaPhieuXuat.setCellValueFactory(new PropertyValueFactory<>("maPhieuXuatKho"));
        colMaKho.setCellValueFactory(new PropertyValueFactory<>("maKho"));
        colMaNhanVien.setCellValueFactory(new PropertyValueFactory<>("maNhanVien"));
        colNgayXuat.setCellValueFactory(new PropertyValueFactory<>("ngayXuatKho"));
        tbXuatKho.setItems(phieuXuatKhoData);
    }


    private ObservableList<PhieuXuatKhoModel> getAllData() throws SQLException {
        String getAllDataSql =  "select * from PhieuXuatKho where is_deleted = '0'";
        ObservableList<PhieuXuatKhoModel> result = FXCollections.observableArrayList();
        ResultSet resultSet = repository.executeQuery(getAllDataSql);

        while (resultSet.next()){
            PhieuXuatKhoModel phieuXuatKhoModel = new PhieuXuatKhoModel(
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getDate(4).toLocalDate()
            );


            result.add(phieuXuatKhoModel);
        }
        return result;
    }


    public void onSelected(MouseEvent mouseEvent) {
        PhieuXuatKhoModel phieuXuatKhoSelected = tbXuatKho.getSelectionModel().getSelectedItem();

        txtPhieuXuatKho.setText(phieuXuatKhoSelected.getMaPhieuXuatKho());

        dpNgayXuatKho.setValue(phieuXuatKhoSelected.getNgayXuatKho());
    }

    public void onClickAdd(ActionEvent actionEvent) throws SQLException {
        String addSql = "insert into PhieuXuatKho(mapxh, manv,makho, ngayxuatkho) values('" + txtPhieuXuatKho.getText() + "','" + maNhanVien + "','" + maKho + "','" + dpNgayXuatKho.getValue() + "')";



        repository.executeVoid(addSql);

        phieuXuatKhoData = getAllData();

        tbXuatKho.setItems(phieuXuatKhoData);
    }

    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {
        String updateSql = "update PhieuXuatKho set MaNv = '" + maNhanVien +"', MaKho = '" + maKho + "', NgayXuatKho = '" + dpNgayXuatKho.getValue() + "'" + "where MaPXH='" + txtPhieuXuatKho.getText() +"'";

        repository.executeVoid(updateSql);

        phieuXuatKhoData = getAllData();
        tbXuatKho.setItems(phieuXuatKhoData);
    }

    public void onClickDelete(ActionEvent actionEvent) {
        if(AlterUtils.showConfirmation("Bạn có chắc chắn muốn xóa phiếu xuất kho này không?")){
            String deleteSql = "update PhieuXuatKho set is_deleted = '1' where MaPXH = '" + txtPhieuXuatKho.getText() + "'";
            try {
                repository.executeVoid(deleteSql);

                phieuXuatKhoData = getAllData();
                tbXuatKho.setItems(phieuXuatKhoData);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void search(KeyEvent keyEvent) {
        FilteredList<PhieuXuatKhoModel> filteredList = new FilteredList<>(phieuXuatKhoData, p -> true);

        filteredList.setPredicate(phieuXuatKhoModel -> {
            if(txtSearch.getText() == null || txtSearch.getText().isEmpty()){
                return true;
            }

            String lowerCaseFilter = txtSearch.getText().toLowerCase().trim();

            if(phieuXuatKhoModel.getMaPhieuXuatKho().toLowerCase().trim().contains(lowerCaseFilter)){
                return true;
            }else if(phieuXuatKhoModel.getMaKho().toLowerCase().trim().contains(lowerCaseFilter)){
                return true;
            }else return phieuXuatKhoModel.getMaNhanVien().toLowerCase().trim().contains(lowerCaseFilter);
        });

        tbXuatKho.setItems(filteredList);
    }

    public void onClickDetail(ActionEvent actionEvent) throws IOException {
        PhieuXuatKhoModel selected = tbXuatKho.getSelectionModel().getSelectedItem();
        if(selected == null){
            return;
        }
        PhieuXuatKhoChiTietController.setMaKho(selected.getMaKho());
        PhieuXuatKhoChiTietController.setMaPhieuXuatKho(selected.getMaPhieuXuatKho());
        System.out.println(selected.getMaPhieuXuatKho());
        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/SubFrm/PhieuXuatKhoChiTiet","Chi tiết phiếu xuất kho", false, Optional.empty());
    }
}
