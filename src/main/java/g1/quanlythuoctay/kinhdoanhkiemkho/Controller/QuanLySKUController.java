package g1.quanlythuoctay.kinhdoanhkiemkho.Controller;

import g1.quanlythuoctay.kinhdoanhkiemkho.Models.SKUSanPhamModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class QuanLySKUController implements Initializable {
    public TableView<SKUSanPhamModel> tbSKUSanPham;
    public TableColumn colMaSKU;
    public TableColumn colMaSanPham;
    public TextField txtMaSKU;
    public TextField txtDonVi;
    public TextField txtDonViGoc;
    public TextField txtQuyDoi;



    public TextField txtSearch;
    public TableColumn colDonVi;
    public TableColumn colDonViGoc;
    public TableColumn colQuyDoi;
    public ComboBox cbMaSanPham;

    private ObservableList<SKUSanPhamModel> data = FXCollections.observableArrayList();

    Repository repository = Repository.getInstance();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            showOnTable();
            data = getAllData();
            cbMaSanPham.setItems(getAllMaSanPham());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void showOnTable() throws SQLException {
        colMaSKU.setCellValueFactory(new PropertyValueFactory<>("maSKU"));
        colDonVi.setCellValueFactory(new PropertyValueFactory<>("donVi"));
        colDonViGoc.setCellValueFactory(new PropertyValueFactory<>("donViGoc"));
        colQuyDoi.setCellValueFactory(new PropertyValueFactory<>("quyDoi"));
        colMaSanPham.setCellValueFactory(new PropertyValueFactory<>("maSanPham"));

        tbSKUSanPham.setItems(getAllData());
    }

    private ObservableList<String> getAllMaSanPham() throws SQLException {
        ObservableList<String> data = FXCollections.observableArrayList();

        String sql = "select MaSP from SanPham";

        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {
            data.add(resultSet.getString(1));
        }
        return data;
    }

    private ObservableList<SKUSanPhamModel> getAllData() throws SQLException {
        ObservableList<SKUSanPhamModel> data = FXCollections.observableArrayList();

        String sql = "select * from SKUSanPham where is_deleted = '0'";

        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {
            SKUSanPhamModel skuSanPhamModel = new SKUSanPhamModel();
            skuSanPhamModel.setMaSKU(resultSet.getString(1));
            skuSanPhamModel.setDonVi(resultSet.getString(2));
            skuSanPhamModel.setDonViGoc(resultSet.getString(3));
            skuSanPhamModel.setQuyDoi(resultSet.getInt(4));
            skuSanPhamModel.setMaSanPham(resultSet.getString(5));
            data.add(skuSanPhamModel);
        }
        return data;
    }

    public void onSelected(MouseEvent mouseEvent) {
        SKUSanPhamModel selected = tbSKUSanPham.getSelectionModel().getSelectedItem();
        txtMaSKU.setText(selected.getMaSKU());
        txtDonVi.setText(String.valueOf(selected.getDonVi()));
        txtDonViGoc.setText(String.valueOf(selected.getDonViGoc()));
        txtQuyDoi.setText(String.valueOf(selected.getQuyDoi()));
        cbMaSanPham.setValue(selected.getMaSanPham());

    }

    public void onClickAdd(ActionEvent actionEvent) throws SQLException {
        String sql = "insert into SKUSanPham(masku, donvi, donvigoc, quydoi, MaSp) values ('" + txtMaSKU.getText() + "', '" + txtDonVi.getText() + "', '" + txtDonViGoc.getText() + "', '" + txtQuyDoi.getText() + "', '" + cbMaSanPham.getValue() + "')";

        repository.executeVoid(sql);

        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Thêm thành công");

        data = getAllData();
        tbSKUSanPham.setItems(data);
    }

    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {
        String oldMaSKU = tbSKUSanPham.getSelectionModel().getSelectedItem().getMaSKU();

        String sql = "update SKUSanPham set masku = '" + txtMaSKU.getText() + "', donvi = '" + txtDonVi.getText() + "', donvigoc = '" + txtDonViGoc.getText() + "', quydoi = '" + txtQuyDoi.getText() + "', MaSP = '" + cbMaSanPham.getValue() + "' where masku = '" + oldMaSKU + "'";
        repository.executeVoid(sql);
        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Sửa thành công");

        data = getAllData();
        tbSKUSanPham.setItems(data);


    }

    public void onClickDelete(ActionEvent actionEvent) throws SQLException {
        String maSKU = tbSKUSanPham.getSelectionModel().getSelectedItem().getMaSKU();

        String sql = "update SKUSanPham set is_deleted = '1' where masku = '" + maSKU + "'";

        if (AlterUtils.showConfirmation("Bạn có chắc chăn muốn xoá không?")) {
            repository.executeVoid(sql);
            AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Xoá thành công");
        }

        data = getAllData();
        tbSKUSanPham.setItems(data);


    }


    public void search(KeyEvent keyEvent) {
        FilteredList<SKUSanPhamModel> filteredList = new FilteredList<>(data, p -> true);

        filteredList.setPredicate(skuSanPhamModel -> {
            if (txtSearch.getText() == null || txtSearch.getText().isEmpty()) {
                return true;
            }

            String lowerCaseFilter = txtSearch.getText().toLowerCase();

            return skuSanPhamModel.getMaSKU().toLowerCase().contains(lowerCaseFilter);
        });

        tbSKUSanPham.setItems(filteredList);
    }
}
