package g1.quanlythuoctay.kinhdoanhkiemkho.Controller;

import g1.quanlythuoctay.kinhdoanhkiemkho.Models.TonKhoModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class QuanLyTonKhoController implements Initializable {
    public TableColumn<Integer, Integer> colStt;
    public TableColumn colMaSp;
    public TableColumn colSoLuong;
    public TableColumn colGiaNhapGoc;
    public TableColumn colGiaTonKho;
    public TableColumn colHSD;
    public TableColumn colThoiGianNhapKho;
    public TableView tbTonKho;

    private final Repository repository = Repository.getInstance();
    public TableColumn colMaPhieuNhapKho;
    private ObservableList<TonKhoModel> listTonKho;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            listTonKho = getAllData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        showOnTable();

    }

    private void showOnTable() {
        colStt.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(tbTonKho.getItems().indexOf(param.getValue()) + 1));

        colMaSp.setCellValueFactory(new PropertyValueFactory<>("maSanPham"));
        colSoLuong.setCellValueFactory(new PropertyValueFactory<>("soLuong"));
        colGiaNhapGoc.setCellValueFactory(new PropertyValueFactory<>("giaNhapGoc"));
        colGiaTonKho.setCellValueFactory(new PropertyValueFactory<>("giaTonKho"));
        colHSD.setCellValueFactory(new PropertyValueFactory<>("hanSuDung"));
        colThoiGianNhapKho.setCellValueFactory(new PropertyValueFactory<>("thoiGianNhapKho"));
        colMaPhieuNhapKho.setCellValueFactory(new PropertyValueFactory<>("maPhieuNhapKho"));

        tbTonKho.setItems(listTonKho);
    }


    private ObservableList<TonKhoModel> getAllData() throws SQLException {
        ObservableList<TonKhoModel> data = FXCollections.observableArrayList();

        String sql = "SELECT * FROM TonKho";
        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {
            TonKhoModel tonKho = new TonKhoModel(
                   resultSet.getString("MaSanPham"),
                    resultSet.getInt("SoLuong"),
                    resultSet.getFloat("GiaNhapGoc"),
                    resultSet.getDate("HanSuDung").toLocalDate(),
                    resultSet.getDate("ThoiGianNhapKho").toLocalDate(),
                    resultSet.getString("MaPhieuNhapKho")

            );

            data.add(tonKho);
        }

        return data;
    }
}
