package g1.quanlythuoctay.kinhdoanhkiemkho.Controller;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController.DonHangNhapChiTietController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuYeuCauNhapHangModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class PhieuYeuCauNhapHangController implements Initializable {

    public TableView<PhieuYeuCauNhapHangModel> tbPhieuYeuCauNhapHang;
    public TableColumn<Integer, Integer> colStt;
    public TableColumn colMaDonHangNhap;
    public TableColumn colMaNhanVien;
    public TableColumn colTrangThai;
    public TableColumn colNgay;

    private final Repository repository = Repository.getInstance();
    public TextField txtStt;
    public TextField txtMaDonHangNhap;

    public DatePicker dpNgay;
    public ComboBox<String> cbTrangThai;
    public Button btnAdd;
    public Button btnUpdate;
    public Button btnDelete;
    public TextField txtSearch;
    public TextField txtMaNhanVien;
    public ComboBox cbMaNhaCungUng;
    public TableColumn colMaNhaCungUng;
    private static String maNhanVien;
    private static String maKho;
    public Label lbTenCungUng;

    public static void setMaKho(String maKho) {
        PhieuYeuCauNhapHangController.maKho = maKho;
    }

    public static void setMaNhanVien(String maNhanVien) {
        PhieuYeuCauNhapHangController.maNhanVien = maNhanVien;
        System.out.println("ma nhan vien: " + maNhanVien);
    }

    private ObservableList<PhieuYeuCauNhapHangModel> phieuYeuCauNhapHangData = FXCollections.observableArrayList();
    private ObservableList<String> cbData = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnAdd.setVisible(false);
        cbData.addAll("Chua nhap", "Da nhap", "Dang nhap");


        cbTrangThai.setItems(cbData);
        cbTrangThai.setValue("Chua nhap");
        txtMaNhanVien.setText(maNhanVien);

        try {
            cbMaNhaCungUng.setItems(PhieuNhapKhoController.getMaNhaCungCap());
            phieuYeuCauNhapHangData = getAllData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        showOnTable();


    }

    private void showOnTable() {
        colStt.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(tbPhieuYeuCauNhapHang.getItems().indexOf(param.getValue()) + 1));
//        colStt.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(tbPhieuYeuCauNhapHang.getItems().indexOf(param) + 1));
        colMaDonHangNhap.setCellValueFactory(new PropertyValueFactory<>("maDonHangNhap"));
        colMaNhanVien.setCellValueFactory(new PropertyValueFactory<>("maNhanVien"));
        colTrangThai.setCellValueFactory(new PropertyValueFactory<>("trangThai"));
        colNgay.setCellValueFactory(new PropertyValueFactory<>("ngay"));
        colMaNhaCungUng.setCellValueFactory(new PropertyValueFactory<>("maNhaCungCap"));
        tbPhieuYeuCauNhapHang.setItems(phieuYeuCauNhapHangData);
    }

    private ObservableList<PhieuYeuCauNhapHangModel> getAllData() throws SQLException {
        ObservableList<PhieuYeuCauNhapHangModel> result = FXCollections.observableArrayList();
        String getAllDataSql = "Select * from phieuyeucaunhaphang where is_deleted = '0'";

        ResultSet resultSet = repository.executeQuery(getAllDataSql);
        while (resultSet.next()) {
            PhieuYeuCauNhapHangModel phieuYeuCauNhapHangModel = new PhieuYeuCauNhapHangModel(
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getString(3) == null ? "Chua nhap" : resultSet.getString(3),
                    resultSet.getDate(4).toLocalDate(),
                    resultSet.getString(6)
            );

            result.add(phieuYeuCauNhapHangModel);
        }

        return result;
    }


    public void onSelected(MouseEvent mouseEvent) {
        btnAdd.setVisible(false);
        PhieuYeuCauNhapHangModel selected = tbPhieuYeuCauNhapHang.getSelectionModel().getSelectedItem();

        String trangThai = selected.getTrangThai().toLowerCase().trim();


        txtStt.setText(String.valueOf(tbPhieuYeuCauNhapHang.getSelectionModel().getSelectedIndex() + 1));
        txtMaDonHangNhap.setText(selected.getMaDonHangNhap());
        cbTrangThai.setValue(selected.getTrangThai());
        dpNgay.setValue(selected.getNgay());

        if (trangThai.equalsIgnoreCase("da nhap")) {

//            btnAdd.setDisable(true);
            btnUpdate.setDisable(true);
            btnDelete.setDisable(true);
        } else {
//            btnAdd.setDisable(false);
            btnUpdate.setDisable(false);
            btnDelete.setDisable(false);
        }


    }


    public void onClickAdd(ActionEvent actionEvent) throws SQLException {

        String sql = "insert into phieuyeucaunhaphang(MaDonHangNhap, MaNv, TrangThai, Ngay, MaNhaCungUng) values ('" + txtMaDonHangNhap.getText() + "', '" + maNhanVien + "', '" + cbTrangThai.getValue() + "', '" + dpNgay.getValue() + "', '" + cbMaNhaCungUng.getValue() + "')";
        String insertPhieuNhapKhoSql = "insert into PhieuNhapKho(MaPhieuNhapKho, NgayLap, MaNV, MaPhieuYeuCauNhapHang, MaNhaCungUng, MaKho) values('MNK"+ getSoLuongMaNhapKho()+ "', '" + dpNgay.getValue() + "', '" + maNhanVien + "', '" + txtMaDonHangNhap.getText() + "', '" + cbMaNhaCungUng.getValue() + "','" + maKho + "')";
        repository.executeVoid(sql);
        repository.executeVoid(insertPhieuNhapKhoSql);
        System.out.println(insertPhieuNhapKhoSql);
        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Thêm thành công");

        phieuYeuCauNhapHangData = getAllData();
        tbPhieuYeuCauNhapHang.setItems(phieuYeuCauNhapHangData);

    }

    private String getSoLuongMaNhapKho() throws SQLException {
        String sql = "select count(*)+1 as Tong from PhieuNhapKho";

        ResultSet resultSet = repository.executeQuery(sql);
        resultSet.next();
        return resultSet.getString(1);
    }

    public Optional<Consumer<Void>> updateOnClose() throws SQLException { // khi dong dialog thi cap nhat lai bang
        return Optional.of(e -> {
            try {
                tbPhieuYeuCauNhapHang.setItems(getAllData());
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public void onClickDetail(ActionEvent actionEvent) throws IOException, SQLException {
        PhieuYeuCauNhapHangModel selected = (PhieuYeuCauNhapHangModel) tbPhieuYeuCauNhapHang.getSelectionModel().getSelectedItem(); // lay dong duoc chon (maDonHangNhap)
        if (selected == null) {
            return;
        }
        DonHangNhapChiTietController.setMaDonHangNhap(selected.getMaDonHangNhap());
        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/SubFrm/DonHangNhapChiTiet", "Chi tiết đơn hàng nhập", false, updateOnClose());
    }

    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {

        String oldMaDonHangNhap = tbPhieuYeuCauNhapHang.getSelectionModel().getSelectedItem().getMaDonHangNhap();

        // kiem tra xem ma don hang nhap co bi trung hay khong
        if (!oldMaDonHangNhap.equalsIgnoreCase(txtMaDonHangNhap.getText())) {
            if (checkIsExitedMaDonHangNhap(txtMaDonHangNhap.getText())) {
                AlterUtils.showAlert(Alert.AlertType.ERROR, "Lỗi", null, "Mã đơn hàng nhập đã tồn tại");
                return;
            }
        }

        String sql = "update phieuyeucaunhaphang set MaDonHangNhap = '" + txtMaDonHangNhap.getText() + "', MaNV = '" + maNhanVien + "', TrangThai = '" + cbTrangThai.getValue() + "', Ngay = '" + dpNgay.getValue() + "', MaNhaCungUng = '" + cbMaNhaCungUng.getValue()  +"' where MaDonHangNhap = '" + oldMaDonHangNhap + "'";
        System.out.println(sql);
        repository.executeVoid(sql);
        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Cập nhật thành công");
        System.out.println(sql);

        phieuYeuCauNhapHangData = getAllData();
        tbPhieuYeuCauNhapHang.setItems(phieuYeuCauNhapHangData);
    }

    public void onClickDelete(ActionEvent actionEvent) throws SQLException {
        String sql2 = "update phieunhapkho set is_deleted = '1' where maphieuyeucaunhaphang = '" + txtMaDonHangNhap.getText() +"'";

        String sql = "update phieuyeucaunhaphang set is_deleted = '1' where MaDonHangNhap = '" + txtMaDonHangNhap.getText() + "'";
        if (AlterUtils.showConfirmation("Bạn có chắc chắn muốn xoá?")) {
            repository.executeVoid(sql);
            repository.executeVoid(sql2);
            AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Xoá thành công");
        }

        phieuYeuCauNhapHangData = getAllData();
        tbPhieuYeuCauNhapHang.setItems(phieuYeuCauNhapHangData);


    }

    public void search(KeyEvent keyEvent) {
        String keyword = txtSearch.getText().toLowerCase().trim();
        FilteredList<PhieuYeuCauNhapHangModel> filteredList = new FilteredList<>(phieuYeuCauNhapHangData, p -> true);

        filteredList.setPredicate(phieuYeuCauNhapHangModel -> {
            if (keyword.isEmpty()) {
                return true;
            }
            if (phieuYeuCauNhapHangModel.getMaDonHangNhap().toLowerCase().contains(keyword)) {
                return true;
            }
            return phieuYeuCauNhapHangModel.getMaNhanVien().toLowerCase().contains(keyword);
        });

        tbPhieuYeuCauNhapHang.setItems(filteredList);
    }

    private String getSoLuongMaDonHangNhap() throws SQLException {
        String sql = "select count(*)+1 as Tong from phieuyeucaunhaphang";
        ResultSet resultSet = repository.executeQuery(sql);
        resultSet.next();
        return resultSet.getString(1);
    }

    private void clear() {
        txtStt.setText("");
        tbPhieuYeuCauNhapHang.getSelectionModel().clearSelection();
        cbTrangThai.setValue("Chua nhap");

    }

    public void onClickRefresh(ActionEvent actionEvent) throws SQLException {
        clear();
        btnAdd.setVisible(true);
        txtMaDonHangNhap.setText("DCT" + getSoLuongMaDonHangNhap());
        dpNgay.setValue(LocalDate.now());

    }

    private boolean checkIsExitedMaDonHangNhap(String maDonHangNhap) throws SQLException {
        String sql = "select * from phieuyeucaunhaphang where MaDonHangNhap = '" + maDonHangNhap + "'";
        ResultSet resultSet = repository.executeQuery(sql);
        return resultSet.next();
    }

    public void onSearch(KeyEvent keyEvent) {
        cbTrangThai.getEditor().textProperty().addListener((observableValue, s, t1) -> {
            FilteredList<String> filteredList = new FilteredList<>(cbData, p -> true);
            filteredList.setPredicate(s1 -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                return s1.toLowerCase().contains(t1.toLowerCase());
            });
            cbTrangThai.setItems(filteredList);
        });

        cbTrangThai.show();

    }


    public void onSelectCb(ActionEvent actionEvent) throws SQLException {
        String sql = "select TenNhaCungUng from NhaCungUng where MaNhaCungUng = '" + cbMaNhaCungUng.getValue() + "'";

        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {
            lbTenCungUng.setText(resultSet.getString(1));
        }
    }
}
