package g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController;

import g1.quanlythuoctay.LoginController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuNhapKhoChiTietModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuYeuCauNhapHangModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import net.sf.jasperreports.engine.export.ooxml.PptxBorderHelper;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

public class PhieuNhapKhoChiTietController implements Initializable {

    private static String maPhieuNhapKho;
    private static String maDonHangNhap;
    private static final Repository repository = Repository.getInstance();
    public TableView<PhieuNhapKhoChiTietModel> tbPhieuNhapKhoChiTiet;
    public TextField txtMaNhapKhoCT;

    public ComboBox cbMaSanPham;
    public TextField txtSoLuong;

    public TextField txtGiaNhap;
    public TableColumn<Integer, Integer> colMaNhapKhoCT;
    public TableColumn colMaPhieuNhap;
    public TableColumn colMaSanPham;

    public TableColumn colMaSKU;
    public TableColumn colGiaNhapKho;
    public TextField txtMaPhieuNhap;
    public TextField txtSearch;
    public TextField txtMaSKU;
    public TableColumn colSoLuongConLai;
    public TableColumn colSoLuongYeuCau;
    public TableColumn colSoLuongNhap;

    private ObservableList<PhieuNhapKhoChiTietModel> data;


    public static void setMaPhieuNhapKho(String maPhieuNhapKho) {
        PhieuNhapKhoChiTietController.maPhieuNhapKho = maPhieuNhapKho;
        System.out.println("maPhieuNhapKho = " + maPhieuNhapKho);
    }

    public static void setMaDonHangNhap(String maDonHangNhap) {
        PhieuNhapKhoChiTietController.maDonHangNhap = maDonHangNhap;
        System.out.println("maDonHangNhap = " + maDonHangNhap);
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        txtMaPhieuNhap.setText(maPhieuNhapKho);
        try {
            data = getAllData();
            showOnTable();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }


    private void showOnTable() throws SQLException {
//        colMaNhapKhoCT.setCellValueFactory(new PropertyValueFactory<>("maNhapChiTiet"));
        colMaNhapKhoCT.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(tbPhieuNhapKhoChiTiet.getItems().indexOf(param.getValue()) + 1));
        colMaPhieuNhap.setCellValueFactory(new PropertyValueFactory<>("maPhieuNhapKho"));
        colMaSanPham.setCellValueFactory(new PropertyValueFactory<>("maSanPham"));
        colMaSKU.setCellValueFactory(new PropertyValueFactory<>("maSKU"));
        colGiaNhapKho.setCellValueFactory(new PropertyValueFactory<>("giaNhap"));
        colSoLuongYeuCau.setCellValueFactory(new PropertyValueFactory<>("soLuongYeuCau"));
        colSoLuongNhap.setCellValueFactory(new PropertyValueFactory<>("soLuong"));
        tbPhieuNhapKhoChiTiet.setItems(data);

    }

    private ObservableList<PhieuNhapKhoChiTietModel> getAllData() throws SQLException {
        ObservableList<PhieuNhapKhoChiTietModel> data = FXCollections.observableArrayList();


        String sql = "SELECT\n" +
                "    distinct\n" +
                "    PNK.MaPhieuNhapKho,\n" +
                "    DHNCT.MaSP,\n" +
                "    DHNCT.MaSKU,\n" +
                "    DHNCT.GiaNhapKho,\n" +
                "    PNKCT.SoLuong as SoLuongNhap,\n" +
                "    DHNCT.Soluong as SoLuongYeuCau\n" +
                "FROM\n" +
                "    PhieuNhapKho PNK\n" +
                "        JOIN\n" +
                "    PhieuNhapKhoChiTiet PNKCT ON PNK.MaPhieuNhapKho = PNKCT.MaPhieuNhapKho\n" +
                "        JOIN\n" +
                "    PhieuYeuCauNhapHang PYCNH ON PNK.MaPhieuYeuCauNhapHang = PYCNH.MaDonHangNhap\n" +
                "        JOIN\n" +
                "    DonHangNhapChiTiet DHNCT ON PYCNH.MaDonHangNhap = DHNCT.MaDonHangNhap and PNKCT.MaSP = DHNCT.MaSP\n" +
                "        join\n" +
                "    SanPham SP on SP.MaSP = PNKCT.MaSP\n" +
                "WHERE\n" +
                "        PNK.MaPhieuNhapKho = '" + maPhieuNhapKho + "'\n" +
                "  AND PNK.is_deleted = '0'\n" +
                "  AND PNKCT.is_deleted = '0'\n" +
                "  AND PYCNH.is_deleted = '0'\n" +
                "  AND DHNCT.is_deleted = '0';\n";

        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {

            String maPhieuNhapKho = resultSet.getString("MaPhieuNhapKho");
            String maSp = resultSet.getString("MaSP");
            String maSKU = resultSet.getString("MaSKU");
            float giaNhap = resultSet.getFloat("GiaNhapKho");
            int soLuongNhap = resultSet.getInt("SoLuongNhap");
            int soLuongYeuCau = resultSet.getInt("SoLuongYeuCau");

            PhieuNhapKhoChiTietModel phieuNhapKho = new PhieuNhapKhoChiTietModel(maPhieuNhapKho, maSp, maSKU, soLuongNhap, giaNhap, soLuongYeuCau);
            data.add(phieuNhapKho);
        }

        return data;
    }

    public void onSelected(MouseEvent mouseEvent) {
        PhieuNhapKhoChiTietModel selected = tbPhieuNhapKhoChiTiet.getSelectionModel().getSelectedItem();

        cbMaSanPham.setValue(selected.getMaSanPham());
        txtMaSKU.setText(selected.getMaSKU());
        txtSoLuong.setText(String.valueOf(selected.getSoLuong()));
        txtGiaNhap.setText(String.valueOf(selected.getGiaNhap()));
    }


    private boolean checkNull() {
        if (txtMaNhapKhoCT.getText().isEmpty() || txtGiaNhap.getText().isEmpty() || txtSoLuong.getText().isEmpty() || cbMaSanPham.getValue() == null) {
            return true;
        }
        return false;
    }


    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {
        PhieuNhapKhoChiTietModel selected = tbPhieuNhapKhoChiTiet.getSelectionModel().getSelectedItem();

        String sql = "Update PhieuNhapKhoChiTiet set SoLuong = " + txtSoLuong.getText() + "where MaPhieuNhapKho = '" + selected.getMaPhieuNhapKho() + "' and " + "MaSP = '" + selected.getMaSanPham() + "'";

        if (checkSoLuongNhap()) {
            AlterUtils.showAlert(Alert.AlertType.ERROR, "Lỗi", null, "Số lượng nhập không được lớn hơn số lượng yêu cầu");
            return;
        }
        updateTrangThaiPhieuYeuCauNhap();

        repository.executeVoid(sql);
        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Cập nhật thành công");
        data = getAllData();
        tbPhieuNhapKhoChiTiet.setItems(data);

        if(findExistedByMaPhieuNhapKhoAndMaSanPham(selected.getMaPhieuNhapKho(), selected.getMaSanPham())) {
            updateTonKho(selected.getMaSanPham(), selected.getMaPhieuNhapKho());
        } else {
            insertTonKho(LoginController.idKho, selected.getMaSanPham(), txtSoLuong.getText(), txtGiaNhap.getText(), LocalDate.now(), LocalDate.now(), selected.getMaPhieuNhapKho());

        }

    }

    private void insertTonKho(String maKho, String maSanPham, String soLuong, String giaNhapGoc, LocalDate hanSuDung, LocalDate thoiGianNhapKho, String maPhieuNhapKho) throws SQLException {
        String sql = "insert into TonKho(MaKho, MaSanPham, SoLuong, GiaNhapGoc, HanSuDung, ThoiGianNhapKho, MaPhieuNhapKho) values("
                + "'" + maKho + "', "
                + "'" + maSanPham + "', "
                + "'" + soLuong + "', "
                + "'" + giaNhapGoc + "', "
                + "'" + hanSuDung + "', "
                + "'" + thoiGianNhapKho + "', "
                + "'" + maPhieuNhapKho + "')";


        repository.executeVoid(sql);
//        System.out.println(sql);
    }

    private boolean findExistedByMaPhieuNhapKhoAndMaSanPham(String maPhieuNhapKho, String maSanPham) throws SQLException {
        String sql = "select * from TonKho where MaPhieuNhapKho = '" + maPhieuNhapKho + "' and MaSanPham = '" + maSanPham + "'";
        ResultSet resultSet = repository.executeQuery(sql);
        return resultSet.next();
    }

    private void updateTonKho(String maSanPham, String maPhieuNhapKho){
        String sql = "update TonKho set SoLuong =" + txtSoLuong.getText() + " where MaSanPham = '" + maSanPham + "' and MaPhieuNhapKho = '" + maPhieuNhapKho + "'";
        repository.executeVoid(sql);
        System.out.println(sql);
    }

    private void updateTrangThaiPhieuYeuCauNhap() {
        PhieuNhapKhoChiTietModel selected = tbPhieuNhapKhoChiTiet.getSelectionModel().getSelectedItem();

        if (selected.getSoLuongYeuCau() > selected.getSoLuong()) {
            String sql = "Update PhieuYeuCauNhapHang set TrangThai = N'Đang nhập' where MaDonHangNhap = '" + maDonHangNhap + "'";
            repository.executeVoid(sql);
        } else if (selected.getSoLuongYeuCau() == selected.getSoLuong()) {
            String sql = "Update PhieuYeuCauNhapHang set TrangThai = N'Đã nhập' where MaDonHangNhap = '" + maDonHangNhap + "'";
            repository.executeVoid(sql);
        }

    }


    private boolean checkSoLuongNhap() {
        PhieuNhapKhoChiTietModel selected = tbPhieuNhapKhoChiTiet.getSelectionModel().getSelectedItem();
        return Integer.parseInt(txtSoLuong.getText()) > selected.getSoLuongYeuCau();
    }

    public void search(KeyEvent keyEvent) {
        FilteredList<PhieuNhapKhoChiTietModel> filteredList = new FilteredList<>(data, p -> true);

        filteredList.setPredicate(phieuNhapKhoChiTietModel -> {
            if (txtSearch.getText() == null || txtSearch.getText().isEmpty()) {
                return true;
            }

            String lowerCaseFilter = txtSearch.getText().toLowerCase();


            if (phieuNhapKhoChiTietModel.getMaPhieuNhapKho().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (phieuNhapKhoChiTietModel.getMaSanPham().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else return phieuNhapKhoChiTietModel.getMaSKU().toLowerCase().contains(lowerCaseFilter);
        });

        tbPhieuNhapKhoChiTiet.setItems(filteredList);
    }


}
