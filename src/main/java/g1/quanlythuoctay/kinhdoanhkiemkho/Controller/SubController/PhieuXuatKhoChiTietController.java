package g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController;

import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuNhapKhoChiTietModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuXuatKhoChiTietModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuXuatKhoModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class PhieuXuatKhoChiTietController implements Initializable {
    @FXML
    private TableView<PhieuXuatKhoChiTietModel> tbPhieuXuatKhoChiTiet;
    @FXML
    private TableColumn colMaSanPham;
    @FXML
    private TableColumn colSoLuong;
    @FXML
    private TableColumn colThanhTien;
    @FXML
    private ComboBox cbMaSanPham;
    @FXML
    private TextField txtSoLuong;
    @FXML
    private TextField txtMaPhieuXuatKho;
    @FXML
    private TableColumn colGiaNhapKho;
    @FXML
    private TextField txtSearch;
    @FXML
    private Button btnAdd;

    private static String maKho;
    private ObservableList<PhieuXuatKhoChiTietModel> data;
    private static final Repository repository = Repository.getInstance();
    private static String maPhieuXuatKho;

    public static void setMaKho(String maKho) {
        PhieuXuatKhoChiTietController.maKho = maKho;
    }

    public static void setMaPhieuXuatKho(String maPhieuXuatKho) {
        PhieuXuatKhoChiTietController.maPhieuXuatKho = maPhieuXuatKho;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        txtMaPhieuXuatKho.setText(maPhieuXuatKho);


        try {
            data = getAllData();
            cbMaSanPham.setItems(getAllMaSanPhamByMaKho());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        showOnTable();

    }

    private ObservableList<String> getAllMaSanPhamByMaKho() throws SQLException {
        ObservableList<String> result = FXCollections.observableArrayList();
        String sql = "Select distinct MaSanPham from TonKho where MaKho = '" + maKho + "'";
        ResultSet rs = repository.executeQuery(sql);

        while (rs.next()) {
            result.add(rs.getString(1));
        }

        return result;
    }


    private void showOnTable() {

        colMaSanPham.setCellValueFactory(new PropertyValueFactory<>("maSanPham"));
        colSoLuong.setCellValueFactory(new PropertyValueFactory<>("soLuong"));
        colThanhTien.setCellValueFactory(new PropertyValueFactory<>("thanhTien"));

        colGiaNhapKho.setCellValueFactory(new PropertyValueFactory<>("giaNhapKho"));

        tbPhieuXuatKhoChiTiet.setItems(data);
    }


    private ObservableList<PhieuXuatKhoChiTietModel> getAllData() throws SQLException {
        ObservableList<PhieuXuatKhoChiTietModel> result = FXCollections.observableArrayList();
        String sql = "select * from PhieuXuatKhoChiTiet where MaPhieuXuatKho = '" + maPhieuXuatKho + "' and is_deleted = '0'";

        ResultSet rs = repository.executeQuery(sql);

        while (rs.next()){
            String maSp = rs.getString("MaSP");
            String maPhieuXuatKho = rs.getString("MaPhieuXuatKho");
            int soLuong = rs.getInt("SoLuong");

            PhieuXuatKhoChiTietModel phieuXuatKhoChiTietModel = new PhieuXuatKhoChiTietModel(maSp, maPhieuXuatKho, soLuong);
            result.add(phieuXuatKhoChiTietModel);

        }


        return result;
    }

    public void onSelected(MouseEvent mouseEvent) {
        btnAdd.setVisible(true);
        PhieuXuatKhoChiTietModel selected = tbPhieuXuatKhoChiTiet.getSelectionModel().getSelectedItem();


        cbMaSanPham.setValue(selected.getMaSanPham());
        txtSoLuong.setText(String.valueOf(selected.getSoLuong()));


    }

    public void onClickAdd(ActionEvent actionEvent) throws SQLException {
        if (getSoLuongTonKho(cbMaSanPham.getValue().toString(), maKho) < Integer.parseInt(txtSoLuong.getText())) {
            AlterUtils.showAlert(Alert.AlertType.ERROR, "Lỗi", null, "Số lượng sản phẩm trong kho không đủ số lượng hiện tại: " + getSoLuongTonKho(cbMaSanPham.getValue().toString(), maKho) + " sản phẩm");
            return;
        }

        String sql = "insert into PhieuXuatKhoChiTiet(MaSP, MaPhieuXuatKho, SoLuong) values('" + cbMaSanPham.getValue().toString() + "', '" + maPhieuXuatKho + "', '" + txtSoLuong.getText() + "')";
        repository.executeVoid(sql);
        data = getAllData();
        tbPhieuXuatKhoChiTiet.setItems(data);
    }

    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {

        String oldMaPhieuXuatChiTiet = tbPhieuXuatKhoChiTiet.getSelectionModel().getSelectedItem().getMaPhieuXuatKhoChiTiet();

        String sql = "update PhieuXuatKhoChiTiet set MaSP = '" + cbMaSanPham.getValue().toString() + "', SoLuong = '" + txtSoLuong.getText() + "' where MaPhieuXuatChiTiet = '" + oldMaPhieuXuatChiTiet + "'";
        if (getSoLuongTonKho(cbMaSanPham.getValue().toString(), maKho) < Integer.parseInt(txtSoLuong.getText())) {
            AlterUtils.showAlert(Alert.AlertType.ERROR, "Lỗi", null, "Số lượng sản phẩm trong kho không đủ số lượng hiện tại: " + getSoLuongTonKho(cbMaSanPham.getValue().toString(), maKho) + " sản phẩm");
            return;
        }

        repository.executeVoid(sql);
        data = getAllData();
        tbPhieuXuatKhoChiTiet.setItems(data);
        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Cập nhật thành công");
    }

    public void onClickDelete(ActionEvent actionEvent) throws SQLException {
        String maPhieuXuatChiTiet = tbPhieuXuatKhoChiTiet.getSelectionModel().getSelectedItem().getMaPhieuXuatKhoChiTiet();

        if (AlterUtils.showConfirmation("Bạn có chắc chắn muốn xóa phiếu xuất kho chi tiết này không?")) {
            String sql = "update PhieuXuatKhoChiTiet set is_deleted = '1' where MaPhieuXuatChiTiet = '" + maPhieuXuatChiTiet + "'";
            repository.executeVoid(sql);

            data = getAllData();
            tbPhieuXuatKhoChiTiet.setItems(data);
            AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Xóa thành công");
        }

    }


    public void search(KeyEvent keyEvent) {
        String keyword = txtSearch.getText().toLowerCase().trim();
//        System.out.println(keyword);
        FilteredList<PhieuXuatKhoChiTietModel> filteredList = new FilteredList<>(data, p -> true);

        filteredList.setPredicate(phieuXuatKhoChiTietModel -> {
            if (keyword.isEmpty()) {
                return true;
            }

            if (phieuXuatKhoChiTietModel.getMaSanPham().toLowerCase().contains(keyword)) {
                return true;
            }

            if (phieuXuatKhoChiTietModel.getMaPhieuXuatKho().toLowerCase().contains(keyword)) {
                return true;
            }

            return phieuXuatKhoChiTietModel.getMaPhieuXuatKhoChiTiet().toLowerCase().contains(keyword);
        });

        tbPhieuXuatKhoChiTiet.setItems(filteredList);
    }

    private int getSoLuongTonKho(String maSanPham, String makho) throws SQLException {
        int result = 0;
//        String sql = "select PNKCT.SoLuong, PNKCT.MaSP from PhieuNhapKho PNK\n" +
//                "join PhieuNhapKhoChiTiet PNKCT on PNK.MaPhieuNhapKho = PNKCT.MaPhieuNhapKho\n" +
//                "where PNKCT.is_deleted = '0' and PNK.MaKho = '" + makho + "' and PNKCT.MaSP = '" + maSanPham + "'";
        String sql = "select SoLuong from TonKho where MaSanPham = '" + maSanPham + "' and MaKho = '" + makho + "'" + " and is_deleted = '0'";

        ResultSet rs = repository.executeQuery(sql);
        if (rs.next()) {
            result = rs.getInt(1);
        }
        return result;

    }

    private String getSoLuong() throws SQLException {
        String sql = "select count(*)+1 as Tong from PhieuXuatKhoChiTiet ";
        ResultSet rs = repository.executeQuery(sql);

        if (rs.next()) {
            return rs.getString(1);
        }

        return null;
    }

    public void onClickRefresh(ActionEvent actionEvent) throws SQLException {
        btnAdd.setVisible(true);
        txtSoLuong.clear();
        cbMaSanPham.getSelectionModel().clearSelection();
    }
}
