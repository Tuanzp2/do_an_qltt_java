package g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController;

import g1.quanlythuoctay.kinhdoanhkiemkho.Models.DonHangNhapChiTietModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.SKUSanPhamModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class DonHangNhapChiTietController implements Initializable {
    private static String maDonHangNhap;
    public TableView<DonHangNhapChiTietModel> tbChiTiet;
    public TableColumn<Integer, Integer> colMaDonHangNhapCT;
    public TableColumn colMaDonHangNhap;
    public TableColumn colMaSanPham;
    public TableColumn colSoLuong;
    public TextField txtMaDonHangNhapCT;
    public TextField txtMaDonHangNhap;

    public TextField txtSoLuong;
    public TextField txtSearch;
    public TableColumn colMaSKU;

    public TableColumn colVi;
    public TableColumn colVien;
    public TableColumn colHop;
    public Button btnAdd;
    public DatePicker dpHanSuDung;
    public TableColumn colHanSuDung;
    public TableColumn colGiaNhapKho;
    public TextField txtGiaNhapKho;
    public TextField txtMaSKU;
    public ComboBox cbTenSanPham;
    public Label lbMaSanPham;

    private ObservableList<DonHangNhapChiTietModel> donHangNhapChiTietData = FXCollections.observableArrayList();

    private final static Repository repository = Repository.getInstance();
    private SKUSanPhamModel sku;

    public SKUSanPhamModel getSku() {
        return sku;
    }

    public void setSku(SKUSanPhamModel sku) {
        this.sku = sku;
    }

    public static void setMaDonHangNhap(String maDonHangNhap) { // nhan maDonHangNhap
        DonHangNhapChiTietController.maDonHangNhap = maDonHangNhap;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        limitDatePicker();
        try {

            cbTenSanPham.setItems(getAllTenSanPham());
            donHangNhapChiTietData = getAllData();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println(maDonHangNhap);
        showOnTable();
        txtMaDonHangNhap.setText(maDonHangNhap);
    }

    private ObservableList<String> getMaSKU() throws SQLException {
        ObservableList<String> data = FXCollections.observableArrayList();
        String sql = "select MaSKU from SKUSanPham";

        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {
            String maSKU = resultSet.getString(1);
            data.add(maSKU);
        }

        return data;
    }

    public ObservableList<String> getMaSanPham() throws SQLException {
        ObservableList<String> data = FXCollections.observableArrayList();
        String sql = "select MaSP from SanPham";

        ResultSet resultSet = repository.executeQuery(sql);

        while (resultSet.next()) {
            String maSP = resultSet.getString(1);
            data.add(maSP);
        }

        return data;
    }

    private ObservableList<String> getAllTenSanPham() throws SQLException {
        ObservableList<String> result = FXCollections.observableArrayList();
        String sql = "select TenSp from SanPham";

        ResultSet rs = repository.executeQuery(sql);
        while (rs.next()) {
            result.add(rs.getString(1));
        }

        return result;

    }

    private void showOnTable() {
        colMaDonHangNhap.setCellValueFactory(new PropertyValueFactory<>("maDonHangNhap"));
        colMaDonHangNhapCT.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(tbChiTiet.getItems().indexOf(param.getValue()) + 1));
        colMaSanPham.setCellValueFactory(new PropertyValueFactory<>("maSanPham"));
        colSoLuong.setCellValueFactory(new PropertyValueFactory<>("soLuong"));
        colMaSKU.setCellValueFactory(new PropertyValueFactory<>("maSKU"));
        colHanSuDung.setCellValueFactory(new PropertyValueFactory<>("hanSuDung"));
//        colVi.setCellValueFactory(new PropertyValueFactory<SKUSanPhamModel,Integer>(""));
        colGiaNhapKho.setCellValueFactory(new PropertyValueFactory<>("giaNhapKho"));
        tbChiTiet.setItems(donHangNhapChiTietData);
    }


    private ObservableList<DonHangNhapChiTietModel> getAllData() throws SQLException {
        ObservableList<DonHangNhapChiTietModel> data = FXCollections.observableArrayList();

//        String sql = "select DHNCT.MaDHNCT, DHNCT.MaDonHangNhap, DHNCT.MaSP, DHNCT.Soluong, SKU.SoVi, SKU.SoVien, SKU.SoHop from DonHangNhapChiTiet DHNCT\n" +
//                "join SKUSanPham SKU on DHNCT.MaSKU = SKU.MaSKU WHERE DHNCT.maDonHangNhap = '" + maDonHangNhap + "'" + "And DHNCT.is_deleted = '0'";
        String sql = "SELECT * FROM DonHangNhapChiTiet WHERE maDonHangNhap = '" + maDonHangNhap + "'" + "And is_deleted = '0'";

        ResultSet resultSet = repository.executeQuery(sql);
        while (resultSet.next()) {
            String maDonHangNhapChiTiet = resultSet.getString(1);
            String maDonHangNhap = resultSet.getString(2);
            String maSanPham = resultSet.getString(3);
            int soLuong = resultSet.getInt(4);
            String maSKU = resultSet.getString("MaSKU");
            LocalDate hanSuDung = resultSet.getDate("HanSuDung").toLocalDate();
            float giaNhapKho = resultSet.getFloat("GiaNhapKho");

//            SKUSanPhamModel sku = new SKUSanPhamModel(maSKU, resultSet.getInt("SoVi"), resultSet.getInt("SoVien"), resultSet.getInt("SoHop"));

            data.add(new DonHangNhapChiTietModel(maDonHangNhapChiTiet, maDonHangNhap, maSanPham, soLuong, maSKU, hanSuDung, giaNhapKho));
        }

        return data;

    }

    public void onSelected(MouseEvent mouseEvent) { // hien thi len textfield khi nhap chuot chon 1 dong nao do
        btnAdd.setVisible(false);
        DonHangNhapChiTietModel donHangNhapChiTietModel = tbChiTiet.getSelectionModel().getSelectedItem();
        txtMaDonHangNhapCT.setText(donHangNhapChiTietModel.getMaDonHangNhapChiTiet());

        txtSoLuong.setText(String.valueOf(donHangNhapChiTietModel.getSoLuong()));
        txtGiaNhapKho.setText(String.valueOf(donHangNhapChiTietModel.getGiaNhapKho()));
        txtMaSKU.setText(donHangNhapChiTietModel.getMaSKU());
        dpHanSuDung.setValue(donHangNhapChiTietModel.getHanSuDung());
    }

    public void onClickAdd(ActionEvent actionEvent) throws SQLException {
        String sql = "INSERT INTO DonHangNhapChiTiet(MaDHNCT, MaDonHangNhap, MaSP, Soluong, masku, hansudung, GiaNhapKho) VALUES ('" +
                txtMaDonHangNhapCT.getText() + "', '" +
                txtMaDonHangNhap.getText() + "', '" +
                lbMaSanPham.getText() + "', " +
                txtSoLuong.getText() + ",'"
                +txtMaSKU.getText() + "','" +
                dpHanSuDung.getValue() + "'," +
                txtGiaNhapKho.getText() + ")";

        if(checkSanPhamExist(lbMaSanPham.getText().toString(), txtMaDonHangNhap.getText())){
            AlterUtils.showAlert(Alert.AlertType.ERROR, "Lỗi", null, "Sản phẩm đã tồn tại");
            return;
        }

        repository.executeVoid(sql);
        System.out.println(sql);
        AlterUtils.showAlert(Alert.AlertType.INFORMATION, "Thông báo", null, "Thêm thành công");
        donHangNhapChiTietData = getAllData();
        tbChiTiet.setItems(donHangNhapChiTietData);

        String getMaPhieuNhapKhoSql = "select Top 1 MaPhieuNhapKho from PhieuNhapKho order by MaPhieuNhapKho desc;";
        ResultSet rs = repository.executeQuery(getMaPhieuNhapKhoSql);

        if (rs.next()) {
            String maPhieuNhapKho = rs.getString(1);
            insertPhieuNhapKhoChiTiet(maPhieuNhapKho, lbMaSanPham.getText(), txtMaSKU.getText(), Float.parseFloat(txtGiaNhapKho.getText()));
        }

    }

    private boolean checkSanPhamExist(String maSanPham, String maDonNhap) throws SQLException {
       String sql = "select MaSP from DonHangNhapChiTiet where MaSP = '" + maSanPham + "' and MaDonHangNhap = '" + maDonNhap + "'";
         ResultSet rs = repository.executeQuery(sql);
        return rs.next();
    }

    private void insertPhieuNhapKhoChiTiet(String maPhieuNhapKho, String maSanPham, String maSKU, float giaNhapKho){
        String sql = "insert into PhieuNhapKhoChiTiet(MaPhieuNhapKho, MaSP, SoLuong, MaSKU, GiaNhapKho) values ('" +
                maPhieuNhapKho + "', '" +
                maSanPham + "', " +
                0 + ", '" +
                maSKU + "', " +
                giaNhapKho + ")";

        repository.executeVoid(sql);
        System.out.println("add");
    }

    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {
        String oldMaDonHangNhapCT = tbChiTiet.getSelectionModel().getSelectedItem().getMaDonHangNhapChiTiet();

        String sql = "UPDATE DonHangNhapChiTiet SET " +
                "MaDHNCT = '" + txtMaDonHangNhapCT.getText() + "', " +
                "MaSP = '" + lbMaSanPham.getText() + "', " +
                "SoLuong = " + txtSoLuong.getText() + ", " +
                "MaSKU = '" + txtMaSKU.getText() + "'," +
                "HanSuDung='" + dpHanSuDung.getValue() + "'," +
                "GiaNhapKho = " + txtGiaNhapKho.getText() +
                "WHERE MaDHNCT = '" + oldMaDonHangNhapCT + "'";

        repository.executeVoid(sql);

        donHangNhapChiTietData = getAllData();
        tbChiTiet.setItems(donHangNhapChiTietData);
    }

    public void onClickDelete(ActionEvent actionEvent) throws SQLException {
        String sql = "UPDATE DonHangNhapChiTiet SET is_deleted = '1' WHERE MaDHNCT = '" + txtMaDonHangNhapCT.getText() + "'";
        if (AlterUtils.showConfirmation("Bạn có chắc chắn muốn xóa?")) {
            repository.executeVoid(sql);
        }

        donHangNhapChiTietData = getAllData();
        tbChiTiet.setItems(donHangNhapChiTietData);
    }

    public void search(KeyEvent keyEvent) {
        String keyword = txtSearch.getText().toLowerCase().trim();
        FilteredList<DonHangNhapChiTietModel> filteredList = new FilteredList<>(donHangNhapChiTietData, p -> true);

        filteredList.setPredicate(donHangNhapChiTietModel -> {
            if (keyword.isEmpty()) {
                return true;
            }
            if (donHangNhapChiTietModel.getMaDonHangNhapChiTiet().toLowerCase().contains(keyword)) {
                return true;
            }
            if (donHangNhapChiTietModel.getMaDonHangNhap().toLowerCase().contains(keyword)) {
                return true;
            }
            return donHangNhapChiTietModel.getMaSanPham().toLowerCase().contains(keyword);
        });

        tbChiTiet.setItems(filteredList);


    }

    private String getSoLuong() throws SQLException {
        String sql = "select count(*)+1 as tong from DonHangNhapChiTiet ";

        ResultSet rs = repository.executeQuery(sql);

        if (rs.next()) {
            return rs.getString(1);
        }
        return null;
    }

    private void limitDatePicker() {
        dpHanSuDung.setDayCellFactory(datePicker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                LocalDate today = LocalDate.now();
                setDisable(date.isBefore(today));
            }
        });
    }

    public void onClickRefresh(ActionEvent actionEvent) throws SQLException {
        txtSoLuong.setText("");
        cbTenSanPham.setValue(null);
        txtMaSKU.setText("");
        btnAdd.setVisible(true);
        txtMaDonHangNhapCT.setText("M" + getSoLuong());
        dpHanSuDung.setValue(LocalDate.now());

    }

    public void onSearchCb(KeyEvent keyEvent) {
        cbTenSanPham.getEditor().textProperty().addListener((observableValue, s, t1) -> {
            FilteredList<String> filteredList = null;
            try {
                filteredList = new FilteredList<>(getAllTenSanPham(), p -> true);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            filteredList.setPredicate(s1 -> {
                if (t1 == null || t1.isEmpty()) {
                    return true;
                }
                return s1.toLowerCase().contains(t1.toLowerCase());
            });
            cbTenSanPham.setItems(filteredList);
        });

        cbTenSanPham.show();
    }

    public void selectMaSKU(ActionEvent actionEvent) {
        String tenSanPham = cbTenSanPham.getSelectionModel().getSelectedItem().toString();
        lbMaSanPham.setText(getMaSanPhamByTenSanPham(tenSanPham));

        String sqlSKU = "select MaSKU from SKUSanPham where MaSP = '" + lbMaSanPham.getText() + "'";
        try {
            ResultSet rs = repository.executeQuery(sqlSKU);
            if (rs.next()) {
                txtMaSKU.setText(rs.getString(1));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String getMaSanPhamByTenSanPham(String tenSanPham){
        String sql = "select MaSP from SanPham where TenSP = '" + tenSanPham + "'";
        try {
            ResultSet rs = repository.executeQuery(sql);
            if(rs.next()){
                return rs.getString(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}
