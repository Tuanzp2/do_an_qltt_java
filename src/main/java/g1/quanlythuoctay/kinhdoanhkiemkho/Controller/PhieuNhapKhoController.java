package g1.quanlythuoctay.kinhdoanhkiemkho.Controller;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController.PhieuNhapKhoChiTietController;
import g1.quanlythuoctay.kinhdoanhkiemkho.CustomList;
import g1.quanlythuoctay.kinhdoanhkiemkho.Models.PhieuNhapKhoModel;
import g1.quanlythuoctay.kinhdoanhkiemkho.Repository;
import g1.quanlythuoctay.kinhdoanhkiemkho.Utils.AlterUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class PhieuNhapKhoController implements Initializable {
    @FXML
    private TableColumn colMaPhieuNhap;
    public TableColumn colNgayLap;
    public TableColumn colMaNv;
    public TableColumn colMaPhieuYeuCau;
    public TableColumn colMaNhaCungCap;
    public TableColumn colMaKho;
    public TableView<PhieuNhapKhoModel> tbPhieuNhapKho;
    public TextField txtMaPhieuNhap;
    public DatePicker dpNgayLap;

    public ComboBox cbMaPhieuYeuCau;


    public TextField txtSearch;
    public TextField txtMaNhanVien, txtMaKho;
    public TextField txtMaNhaCungUng;

    private ObservableList<PhieuNhapKhoModel> phieuNhapKhoData;
    private static String maNhanVien;
    private static final Repository repository = Repository.getInstance();
    private static String maKho;

    public static void setMaKho(String maKho) {
        PhieuNhapKhoController.maKho = maKho;
    }

    public static void setMaNhanVien(String maNhanVien) {
        PhieuNhapKhoController.maNhanVien = maNhanVien;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        highLightNewOrder();
        txtMaKho.setText(maKho);
        txtMaNhanVien.setText(maNhanVien);

        try {
            System.out.println(getSTTCongNo());
            phieuNhapKhoData = getAllData();

            cbMaPhieuYeuCau.setItems(getMaPhieuYeuCau());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        showOnTable();
    }
    private void highLightNewOrder(){
        tbPhieuNhapKho.setRowFactory(tv -> new TableRow<PhieuNhapKhoModel>(){
            @Override
            protected void updateItem(PhieuNhapKhoModel item, boolean empty){
                super.updateItem(item, empty);
                if(item != null && item.getNgayLap().isEqual(LocalDate.now())){
                    setStyle("-fx-background-color: #AAB6FB;");
                }
            }
        });
    }

    private void showOnTable() {
        colMaPhieuNhap.setCellValueFactory(new PropertyValueFactory<>("maPhieuNhapKho"));
        colNgayLap.setCellValueFactory(new PropertyValueFactory<>("ngayLap"));
        colMaNv.setCellValueFactory(new PropertyValueFactory<>("maNhanVien"));
        colMaPhieuYeuCau.setCellValueFactory(new PropertyValueFactory<>("maPhieuYeuCauNhapHang"));
        colMaNhaCungCap.setCellValueFactory(new PropertyValueFactory<>("maNhaCungCap"));
        colMaKho.setCellValueFactory(new PropertyValueFactory<>("maKho"));
        tbPhieuNhapKho.setItems(phieuNhapKhoData);

    }

    private ObservableList<PhieuNhapKhoModel> getAllData() throws SQLException {
        ObservableList<PhieuNhapKhoModel> result = FXCollections.observableArrayList();
        String getAllDataSql = "Select * from phieunhapkho where is_deleted = '0';";

        try (ResultSet resultSet = repository.executeQuery(getAllDataSql)) {


            while (resultSet.next()) {
                PhieuNhapKhoModel phieuNhapKhoModel = new PhieuNhapKhoModel(
                        resultSet.getString(1),
                        resultSet.getDate(2).toLocalDate(),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                );

                result.add(phieuNhapKhoModel);
            }

            System.out.println(result.size());
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    public void onSelected(MouseEvent mouseEvent) throws SQLException {
        PhieuNhapKhoModel phieuNhapKhoSelected = tbPhieuNhapKho.getSelectionModel().getSelectedItem();

        txtMaPhieuNhap.setText(phieuNhapKhoSelected.getMaPhieuNhapKho());
        txtMaNhaCungUng.setText(phieuNhapKhoSelected.getMaNhaCungCap());
        txtMaKho.setText(phieuNhapKhoSelected.getMaKho());
        dpNgayLap.setValue(phieuNhapKhoSelected.getNgayLap());
        cbMaPhieuYeuCau.setValue(phieuNhapKhoSelected.getMaPhieuYeuCauNhapHang());
    }

    public static ObservableList<String> getMaNhanVien() throws SQLException {
        ObservableList<String> maNhanVien = FXCollections.observableArrayList();
        String getMaNhanVienSql = "Select MaNv from NhanVien";

        ResultSet resultSet = repository.executeQuery(getMaNhanVienSql);

        while (resultSet.next()) {
            maNhanVien.add(resultSet.getString(1));
        }

        return maNhanVien;

    }

    public static ObservableList<String> getMaPhieuYeuCau() throws SQLException {
        ObservableList<String> maPhieuYeuCau = FXCollections.observableArrayList();

        String getMaPhieuYeuCauSql = "Select MaDonHangNhap from PhieuYeuCauNhapHang where is_deleted = '0'";

        ResultSet resultSet = repository.executeQuery(getMaPhieuYeuCauSql);

        while (resultSet.next()) {
            maPhieuYeuCau.add(resultSet.getString(1));
        }

        return maPhieuYeuCau;
    }

    public static ObservableList<String> getMaNhaCungCap() throws SQLException {
        ObservableList<String> maNhaCungCap = FXCollections.observableArrayList();

        String getMaNhaCungCapSql = "Select MaNhaCungUng from NhaCungUng";

        ResultSet resultSet = repository.executeQuery(getMaNhaCungCapSql);

        while (resultSet.next()) {
            maNhaCungCap.add(resultSet.getString(1));
        }

        return maNhaCungCap;
    }


    public static ObservableList<String> getMaKho() throws SQLException {
        ObservableList<String> maKho = FXCollections.observableArrayList();

        String getMaKhoSql = "Select makho from KHO";

        ResultSet resultSet = repository.executeQuery(getMaKhoSql);

        while (resultSet.next()) {
            maKho.add(resultSet.getString(1));
        }

        return maKho;
    }

    private int getSTTCongNo() throws SQLException {
        int result = 1;
        String sql = "select count(*) as STT from CongNo";

        ResultSet rs = repository.executeQuery(sql);

        if (rs.next()) {
            result = rs.getInt("STT");
        }
        return result;
    }

    public void onClickAdd(ActionEvent actionEvent) throws SQLException {

        String insertData = "Insert into PhieuNhapKho(MaPhieuNhapKho, NgayLap, MaNV, MaPhieuYeuCauNhapHang, MaKho) values ('" + txtMaPhieuNhap.getText() + "','" + dpNgayLap.getValue() + "','" + maNhanVien + "','" + cbMaPhieuYeuCau.getValue() + "','" +  maKho + "')";
        System.out.println(insertData);

        repository.executeVoid(insertData);
        phieuNhapKhoData = getAllData();
        tbPhieuNhapKho.setItems(phieuNhapKhoData);

        String sql = "insert into CongNo(MaCongNo, TongNo, HanTra, MaNhaCungUng, TrangThai, MaPhieuNhapKho,)" +
                "select 'MCN" + getSTTCongNo() + "', null,null, MaNhaCungUng, null, '" + txtMaPhieuNhap.getText() + "' from PhieuNhapKho where MaPhieuNhapKho = '" + txtMaPhieuNhap.getText() + "'";
        repository.executeVoid(sql);
    }

    private LocalDate getNgayYeuCauNhapHangByMaPhieuYeuCau(String maPhieuYeuCau) throws SQLException {

        String sql = " select Ngay from PhieuYeuCauNhapHang where MaDonHangNhap = '" + maPhieuYeuCau + "'";

        ResultSet rs = repository.executeQuery(sql);

        if (rs.next()) {
            return rs.getDate(1).toLocalDate();
        }
        return null;
    }

    private void limitDatePicker(DatePicker datePicker, LocalDate fromDate) {
        datePicker.setDayCellFactory(picker -> new DateCell() {
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(date.isBefore(fromDate));
            }
        });
    }

    private CustomList<String> getAllMaSanPhamFromPhieuNhapKhoChiTiet(String maPhieuNhap) throws SQLException {
        CustomList<String> result = new CustomList<>();
        String sql = "select MaSP from PhieuNhapKhoChiTiet where MaPhieuNhapKho = '" + maPhieuNhap + "'";
        System.out.println(sql);
        ResultSet rs = repository.executeQuery(sql);

        while (rs.next()){
            result.add(rs.getString(1));
        }
        return result;

    }

    public void onClickUpdate(ActionEvent actionEvent) throws SQLException {

        String updateSql = "update PhieuNhapKho set NgayLap = '" + dpNgayLap.getValue() + "',MaNv = '" + maNhanVien + "', MaPhieuYeuCauNhapHang = '" + cbMaPhieuYeuCau.getValue() +  "', MaKho ='" + txtMaKho.getText() + "' where MaPhieuNhapKho = '" + txtMaPhieuNhap.getText() + "'";



        String updateTonKho = "Declare @MaSanPhamTable dbo.MaSanPhamTableType;\n" +
                "insert into @MaSanPhamTable(MaSanPham) values" + getAllMaSanPhamFromPhieuNhapKhoChiTiet(txtMaPhieuNhap.getText()) +";" +
                "Exec dbo.UpdateThoiGianNhapKho @MaSanPhamList = @MaSanPhamTable, @NgayCapNhat = '" + dpNgayLap.getValue() + "';";



        System.out.println(updateSql);
        repository.executeVoid(updateSql);
        repository.executeVoid(updateTonKho);

        phieuNhapKhoData = getAllData();
        tbPhieuNhapKho.setItems(phieuNhapKhoData);
    }

    public void onClickDelete(ActionEvent actionEvent) {
        if (AlterUtils.showConfirmation("Bạn có chắc chắn muốn xoá không?")) {
            String deleteSql = "update PhieuNhapKho set is_deleted = '1' where MaPhieuNhapKho = '" + txtMaPhieuNhap.getText() + "'";
            System.out.println(deleteSql);
            try {
                repository.executeVoid(deleteSql);
                phieuNhapKhoData = getAllData();
                tbPhieuNhapKho.setItems(phieuNhapKhoData);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Optional<Consumer<Void>> getOnCloseEvent() {
        //TODO
        return Optional.of(e -> {
            try {
                if(!getAllMaSanPhamFromPhieuNhapKhoChiTiet(txtMaPhieuNhap.getText()).isEmpty()){
                    tbPhieuNhapKho.setRowFactory(tv -> new TableRow<PhieuNhapKhoModel>(){
                        @Override
                        protected void updateItem(PhieuNhapKhoModel item, boolean empty){
                            super.updateItem(item, empty);
                                setStyle(null);
                        }
                    });
                }
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public void onClickDetail(ActionEvent actionEvent) throws IOException {
        String maPhieuNhapKho = tbPhieuNhapKho.getSelectionModel().getSelectedItem().getMaPhieuNhapKho();
        String maDonHangNhap = tbPhieuNhapKho.getSelectionModel().getSelectedItem().getMaPhieuYeuCauNhapHang();

        PhieuNhapKhoChiTietController.setMaPhieuNhapKho(maPhieuNhapKho);
        PhieuNhapKhoChiTietController.setMaDonHangNhap(maDonHangNhap);

        System.out.println("maPhieuNhapKho = " + maPhieuNhapKho);
        System.out.println("maDonHangNhap = " + maDonHangNhap);
        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/SubFrm/PhieuNhapKhoChiTiet", "Chi tiết phiếu nhập kho", false, Optional.empty());

    }

    public void search(KeyEvent keyEvent) {
        String keyword = txtSearch.getText().trim().toLowerCase();
        System.out.println(keyword);
        FilteredList<PhieuNhapKhoModel> filteredList = new FilteredList<>(phieuNhapKhoData, p -> true);

        filteredList.setPredicate(phieuNhapKhoModel -> {
            if (keyword.isEmpty()) {
                return true;
            }
            if (phieuNhapKhoModel.getMaKho().toLowerCase().contains(keyword)) {
                return true;
            }
            if (phieuNhapKhoModel.getMaPhieuNhapKho().toLowerCase().contains(keyword)) {
                return true;
            }
            return phieuNhapKhoModel.getMaNhanVien().toLowerCase().contains(keyword);
        });

        tbPhieuNhapKho.setItems(filteredList);


    }

    public void onSelectedMaPhieuYeuCau(ActionEvent actionEvent) throws SQLException {
        String maPhieuYeuCau = cbMaPhieuYeuCau.getValue().toString();
        LocalDate date = getNgayYeuCauNhapHangByMaPhieuYeuCau(maPhieuYeuCau);
        System.out.println(date);

        limitDatePicker(dpNgayLap,date);
    }
}
