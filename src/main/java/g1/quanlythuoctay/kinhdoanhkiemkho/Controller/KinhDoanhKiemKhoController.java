/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.kinhdoanhkiemkho.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import g1.quanlythuoctay.App;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class KinhDoanhKiemKhoController implements Initializable {

    public TabPane tabPane;

    public Menu menu;
    private List<Tab> tabs = new ArrayList<>();

    @FXML
    private AnchorPane panePhieuNhapKho;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        tabPane.getSelectionModel().select(0);
        loadTab(tabPane.getSelectionModel().getSelectedItem(), tabPane.getSelectionModel().getSelectedItem().getText());

        List<Tab> tabs = tabPane.getTabs();
        for (Tab tab : tabs) {
            tab.setOnSelectionChanged(event -> {
                if (tab.isSelected()) {
                    loadTab(tab, tab.getText());
                }
            });
        }

//        tab0 = tabPane.getTabs().get(0);
//        loadTab(tab0, "Phiếu yêu cầu nhập hàng");
//
//        tab1 = tabPane.getTabs().get(1);
//        loadTab(tab1, "Phiếu nhập kho");
//
//        tab2 = tabPane.getTabs().get(2);
//        loadTab(tab2, "Phiếu xuất kho");
//
//        Tab tab1 = tabPane.getTabs().get(0);
//        System.out.println(tab1.getText());
//        loadPane(tab1,"/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuYeuCauNhapHang.fxml");
//
//        Tab tab2 = tabPane.getTabs().get(1);
//        loadPane(tab2,"/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuNhapKho.fxml");
//
//        Tab tab3 = tabPane.getTabs().get(2);
//        loadPane(tab3, "/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuXuatKho.fxml");
    }

    @FXML
    private void handleBackToLoginAction(ActionEvent event) {
        try {
            // Load the login scene FXML
            Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

            // Get the current stage from the event source
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Set the login scene to the stage
            stage.setScene(new Scene(loginRoot));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            // handle exception
        }
    }


    private void loadTab(Tab tab, String name) {
        String frmPath = "";
        switch (name) {
            case "Phiếu yêu cầu nhập hàng":
                frmPath = "/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuYeuCauNhapHang.fxml";
                break;
            case "Phiếu nhập kho":
                frmPath = "/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuNhapKho.fxml";
                break;
            case "Phiếu xuất kho":
                frmPath = "/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuXuatKho.fxml";
                break;
            case "Quản lý SKU":
                frmPath = "/g1/quanlythuoctay/kinhdoanhkiemkho/QuanLySKU.fxml";
                break;
            case "Quản lý tồn kho":
                frmPath = "/g1/quanlythuoctay/kinhdoanhkiemkho/QuanLyTonKho.fxml";
                break;
            default:
                frmPath = "/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuYeuCauNhapHang.fxml";
                break;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource(frmPath));

        try {
            Parent root = loader.load();
            tab.setContent(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void openPhieuYeuCauNhapHang(ActionEvent actionEvent) throws IOException {
        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuYeuCauNhapHang", "Phiếu yêu cầu nhập hàng", true, Optional.empty());
    }

    public void openPhieuNhapKho(ActionEvent actionEvent) throws IOException {
        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuNhapKho", "Phiếu nhập kho", true, Optional.empty());

    }

    public void openPhieuXuatKho(ActionEvent actionEvent) throws IOException {
        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/PhieuXuatKho", "Phiếu xuất kho", true, Optional.empty());

    }

    public void openQuanLySKU(ActionEvent actionEvent) throws IOException {

        App.setRootPop("/g1/quanlythuoctay/kinhdoanhkiemkho/QuanLySKU", "Quản lý SKU", true, Optional.empty());
    }
}
