package g1.quanlythuoctay.kinhdoanhkiemkho.Models;


public class PhieuNhapKhoChiTietModel {
    private int maNhapChiTiet;
    private String maPhieuNhapKho;
    private String maSanPham;
    private String maSKU;
    private int soLuong;
    private float giaNhap;
    private int soLuongYeuCau;

    public PhieuNhapKhoChiTietModel(int maNhapChiTiet,String maPhieuNhapKho, String maSanPham, String maSKU, int soLuong, float giaNhap, int soLuongYeuCau) {
        this.maNhapChiTiet = maNhapChiTiet;
        this.maPhieuNhapKho = maPhieuNhapKho;
        this.maSanPham = maSanPham;
        this.maSKU = maSKU;
        this.soLuong = soLuong;
        this.giaNhap = giaNhap;
        this.soLuongYeuCau = soLuongYeuCau;
    }

    public PhieuNhapKhoChiTietModel(String maNhapChiTiet, String maPhieuNhapKho, String maSanPham, String maSKU, int soLuong, float giaNhap) {
        this.maNhapChiTiet = Integer.parseInt(maNhapChiTiet);
        this.maPhieuNhapKho = maPhieuNhapKho;
        this.maSanPham = maSanPham;
        this.maSKU = maSKU;
        this.soLuong = soLuong;
        this.giaNhap = giaNhap;
    }

    public PhieuNhapKhoChiTietModel(String maPhieuNhapKho, String maSanPham, String maSKU, int soLuong, float giaNhap, int soLuongYeuCau) {
        this.maPhieuNhapKho = maPhieuNhapKho;
        this.maSanPham = maSanPham;
        this.maSKU = maSKU;
        this.soLuong = soLuong;
        this.giaNhap = giaNhap;
        this.soLuongYeuCau = soLuongYeuCau;
    }

    public int getMaNhapChiTiet() {
        return maNhapChiTiet;
    }

    public void setMaNhapChiTiet(int maNhapChiTiet) {
        this.maNhapChiTiet = maNhapChiTiet;
    }

    public String getMaPhieuNhapKho() {
        return maPhieuNhapKho;
    }

    public void setMaPhieuNhapKho(String maPhieuNhapKho) {
        this.maPhieuNhapKho = maPhieuNhapKho;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public String getMaSKU() {
        return maSKU;
    }

    public void setMaSKU(String maSKU) {
        this.maSKU = maSKU;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public float getGiaNhap() {
        return giaNhap;
    }

    public void setGiaNhap(float giaNhap) {
        this.giaNhap = giaNhap;
    }

    public int getSoLuongYeuCau() {
        return soLuongYeuCau;
    }

    public void setSoLuongYeuCau(int soLuongYeuCau) {
        this.soLuongYeuCau = soLuongYeuCau;
    }
}
