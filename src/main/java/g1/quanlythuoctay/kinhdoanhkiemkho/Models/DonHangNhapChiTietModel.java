package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

import java.time.LocalDate;

public class DonHangNhapChiTietModel {
    private String maDonHangNhapChiTiet;
    private String maDonHangNhap;
    private String maSanPham;
    private int soLuong;
    private String maSKU;
    private LocalDate hanSuDung;
    private float GiaNhapKho;

    public float getGiaNhapKho() {
        return GiaNhapKho;
    }

    public void setGiaNhapKho(float giaNhapKho) {
        GiaNhapKho = giaNhapKho;
    }

    public DonHangNhapChiTietModel(String maDonHangNhapChiTiet, String maDonHangNhap, String maSanPham, int soLuong, String maSKU, LocalDate hanSuDung, float giaNhapKho) {
        this.maDonHangNhapChiTiet = maDonHangNhapChiTiet;
        this.maDonHangNhap = maDonHangNhap;
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.maSKU = maSKU;
        this.hanSuDung = hanSuDung;
        GiaNhapKho = giaNhapKho;
    }

    public LocalDate getHanSuDung() {
        return hanSuDung;
    }

    public void setHanSuDung(LocalDate hanSuDung) {
        this.hanSuDung = hanSuDung;
    }

    public DonHangNhapChiTietModel(String maDonHangNhapChiTiet, String maDonHangNhap, String maSanPham, int soLuong, String maSKU, SKUSanPhamModel skuSanPham) {
        this.maDonHangNhapChiTiet = maDonHangNhapChiTiet;
        this.maDonHangNhap = maDonHangNhap;
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.maSKU = maSKU;
        this.skuSanPham = skuSanPham;
    }

    public SKUSanPhamModel getSkuSanPham() {
        return skuSanPham;
    }

    public void setSkuSanPham(SKUSanPhamModel skuSanPham) {
        this.skuSanPham = skuSanPham;
    }

    private SKUSanPhamModel skuSanPham;

    public String getMaSKU() {
        return maSKU;
    }

    public void setMaSKU(String maSKU) {
        this.maSKU = maSKU;
    }

    public DonHangNhapChiTietModel(String maDonHangNhapChiTiet, String maDonHangNhap, String maSanPham, int soLuong, String maSKU, LocalDate hanSuDung) {
        this.maDonHangNhapChiTiet = maDonHangNhapChiTiet;
        this.maDonHangNhap = maDonHangNhap;
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.maSKU = maSKU;
        this.hanSuDung = hanSuDung;
    }

    public DonHangNhapChiTietModel(String maDonHangNhapChiTiet, String maDonHangNhap, String maSanPham, int soLuong) {
        this.maDonHangNhapChiTiet = maDonHangNhapChiTiet;
        this.maDonHangNhap = maDonHangNhap;
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
    }

    public String getMaDonHangNhapChiTiet() {
        return maDonHangNhapChiTiet;
    }

    public void setMaDonHangNhapChiTiet(String maDonHangNhapChiTiet) {
        this.maDonHangNhapChiTiet = maDonHangNhapChiTiet;
    }

    public String getMaDonHangNhap() {
        return maDonHangNhap;
    }

    public void setMaDonHangNhap(String maDonHangNhap) {
        this.maDonHangNhap = maDonHangNhap;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }
}
