package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

public class SKUSanPhamModel {
    private String maSKU;
    private String donVi;
    private String donViGoc;
    private int quyDoi;
    private String maSanPham;

    public SKUSanPhamModel() {
    }

    public SKUSanPhamModel(String maSKU, String donVi, String donViGoc, int quyDoi, String maSanPham) {
        this.maSKU = maSKU;
        this.donVi = donVi;
        this.donViGoc = donViGoc;
        this.quyDoi = quyDoi;
        this.maSanPham = maSanPham;
    }

    public String getMaSKU() {
        return maSKU;
    }

    public void setMaSKU(String maSKU) {
        this.maSKU = maSKU;
    }

    public String getDonVi() {
        return donVi;
    }

    public void setDonVi(String donVi) {
        this.donVi = donVi;
    }

    public String getDonViGoc() {
        return donViGoc;
    }

    public void setDonViGoc(String donViGoc) {
        this.donViGoc = donViGoc;
    }

    public int getQuyDoi() {
        return quyDoi;
    }

    public void setQuyDoi(int quyDoi) {
        this.quyDoi = quyDoi;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }
}
