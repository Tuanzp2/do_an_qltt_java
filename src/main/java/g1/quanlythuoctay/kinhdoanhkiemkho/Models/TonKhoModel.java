package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

import java.time.LocalDate;

public class TonKhoModel {
    private String maSanPham;
    private int soLuong;
    private float giaNhapGoc;
    private float giaTonKho;

    private LocalDate hanSuDung;
    private LocalDate thoiGianNhapKho;
    private String maPhieuNhapKho;



    public TonKhoModel(String maSanPham, int soLuong, float giaNhapGoc, LocalDate hanSuDung, LocalDate thoiGianNhapKho, String maPhieuNhapKho) {
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.giaNhapGoc = giaNhapGoc;

        this.hanSuDung = hanSuDung;
        this.thoiGianNhapKho = thoiGianNhapKho;
        this.maPhieuNhapKho = maPhieuNhapKho;
    }

    public TonKhoModel(String maSanPham, int soLuong, float giaNhapGoc, LocalDate hanSuDung, LocalDate thoiGianNhapKho) {
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.giaNhapGoc = giaNhapGoc;
        this.hanSuDung = hanSuDung;
        this.thoiGianNhapKho = thoiGianNhapKho;
    }


    public String getMaPhieuNhapKho() {
        return maPhieuNhapKho;
    }

    public void setMaPhieuNhapKho(String maPhieuNhapKho) {
        this.maPhieuNhapKho = maPhieuNhapKho;
    }
    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public float getGiaNhapGoc() {
        return giaNhapGoc;
    }

    public void setGiaNhapGoc(float giaNhapGoc) {
        this.giaNhapGoc = giaNhapGoc;
    }

    public float getGiaTonKho() {
        return soLuong * giaNhapGoc;
    }

    public void setGiaTonKho(float giaTonKho) {
        this.giaTonKho = giaTonKho;
    }

    public LocalDate getHanSuDung() {
        return hanSuDung;
    }

    public void setHanSuDung(LocalDate hanSuDung) {
        this.hanSuDung = hanSuDung;
    }

    public LocalDate getThoiGianNhapKho() {
        return thoiGianNhapKho;
    }

    public void setThoiGianNhapKho(LocalDate thoiGianNhapKho) {
        this.thoiGianNhapKho = thoiGianNhapKho;
    }
}
