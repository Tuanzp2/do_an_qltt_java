package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

import java.time.LocalDate;

public class PhieuYeuCauNhapHangModel {
    private String maDonHangNhap;
    private String maNhanVien;
    private String trangThai;
    private LocalDate ngay;
    private String maNhaCungCap;



    public PhieuYeuCauNhapHangModel(String maDonHangNhap, String maNhanVien, String trangThai, LocalDate ngay, String maNhaCungCap) {
        this.maDonHangNhap = maDonHangNhap;
        this.maNhanVien = maNhanVien;
        this.trangThai = trangThai;
        this.ngay = ngay;
        this.maNhaCungCap = maNhaCungCap;
    }

    public PhieuYeuCauNhapHangModel(String maDonHangNhap, String maNhanVien, String trangThai, LocalDate ngay) {
        this.maDonHangNhap = maDonHangNhap;
        this.maNhanVien = maNhanVien;
        this.trangThai = trangThai;
        this.ngay = ngay;
    }

    public String getMaDonHangNhap() {
        return maDonHangNhap;
    }

    public void setMaDonHangNhap(String maDonHangNhap) {
        this.maDonHangNhap = maDonHangNhap;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }

    public LocalDate getNgay() {
        return ngay;
    }

    public void setNgay(LocalDate ngay) {
        this.ngay = ngay;
    }
    public String getMaNhaCungCap() {
        return maNhaCungCap;
    }

    public void setMaNhaCungCap(String maNhaCungCap) {
        this.maNhaCungCap = maNhaCungCap;
    }
}
