package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

import java.time.LocalDate;

public class PhieuNhapKhoModel {
    private String maPhieuNhapKho;
    private LocalDate ngayLap;
    private String maNhanVien;
    private String maPhieuYeuCauNhapHang;
    private String maNhaCungCap;
    private String maKho;

    public PhieuNhapKhoModel(){}

    public PhieuNhapKhoModel(String maPhieuNhapKho, LocalDate ngayLap, String maNhanVien, String maPhieuYeuCauNhapHang, String maNhaCungCap, String maKho) {
        this.maPhieuNhapKho = maPhieuNhapKho;
        this.ngayLap = ngayLap;
        this.maNhanVien = maNhanVien;
        this.maPhieuYeuCauNhapHang = maPhieuYeuCauNhapHang;
        this.maNhaCungCap = maNhaCungCap;
        this.maKho = maKho;
    }

    public String getMaPhieuNhapKho() {
        return maPhieuNhapKho;
    }

    public void setMaPhieuNhapKho(String maPhieuNhapKho) {
        this.maPhieuNhapKho = maPhieuNhapKho;
    }

    public LocalDate getNgayLap() {
        return ngayLap;
    }

    public void setNgayLap(LocalDate ngayLap) {
        this.ngayLap = ngayLap;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public String getMaPhieuYeuCauNhapHang() {
        return maPhieuYeuCauNhapHang;
    }

    public void setMaPhieuYeuCauNhapHang(String maPhieuYeuCauNhapHang) {
        this.maPhieuYeuCauNhapHang = maPhieuYeuCauNhapHang;
    }

    public String getMaNhaCungCap() {
        return maNhaCungCap;
    }

    public void setMaNhaCungCap(String maNhaCungCap) {
        this.maNhaCungCap = maNhaCungCap;
    }

    public String getMaKho() {
        return maKho;
    }

    public void setMaKho(String maKho) {
        this.maKho = maKho;
    }

    @Override
    public String toString() {
        return "PhieuNhapKhoModel{" +
                "maPhieuNhapKho='" + maPhieuNhapKho + '\'' +
                ", ngayLap=" + ngayLap +
                ", maNhanVien='" + maNhanVien + '\'' +
                ", maPhieuYeuCauNhapHang='" + maPhieuYeuCauNhapHang + '\'' +
                ", maNhaCungCap='" + maNhaCungCap + '\'' +
                ", maKho='" + maKho + '\'' +
                '}';
    }
}
