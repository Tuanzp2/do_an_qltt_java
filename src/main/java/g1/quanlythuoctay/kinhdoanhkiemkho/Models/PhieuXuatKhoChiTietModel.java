package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

import java.time.LocalDate;

public class PhieuXuatKhoChiTietModel {
    private String maSanPham;
    private int soLuong;
    private int thanhTien;
    private String maPhieuXuatKho;
    private String maPhieuXuatKhoChiTiet;
    private float giaNhapKho;

    private String maSKU;
    private LocalDate hanSuDung;

    public PhieuXuatKhoChiTietModel(String maSanPham, String maPhieuXuatKho, int soLuong) {
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.maPhieuXuatKho = maPhieuXuatKho;
    }

    public PhieuXuatKhoChiTietModel(String maSanPham, int soLuong, int thanhTien, String maPhieuXuatKho, String maPhieuXuatKhoChiTiet, float giaNhapKho, String maSKU, LocalDate hanSuDung) {
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.thanhTien = thanhTien;
        this.maPhieuXuatKho = maPhieuXuatKho;
        this.maPhieuXuatKhoChiTiet = maPhieuXuatKhoChiTiet;
        this.giaNhapKho = giaNhapKho;
        this.maSKU = maSKU;
        this.hanSuDung = hanSuDung;
    }

    public PhieuXuatKhoChiTietModel(String maSanPham, int soLuong, int thanhTien, String maPhieuXuatKho, String maPhieuXuatKhoChiTiet) {
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.thanhTien = thanhTien;
        this.maPhieuXuatKho = maPhieuXuatKho;
        this.maPhieuXuatKhoChiTiet = maPhieuXuatKhoChiTiet;
    }

    public PhieuXuatKhoChiTietModel(String maSanPham, int soLuong, String maPhieuXuatKho, String maPhieuXuatKhoChiTiet) {
        this.maSanPham = maSanPham;
        this.soLuong = soLuong;
        this.maPhieuXuatKho = maPhieuXuatKho;
        this.maPhieuXuatKhoChiTiet = maPhieuXuatKhoChiTiet;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }

    public String getMaPhieuXuatKho() {
        return maPhieuXuatKho;
    }

    public void setMaPhieuXuatKho(String maPhieuXuatKho) {
        this.maPhieuXuatKho = maPhieuXuatKho;
    }

    public String getMaPhieuXuatKhoChiTiet() {
        return maPhieuXuatKhoChiTiet;
    }

    public void setMaPhieuXuatKhoChiTiet(String maPhieuXuatKhoChiTiet) {
        this.maPhieuXuatKhoChiTiet = maPhieuXuatKhoChiTiet;
    }
    public float getGiaNhapKho() {
        return giaNhapKho;
    }

    public void setGiaNhapKho(float giaNhapKho) {
        this.giaNhapKho = giaNhapKho;
    }

    public String getMaSKU() {
        return maSKU;
    }

    public void setMaSKU(String maSKU) {
        this.maSKU = maSKU;
    }

    public LocalDate getHanSuDung() {
        return hanSuDung;
    }

    public void setHanSuDung(LocalDate hanSuDung) {
        this.hanSuDung = hanSuDung;
    }

}
