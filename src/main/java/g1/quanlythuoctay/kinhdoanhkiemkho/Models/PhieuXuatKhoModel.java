package g1.quanlythuoctay.kinhdoanhkiemkho.Models;

import java.time.LocalDate;

public class PhieuXuatKhoModel{
    private String maPhieuXuatKho;
    private String maNhanVien;
    private String maKho;
    private LocalDate ngayXuatKho;

    public String getMaPhieuXuatKho() {
        return maPhieuXuatKho;
    }

    public void setMaPhieuXuatKho(String maPhieuXuatKho) {
        this.maPhieuXuatKho = maPhieuXuatKho;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public String getMaKho() {
        return maKho;
    }

    public void setMaKho(String maKho) {
        this.maKho = maKho;
    }

    public LocalDate getNgayXuatKho() {
        return ngayXuatKho;
    }

    public void setNgayXuatKho(LocalDate ngayXuatKho) {
        this.ngayXuatKho = ngayXuatKho;
    }

    public PhieuXuatKhoModel(String maPhieuXuatKho, String maNhanVien, String maKho, LocalDate ngayXuatKho) {
        this.maPhieuXuatKho = maPhieuXuatKho;
        this.maNhanVien = maNhanVien;
        this.maKho = maKho;
        this.ngayXuatKho = ngayXuatKho;
    }
}
