package g1.quanlythuoctay.kinhdoanhkiemkho;

import java.sql.*;
import java.util.Dictionary;
import java.util.ResourceBundle;

public class Repository {
    private static Repository instance;

//    private final static String jdbcUrl = "jdbc:sqlserver://localhost:1433;databaseName=QLTT;encrypt=true;trustServerCertificate=true;";
private final static String jdbcUrl = "jdbc:sqlserver://localhost:1433;databaseName=QLTT;encrypt=true;trustServerCertificate=true;";

//    private final static String username = "sa";
    private final static String username = "sa";
    private final static String password = "123";

    private Connection connection;


    private Repository(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection(jdbcUrl, username,password);
        } catch (ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }

    public static Repository getInstance(){
        if (instance == null){
            synchronized (Repository.class) {
                if (instance == null){
                    instance = new Repository();
                }
            }
        }
        return instance;
    }

    public ResultSet executeQuery(String sql) { // select
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            return resultSet;
        } catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


    public void executeVoid(String sql){
        try{
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
