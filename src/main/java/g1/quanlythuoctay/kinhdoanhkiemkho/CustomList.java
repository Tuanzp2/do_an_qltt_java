package g1.quanlythuoctay.kinhdoanhkiemkho;

import java.util.ArrayList;

public class CustomList<T> extends ArrayList<T> {
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < this.size(); i++){
            result.append("('").append(this.get(i)).append("')");
            if(i < this.size() - 1){
                result.append(", ");
            }
        }
        return result.toString();
    }
}
