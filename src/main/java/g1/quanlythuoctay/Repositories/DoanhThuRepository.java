/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.NhaCungUng;
import g1.quanlythuoctay.User;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashSet;

/**
 *
 * @author ADMIN
 */
public class DoanhThuRepository {

    private static DoanhThuRepository instance = null;

    private DoanhThuRepository() {
    }

    public static DoanhThuRepository Instance() {
        return instance == null ? instance = new DoanhThuRepository() : instance;
    }

    public LinkedHashSet<HoaDon> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        LinkedHashSet<HoaDon> ls = new LinkedHashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + HoaDon.TBL_HD + " JOIN " + User.TBL_User + " ON " + User.TBL_User + "." + User.idNV + " = " + HoaDon.TBL_HD + "." + HoaDon.ID_NV
            //                                    + " JOIN " + Kho.TBL_KHO + " ON " + Kho.TBL_KHO+ "." +Kho.id_Kho + " = " + User.TBL_User+'.'+User.idKho
            );
            // Now do something with the ResultSet ....
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString(HoaDon.ID_HD));
                hd.setSDT(rs.getString(HoaDon.phone_num));
                hd.setMaNV(rs.getString(HoaDon.ID_NV));
                hd.setNgayHoaDon(rs.getString(HoaDon.Date));
                hd.setMaKho(rs.getString(HoaDon.ID_Kho));
                hd.setTotal(rs.getDouble(HoaDon.Total_HD));
                hd.setTenNV(rs.getString(User.nameNV));
//                hd.setTenKho(rs.getString(Kho.name_Kho));
                ls.add(hd);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public LinkedHashSet<HoaDon> findNV() {
        Statement stmt = null;
        ResultSet rs = null;
        LinkedHashSet<HoaDon> ls = new LinkedHashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT " + User.nameNV + " , " + Kho.name_Kho + " , SUM(" + HoaDon.Total_HD + " ) as TongTien " + " FROM " + HoaDon.TBL_HD + " JOIN " + User.TBL_User + " ON " + User.TBL_User + "." + User.idNV + " = " + HoaDon.TBL_HD + "." + HoaDon.ID_NV
                    + " JOIN " + Kho.TBL_KHO + " ON " + Kho.TBL_KHO + "." + Kho.id_Kho + " = " + HoaDon.TBL_HD + '.' + HoaDon.ID_Kho
                    + " GROUP BY " + User.nameNV + " , " + Kho.name_Kho
            );
            // Now do something with the ResultSet ....
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setTotal(rs.getDouble("TongTien"));
                hd.setTenNV(rs.getString(User.nameNV));
                hd.setTenKho(rs.getString(Kho.name_Kho));
                ls.add(hd);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public LinkedHashSet<HoaDon> findSortNV() {
        LinkedHashSet<HoaDon> ls = new LinkedHashSet<>();

        try (Connection conn = DBConnection.Instance().DBConnect(); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery("SELECT " + User.nameNV + " , " + Kho.name_Kho + " , SUM(" + HoaDon.Total_HD + " ) as TongTien "
                + " FROM " + HoaDon.TBL_HD
                + " JOIN " + User.TBL_User + " ON " + User.TBL_User + "." + User.idNV + " = " + HoaDon.TBL_HD + "." + HoaDon.ID_NV
                + " JOIN " + Kho.TBL_KHO + " ON " + Kho.TBL_KHO + "." + Kho.id_Kho + " = " + HoaDon.TBL_HD + '.' + HoaDon.ID_Kho
                + " WHERE " + HoaDon.Date + " BETWEEN '" + HoaDon.dateStart + "' AND '" + HoaDon.dateEnd + "'"
                + " GROUP BY " + User.nameNV + " , " + Kho.name_Kho
        )) {

            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setNgayHoaDon(rs.getString(HoaDon.Date));
                hd.setTotal(rs.getDouble("TongTien"));
                hd.setTenNV(rs.getString(User.nameNV));
                hd.setTenKho(rs.getString(Kho.name_Kho));
                ls.add(hd);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            // log or handle the exception appropriately
        }

        return ls;
    }

}
