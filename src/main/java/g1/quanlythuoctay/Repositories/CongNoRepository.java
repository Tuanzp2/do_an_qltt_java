/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.CongNo;
import g1.quanlythuoctay.NhaCungUng;
import g1.quanlythuoctay.PhieuChi;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 *
 * @author ADMIN
 */
public class CongNoRepository {
    private static CongNoRepository instance = null;

    private CongNoRepository() {
    }

    public static CongNoRepository Instance() {
        return instance == null ? instance = new CongNoRepository() : instance;
    }

    public LinkedHashSet<CongNo> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        LinkedHashSet<CongNo> ls = new LinkedHashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + CongNo.TBL_CongNo+ " JOIN " + NhaCungUng.TBL_NCU + " ON " + CongNo.TBL_CongNo + "." + CongNo.id_NCU + " = " + NhaCungUng.TBL_NCU + "." + NhaCungUng.id_NCU
//                                    + " JOIN " + PhieuChi.TBL_PhieuChi + " ON " + PhieuChi.TBL_PhieuChi+ "." +PhieuChi.id_CongNo + " = " + CongNo.TBL_CongNo+'.'+CongNo.id_CongNo
                                    );
            // Now do something with the ResultSet ....
            while (rs.next()) {
                CongNo item = new CongNo();
                item.setMaCongNo(rs.getString(CongNo.id_CongNo));
                item.setTotalNo(rs.getDouble(CongNo.tienNo));
                item.setPayDeadline(rs.getString(CongNo.hanTra));
                item.setMaNCU(rs.getString(CongNo.id_NCU));
                item.setTenNCU(rs.getString(NhaCungUng.name_NCU));
                item.setTrangThai(rs.getInt(CongNo.trangthai));
                item.setMaPhieuKho(rs.getString(CongNo.id_phieuNhapKho));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
