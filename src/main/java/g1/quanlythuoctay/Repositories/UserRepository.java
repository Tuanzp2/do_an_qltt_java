/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.User;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class UserRepository {
    private static UserRepository instance = null;

    private UserRepository() {
    }

    public static UserRepository Instance() {
        return instance == null ? instance = new UserRepository() : instance;
    }

    public HashSet<User> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<User> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM NhanVien");

            // Now do something with the ResultSet ....
            while (rs.next()) {
                User nv = new User();
                nv.setMaNV(rs.getString(User.idNV));
                nv.setTenNV(rs.getString(User.nameNV));
                nv.setMaKho(rs.getString(User.idKho));
                nv.setUserName(rs.getString(User.usr));
                nv.setMatKhau(rs.getString(User.mk));
                nv.getMaca(rs.getInt(User.idCa));
                ls.add(nv);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
