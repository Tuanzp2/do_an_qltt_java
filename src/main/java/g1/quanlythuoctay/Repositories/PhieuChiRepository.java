/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.CongNo;
import g1.quanlythuoctay.PhieuChi;
import g1.quanlythuoctay.User;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashSet;

/**
 *
 * @author ADMIN
 */
public class PhieuChiRepository {

    private static PhieuChiRepository instance = null;

    private PhieuChiRepository() {
    }

    public static PhieuChiRepository Instance() {
        return instance == null ? instance = new PhieuChiRepository() : instance;
    }

    public LinkedHashSet<PhieuChi> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        LinkedHashSet<PhieuChi> ls = new LinkedHashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + PhieuChi.TBL_PhieuChi
                    + " INNER JOIN " + CongNo.TBL_CongNo + " ON " + PhieuChi.TBL_PhieuChi + "." + PhieuChi.id_CongNo + " = " + CongNo.TBL_CongNo + "." + CongNo.id_CongNo
                    + " INNER JOIN " + User.TBL_User + " ON " + PhieuChi.TBL_PhieuChi + "." + PhieuChi.id_NV + " = " + User.TBL_User + "." + User.idNV);

            // Now do something with the ResultSet ....
            while (rs.next()) {
                PhieuChi item = new PhieuChi();
                item.setMaPhieuChi(rs.getString(PhieuChi.id_PhieuChi));
                item.setMaCongNo(rs.getString(CongNo.id_CongNo));
                item.setSoTien(rs.getDouble(PhieuChi.soTienTra));
                item.setTienNo(rs.getDouble(CongNo.tienNo));
                item.setNote(rs.getString(PhieuChi.ghiChu));
                item.setMaNV(rs.getString(PhieuChi.id_NV));
                item.setThoiGian(rs.getString(PhieuChi.time));
                item.setNameNV(rs.getString(User.nameNV));
                item.setTienDaTra(rs.getDouble(CongNo.money_DaTra));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public void InsertBill(PhieuChi newBill) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;

            try {
                String strQuery = "INSERT INTO " + PhieuChi.TBL_PhieuChi + " ("
                        + PhieuChi.id_CongNo + ", "
                        + PhieuChi.soTienTra + ", "
                        + PhieuChi.id_NV + ", "
                        + PhieuChi.time + ", "
                        + PhieuChi.ghiChu
                        + ") "
                        + "VALUES (?, ?, ?, ?, ?)";
                stmt = conn.prepareStatement(strQuery);

                // Gán giá trị cho các tham số
//                stmt.setString(1, newMaHD);
                stmt.setString(1, newBill.getMaCongNo());
                stmt.setDouble(2, newBill.getSoTien());
                stmt.setString(3, newBill.getMaNV());
                stmt.setString(4, newBill.getThoiGian());
                stmt.setString(5, newBill.getNote());
                // Thực thi truy vấn
                int rowsAffected = stmt.executeUpdate();

                if (rowsAffected > 0) {
                    System.out.println("Thêm hóa đơn thành công.");
                } else {
                    System.out.println("Thêm hóa đơn không thành công.");
                }

//                return rowsAffected; // Trả về số dòng bị ảnh hưởng bởi truy vấn
            } catch (SQLException ex) {
                // Xử lý lỗi SQL
                ex.printStackTrace();
            } finally {
                // Đảm bảo kết nối và statement được đóng đúng cách
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (Exception e) {
            // Xử lý ngoại lệ khác (nếu có)
            e.printStackTrace();
        }

//        return 0; // Trả về 0 nếu có lỗi xảy ra
    }
}
