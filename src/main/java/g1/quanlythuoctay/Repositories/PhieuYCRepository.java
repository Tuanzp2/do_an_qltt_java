/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.CTPhieuYC;
import g1.quanlythuoctay.PhieuYCNhapKho;
import g1.quanlythuoctay.User;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class PhieuYCRepository {
    
    private static PhieuYCRepository instance = null;

    private PhieuYCRepository() {
    }

    public static PhieuYCRepository Instance() {
        return instance == null ? instance = new PhieuYCRepository() : instance;
    }

    public HashSet<PhieuYCNhapKho> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<PhieuYCNhapKho> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + PhieuYCNhapKho.TBL_PYCNHAPKHO 
//                    + " JOIN " +CTPhieuYC.TBL_CTPhieuYC + " ON " +PhieuYCNhapKho.TBL_PYCNHAPKHO+"."+PhieuYCNhapKho.Ma_Phieu+" = "+CTPhieuYC.TBL_CTPhieuYC+"."+CTPhieuYC.MaPhieuNhap
                    + " JOIN " +User.TBL_User + " ON " +PhieuYCNhapKho.TBL_PYCNHAPKHO+"."+PhieuYCNhapKho.id_NV+" = "+User.TBL_User+"."+User.idNV
            );

            // Now do something with the ResultSet ....
            while (rs.next()) {
                PhieuYCNhapKho item = new PhieuYCNhapKho();
                item.setId_MaYC(rs.getString(PhieuYCNhapKho.Ma_Phieu));
                item.setMaNV(rs.getString(PhieuYCNhapKho.id_NV));
                item.setNameNV(rs.getString(User.nameNV));
                item.setNgayLap(rs.getString(PhieuYCNhapKho.date));
                item.setNote(rs.getString(PhieuYCNhapKho.ghiChu));
                item.setTrangThai(rs.getInt(PhieuYCNhapKho.trang_thai));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
