/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.ChiTietHD;
import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.SanPham;
import g1.quanlythuoctay.TonKho;
import g1.quanlythuoctay.User;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class CTHoaDonRepository {
    private static CTHoaDonRepository instance = null;

    public CTHoaDonRepository() {
    }

    

    public static CTHoaDonRepository Instance() {
        return instance == null ? instance = new CTHoaDonRepository() : instance;
    }

    public HashSet<ChiTietHD> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<ChiTietHD> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM "+ChiTietHD.TBL_CTHD + " join " + SanPham.TBL_SP + " on " + ChiTietHD.TBL_CTHD+"."+ChiTietHD.ID_SP+" = " +SanPham.TBL_SP+"."+SanPham.Id_SP 
                    + " join " + HoaDon.TBL_HD + " on " + HoaDon.TBL_HD+"."+HoaDon.ID_HD +" = "+ChiTietHD.TBL_CTHD+"."+ChiTietHD.ID_HD
                    + " join " + User.TBL_User+ " on " + HoaDon.TBL_HD+"."+HoaDon.ID_NV +" = "+User.TBL_User+"."+User.idNV
                    + " join " + TonKho.TBL_TonKho+ " on " + TonKho.TBL_TonKho+"."+TonKho.ID_TonKho +" = "+ChiTietHD.TBL_CTHD+"."+ChiTietHD.ID_TK
                    + " join " + Kho.TBL_KHO+ " on " + Kho.TBL_KHO+"."+Kho.id_Kho +" = "+TonKho.TBL_TonKho+"."+TonKho.ID_Kho
            );

            // Now do something with the ResultSet ....
            while (rs.next()) {
                ChiTietHD cthd = new ChiTietHD();
                cthd.setMaChiTietHD(rs.getString(ChiTietHD.ID_CHITIETHD));
                cthd.setMaHD(rs.getString(ChiTietHD.ID_HD));
                cthd.setMaSP(rs.getString(ChiTietHD.ID_SP));
                cthd.setSLMua(rs.getInt(ChiTietHD.quantity_buy));
                cthd.setMaTK(rs.getString(ChiTietHD.ID_TK));
                cthd.setTenSP(rs.getString(SanPham.Name_SP));
                cthd.setTenNV(rs.getString(User.nameNV));
                cthd.setGiaGoc(rs.getDouble(SanPham.priceNhapKho));
                cthd.setMaKho(rs.getString(Kho.id_Kho));
                cthd.setNgayHD(rs.getString(HoaDon.Date));
                ls.add(cthd);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
