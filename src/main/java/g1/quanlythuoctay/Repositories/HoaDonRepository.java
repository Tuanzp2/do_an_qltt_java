/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 *
 * @author ADMIN
 */
public class HoaDonRepository {
    private static HoaDonRepository instance = null;

    public HoaDonRepository() {
    }

    

    public static HoaDonRepository Instance() {
        return instance == null ? instance = new HoaDonRepository() : instance;
    }

    public HashSet<HoaDon> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<HoaDon> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM "+HoaDon.TBL_HD + " join " + Kho.TBL_KHO + " on " + HoaDon.TBL_HD+"."+HoaDon.ID_Kho+" = " + Kho.TBL_KHO+"."+Kho.id_Kho );

            // Now do something with the ResultSet ....
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString(HoaDon.ID_HD));
                hd.setSDT(rs.getString(HoaDon.phone_num));
                hd.setMaNV(rs.getString(HoaDon.ID_NV));
                hd.setNgayHoaDon(rs.getString(HoaDon.Date));
                hd.setMaKho(rs.getString(HoaDon.ID_Kho));
                hd.setTotal(rs.getDouble(HoaDon.Total_HD));
                hd.setTenKho(rs.getString(Kho.name_Kho));
                ls.add(hd);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
    public LinkedHashSet<HoaDon> findAllSort() {
        Statement stmt = null;
        ResultSet rs = null;
        LinkedHashSet<HoaDon> ls = new LinkedHashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM "+HoaDon.TBL_HD + " join " + Kho.TBL_KHO + " on " + HoaDon.TBL_HD+"."+HoaDon.ID_Kho+" = " + Kho.TBL_KHO+"."+Kho.id_Kho 
                                    + " Order by " + HoaDon.column_sort + " "+HoaDon.Sort
                                    );

            // Now do something with the ResultSet ....
            while (rs.next()) {
                HoaDon hd = new HoaDon();
                hd.setMaHD(rs.getString(HoaDon.ID_HD));
                hd.setSDT(rs.getString(HoaDon.phone_num));
                hd.setMaNV(rs.getString(HoaDon.ID_NV));
                hd.setNgayHoaDon(rs.getString(HoaDon.Date));
                hd.setMaKho(rs.getString(HoaDon.ID_Kho));
                hd.setTotal(rs.getDouble(HoaDon.Total_HD));
                hd.setTenKho(rs.getString(Kho.name_Kho));
                ls.add(hd);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
