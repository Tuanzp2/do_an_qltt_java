/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.CTPhieuYC;
import g1.quanlythuoctay.PhieuYCNhapKho;
import g1.quanlythuoctay.SanPham;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class SanPhamRepository {

    private static SanPhamRepository instance = null;

    private SanPhamRepository() {
    }

    public static SanPhamRepository Instance() {
        return instance == null ? instance = new SanPhamRepository() : instance;
    }

    public HashSet<SanPham> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<SanPham> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + SanPham.TBL_SP);

            // Now do something with the ResultSet ....
            while (rs.next()) {
                SanPham item = new SanPham();
                item.setMaSP(rs.getString(SanPham.Id_SP));
                item.setTenSP(rs.getString(SanPham.Name_SP));
                item.setMaNhaCungUng(rs.getString(SanPham.ID_CungUng));
                item.setMaLoai(rs.getString(SanPham.ID_loai));
                item.setChiTiet(rs.getString(SanPham.detail));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public void InsertPhieu(PhieuYCNhapKho newBill) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;

            try {
                String strQuery = "INSERT INTO " + PhieuYCNhapKho.TBL_PYCNHAPKHO + " ( "
                        + PhieuYCNhapKho.id_NV + ", "
                        + PhieuYCNhapKho.date + ", "
                        + PhieuYCNhapKho.trang_thai + ", "
                        + PhieuYCNhapKho.ghiChu 
                        + " ) "
                        + "VALUES (?, ?, ?, ?)";
                stmt = conn.prepareStatement(strQuery);

                // Gán giá trị cho các tham số
//                stmt.setString(1, newMaHD);
                stmt.setString(1, newBill.getMaNV());
                stmt.setString(2, newBill.getNgayLap());
                stmt.setInt(3, newBill.getTrangThai());
                stmt.setString(4, newBill.getNote());
                // Thực thi truy vấn
                int rowsAffected = stmt.executeUpdate();

                if (rowsAffected > 0) {
                    System.out.println("Thêm hóa đơn thành công.");
                } else {
                    System.out.println("Thêm hóa đơn không thành công.");
                }

//                return rowsAffected; // Trả về số dòng bị ảnh hưởng bởi truy vấn
            } catch (SQLException ex) {
                // Xử lý lỗi SQL
                ex.printStackTrace();
            } finally {
                // Đảm bảo kết nối và statement được đóng đúng cách
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (Exception e) {
            // Xử lý ngoại lệ khác (nếu có)
            e.printStackTrace();
        }

//        return 0; // Trả về 0 nếu có lỗi xảy ra
    }

    public void InsertCTPhieu(CTPhieuYC newBill) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;

            try {
                String strQuery = "INSERT INTO " + CTPhieuYC.TBL_CTPhieuYC + " ( "
                        + CTPhieuYC.MaPhieuNhap + ", "
                        + CTPhieuYC.maSP + ", "
                        + CTPhieuYC.soluong 
                        + " ) "
                        + "VALUES (?, ?, ?)";
                stmt = conn.prepareStatement(strQuery);

                // Gán giá trị cho các tham số
//                stmt.setString(1, newMaHD);
                stmt.setString(1, newBill.getId_PhieuNhap());
                stmt.setString(2, newBill.getId_SP());
                stmt.setInt(3, newBill.getQuantity());
                // Thực thi truy vấn
                int rowsAffected = stmt.executeUpdate();

                if (rowsAffected > 0) {
                    System.out.println("Thêm hóa đơn thành công.");
                } else {
                    System.out.println("Thêm hóa đơn không thành công.");
                }

//                return rowsAffected; // Trả về số dòng bị ảnh hưởng bởi truy vấn
            } catch (SQLException ex) {
                // Xử lý lỗi SQL
                ex.printStackTrace();
            } finally {
                // Đảm bảo kết nối và statement được đóng đúng cách
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (Exception e) {
            // Xử lý ngoại lệ khác (nếu có)
            e.printStackTrace();
        }

//        return 0; // Trả về 0 nếu có lỗi xảy ra
    }
}
