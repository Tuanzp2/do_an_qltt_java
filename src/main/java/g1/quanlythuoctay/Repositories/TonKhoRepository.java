/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.TonKho;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javafx.scene.control.Alert;

/**
 *
 * @author ADMIN
 */
public class TonKhoRepository {
    private static TonKhoRepository instance = null;

    public TonKhoRepository() {
    }
    public static TonKhoRepository Instance() {
        return instance == null ? instance = new TonKhoRepository() : instance;
    }
    public HashSet<TonKho> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<TonKho> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + TonKho.TBL_TonKho);
            // Now do something with the ResultSet ....
            while (rs.next()) {
                TonKho item = new TonKho();
                item.setMaKhoTon(rs.getString(TonKho.ID_TonKho));
                item.setMaKho(rs.getString(TonKho.ID_Kho));
                item.setMaSP(rs.getString(TonKho.ID_SP));
                item.setMaPhieuNhapKho(rs.getString(TonKho.ID_PhieuNhapKho));
                item.setSoLuongMin(rs.getInt(TonKho.SL_Min));
                item.setSoLuong(rs.getInt(TonKho.SL));
                ls.add(item);

            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public void updateTonKho(Map<Integer, Map<Integer, String>> cellValues, List<String> maTonKhoList) {
        try (Connection conn = DBConnection.Instance().DBConnect()) {
            try {
                int i = 0; // Index for accessing maTonKhoList
                for (Map.Entry<Integer, Map<Integer, String>> entry : cellValues.entrySet()) {
                    int rowIndex = entry.getKey();
                    Map<Integer, String> columnValues = entry.getValue();
                    String maTonKho = maTonKhoList.get(i++); // Get maTonKho from the list
                    for (Map.Entry<Integer, String> columnEntry : columnValues.entrySet()) {
                        int columnIndex = columnEntry.getKey();
                        String value = columnEntry.getValue();
                        String columnName;

                        switch (columnIndex) {
                            case 1:
                                columnName = TonKho.ID_Kho;
                                break;
                            case 2:
                                columnName = TonKho.ID_SP;
                                break;
                            case 3:
                                columnName = TonKho.SL_Min;
                                break;
                            case 4:
                                columnName = TonKho.ID_PhieuNhapKho;
                                break;
                            case 5:
                                columnName = TonKho.SL;
                                break;
                            default:
                                // Handle the default case, if necessary
                                throw new IllegalArgumentException("Unsupported columnIndex: " + columnIndex);
                        }

                        System.out.println("Updating TonKho at Row " + rowIndex + ", Column " + columnIndex + " with value: " + value + " and MaTonKho: " + maTonKho);

                        String sql = "UPDATE " + TonKho.TBL_TonKho + " SET " + columnName + " = ? WHERE MaTonKho = ?";
                        try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
                            preparedStatement.setString(1, value);
                            preparedStatement.setString(2, maTonKho);
                            preparedStatement.executeUpdate();
                            // Thực hiện câu lệnh SQL UPDATE
                            int rowsUpdated = preparedStatement.executeUpdate();

                            if (rowsUpdated > 0) {
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setTitle("Success");
                                alert.setContentText("Update successful!");
                                alert.show();
                            } else {
                                System.out.println("Cập nhật không thành công.");
                            }
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
  
}
