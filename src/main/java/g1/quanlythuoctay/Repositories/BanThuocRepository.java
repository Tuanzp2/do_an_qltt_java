/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.ChiTietHD;
import g1.quanlythuoctay.HoaDon;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import g1.quanlythuoctay.util.DBConnection;
import g1.quanlythuoctay.TonKho;
import java.sql.PreparedStatement;
import java.util.Map;
import g1.quanlythuoctay.SanPham;
import java.sql.CallableStatement;

/**
 *
 * @author ADMIN
 */
public class BanThuocRepository {

    private static BanThuocRepository instance = null;

    private BanThuocRepository() {
    }

    public static BanThuocRepository Instance() {
        return instance == null ? instance = new BanThuocRepository() : instance;
    }

    public HashSet<SanPham> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<SanPham> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + SanPham.TBL_SP + " JOIN " + TonKho.TBL_TonKho + " ON " + SanPham.TBL_SP + "." + SanPham.Id_SP + " = " + TonKho.TBL_TonKho + "." + TonKho.ID_SP
                    + " ORDER BY " + SanPham.Name_SP);

            // Now do something with the ResultSet ....
            while (rs.next()) {
                SanPham item = new SanPham();
                item.setMaSP(rs.getString(SanPham.Id_SP));
                item.setTenSP(rs.getString(SanPham.Name_SP));
                item.setMaNhaCungUng(rs.getString(SanPham.ID_CungUng));
                item.setMaLoai(rs.getString(SanPham.ID_loai));
                item.setChiTiet(rs.getString(SanPham.detail));
                item.setGiaban(rs.getDouble(SanPham.price));
                item.setGiaNhapKho(rs.getDouble(SanPham.priceNhapKho));
                item.setSoluong(rs.getInt(TonKho.SL));
                item.setTinhtrang(getTinhTrang(item));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public String getTinhTrang(SanPham item) {
        CallableStatement cstmt = null;
        Connection conn = DBConnection.Instance().DBConnect();
        String tinhTrang = "";

        try {
            // Gọi stored procedure và truyền tham số
            cstmt = conn.prepareCall("{call TinhTrang(?)}");
            cstmt.setString(1, item.getMaSP());  // Sửa thành truyền MaSP thay vì SoLuong

            // Thực thi stored procedure
            boolean hasResults = cstmt.execute();

            // Nếu có kết quả, lấy ResultSet
            if (hasResults) {
                ResultSet rs = cstmt.getResultSet();

                // Xử lý ResultSet
                if (rs.next()) {
                    tinhTrang = rs.getString("TinhTrangResult");  // Sửa thành TinhTrangResult vì đổi tên cột trong stored procedure
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Đóng CallableStatement
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return tinhTrang;
    }

    public void BuyProducts(HashSet<SanPham> products, Map<String, Integer> quantities) {
        try (Connection conn = DBConnection.Instance().DBConnect()) {
            // Define the SQL query for updating the quantity
            String updateQuery = "UPDATE " + TonKho.TBL_TonKho + " SET " + TonKho.SL + " = " + TonKho.SL + " - ? WHERE " + TonKho.ID_SP + " = ?";

            // Prepare the statement with parameters
            try (PreparedStatement preparedStatement = conn.prepareStatement(updateQuery)) {
                for (SanPham product : products) {
                    // Lấy số lượng từ bản đồ quantities
                    int quantity = quantities.getOrDefault(product.getMaSP(), 0);

                    // Set the parameters
                    preparedStatement.setInt(1, quantity);  // Giả sử giảm số lượng theo lượng được chỉ định sau mỗi lần mua hàng
                    preparedStatement.setString(2, product.getMaSP());

                    // Execute the update
                    int rowsAffected = preparedStatement.executeUpdate();

                    if (rowsAffected > 0) {
                        System.out.println(updateQuery);
                        System.out.println("Đã cập nhật số lượng sản phẩm thành công cho sản phẩm có MaSP: " + product.getMaSP());
                    } else {
                        System.out.println("Không tìm thấy sản phẩm hoặc cập nhật số lượng thất bại cho sản phẩm có MaSP: " + product.getMaSP());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    

    public void InsertBill(HoaDon newBill) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;

            try {
                String strQuery = "INSERT INTO " + HoaDon.TBL_HD + " ("
                        + HoaDon.phone_num + ", "
                        + HoaDon.ID_NV + ", "
                        + HoaDon.Date + ", "
                        + HoaDon.ID_Kho + ", "
                        + HoaDon.Total_HD 
                        + ") "
                        + "VALUES (?, ?, ?, ?, ?)";
                stmt = conn.prepareStatement(strQuery);

                // Gán giá trị cho các tham số
//                stmt.setString(1, newMaHD);
                stmt.setString(1, newBill.getSDT());
                stmt.setString(2, newBill.getMaNV());
                stmt.setString(3, newBill.getNgayHoaDon());
                stmt.setString(4, newBill.getMaKho());
                stmt.setDouble(5, newBill.getTotal());
                // Thực thi truy vấn
                int rowsAffected = stmt.executeUpdate();

                if (rowsAffected > 0) {
                    System.out.println("Thêm hóa đơn thành công.");
                } else {
                    System.out.println("Thêm hóa đơn không thành công.");
                }

//                return rowsAffected; // Trả về số dòng bị ảnh hưởng bởi truy vấn
            } catch (SQLException ex) {
                // Xử lý lỗi SQL
                ex.printStackTrace();
            } finally {
                // Đảm bảo kết nối và statement được đóng đúng cách
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (Exception e) {
            // Xử lý ngoại lệ khác (nếu có)
            e.printStackTrace();
        }

//        return 0; // Trả về 0 nếu có lỗi xảy ra
    }

    public void InsertBillDetail(ChiTietHD newBill) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;

            try {
                String strQuery = "INSERT INTO " + ChiTietHD.TBL_CTHD + " ("
                        + ChiTietHD.ID_HD + ", "
                        + ChiTietHD.ID_SP + ", "
                        + ChiTietHD.ID_TK + ", "
                        + ChiTietHD.quantity_buy
                        + ") "
                        + "VALUES (?, ?, ?, ?)";
                stmt = conn.prepareStatement(strQuery);

                stmt.setString(1, newBill.getMaHD());
                stmt.setString(2, newBill.getMaSP());
                stmt.setString(3, newBill.getMaTK());
                stmt.setInt(4, newBill.getSLMua());
                
                // Thực thi truy vấn
                int rowsAffected = stmt.executeUpdate();
                  
                if (rowsAffected > 0) {
                    System.out.println("Thêm chi tiết hóa đơn thành công.");
                } else {
                    System.out.println("Thêm chi tiết hóa đơn không thành công.");
                }

//                return rowsAffected; // Trả về số dòng bị ảnh hưởng bởi truy vấn
            } catch (SQLException ex) {
                // Xử lý lỗi SQL
                ex.printStackTrace();
            } finally {
                // Đảm bảo kết nối và statement được đóng đúng cách
                if (stmt != null) {
                    stmt.close();
                }
            }
        } catch (Exception e) {
            // Xử lý ngoại lệ khác (nếu có)
            e.printStackTrace();
        }

//        return 0; // Trả về 0 nếu có lỗi xảy ra
    }

}
