/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.Repositories;

import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class KhoRepository {
    private static KhoRepository instance = null;

    public KhoRepository() {
    }

    

    public static KhoRepository Instance() {
        return instance == null ? instance = new KhoRepository() : instance;
    }

    public HashSet<Kho> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<Kho> ls = new HashSet<>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM KHO");

            // Now do something with the ResultSet ....
            while (rs.next()) {
                Kho kh = new Kho();
                kh.setMaKho(rs.getString(Kho.id_Kho));
                kh.setTenKho(rs.getString(Kho.name_Kho));
                kh.setKhoTong(rs.getInt(Kho.tong_Kho));

                ls.add(kh);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
