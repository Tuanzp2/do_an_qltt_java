/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class Kho {



    private String MaKho;
    private String TenKho;
    private int KhoTong;
    public static String TBL_KHO="KHO";
    public static String id_Kho = "MaKho";
    public static String name_Kho = "TenKho";
    public static String tong_Kho = "KhoTong";

    public Kho() {
    }

    public Kho(String MaKho, String TenKho, int KhoTong) {
        this.MaKho = MaKho;
        this.TenKho = TenKho;
        this.KhoTong = KhoTong;
    }
    /**
     * @return the MaKho
     */
    public String getMaKho() {
        return MaKho;
    }

    /**
     * @param MaKho the MaKho to set
     */
    public void setMaKho(String MaKho) {
        this.MaKho = MaKho;
    }

    /**
     * @return the TenKho
     */
    public String getTenKho() {
        return TenKho;
    }

    /**
     * @param TenKho the TenKho to set
     */
    public void setTenKho(String TenKho) {
        this.TenKho = TenKho;
    }

    /**
     * @return the KhoTong
     */
        /**
     * @return the KhoTong
     */
    public int getKhoTong() {
        return KhoTong;
    }

    /**
     * @param KhoTong the KhoTong to set
     */
    public void setKhoTong(int KhoTong) {
        this.KhoTong = KhoTong;
    }
}
