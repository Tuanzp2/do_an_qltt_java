/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.control.ComboBox;

/**
 *
 * @author khiemle
 */
public class MyComboBox2 extends ComboBox {

    HashSet _datasource = null;
    private LinkedHashSet<String> _DisplayText = new LinkedHashSet<>();
    private Object selectedSort;
    String _title = "";
    String _DisplayProperty = "";//ten field se duoc hien thi
    String _ValueProperty = "";//ten field se duoc lay data

    public MyComboBox2() {

    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getTitle() {
        return _title;
    }

    public HashSet getDatasource() {
        return _datasource;
    }

    public void setDatasource(HashSet _datasource) {
        this._datasource = _datasource;
    }

    public void setDisplayProperty(String _DisplayProperty) {
        this._DisplayProperty = _DisplayProperty;
    }

    public String getDisplayProperty() {
        return _DisplayProperty;
    }

    public void setValueProperty(String _ValueProperty) {
        this._ValueProperty = _ValueProperty;
    }

    public String getValueProperty() {
        return _ValueProperty;
    }

    /**
     * @return the _DisplayText
     */
    public HashSet<String> getDisplayText() {
        return _DisplayText;
    }

    /**
     * @param _DisplayText the _DisplayText to set
     */
    public void setDisplayText(LinkedHashSet<String> _DisplayText) {
        this._DisplayText = _DisplayText;
    }

//    private void setList(HashSet<Object> ls) {
//        setItems(FXCollections.observableArrayList(ls));
//    }
    /**
     * @return the selectedSort
     */
    public Object getSelectedSort() {
        return selectedSort;
    }

    /**
     * @param selectedSort the selectedSort to set
     */
    public void setSelectedSort(Object selectedSort) {
        this.selectedSort = selectedSort;
    }

    public void tx() {
        this.getItems().add(_title);
        for (Object l : getDisplayText()) {
            try {
                this.getItems().add(l);
            } catch (Exception e) {
            }
        }

        // Thêm sự kiện lắng nghe cho sự kiện thay đổi lựa chọn
        this.setOnAction(event -> handleComboBoxSelection());

        this.getSelectionModel().select(0);
    }
    public void handleComboBoxSelection() {
        Object selectedValue = this.getValue();
        System.out.println("Selected Value: " + selectedValue);

        // Gán giá trị cho selectedSort
        setSelectedSort(selectedValue);
    }

    public void Bind() {
        this.getItems().add(_title);
        for (Object l : getDatasource()) {
            Class t = l.getClass();
            try {
                Method method = t.getMethod("get" + _DisplayProperty, null);
                Object obj = method.invoke(l, null);
                this.getItems().add(obj);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.getSelectionModel().select(0);
    }

    public Object selectedValue() {
        int index = this.getSelectionModel().getSelectedIndex();
        int currentIndex = 0;
        if (index != 0) {
            for (Object obj : _datasource) {
                if (currentIndex == index - 1) {
                    Class t = obj.getClass();
                    try {
                        Method method = t.getMethod("get" + _ValueProperty, null);
                        Object key = method.invoke(obj, null);
                        return key;
                    } catch (NoSuchMethodException ex) {
                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SecurityException ex) {
                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                currentIndex++;
            }
        }
        return -1;
    }

}
