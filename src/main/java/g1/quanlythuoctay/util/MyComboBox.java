/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.control.ComboBox;

/**
 *
 * @author khiemle
 */
public class MyComboBox extends ComboBox {

    HashSet _datasource = null;

    String _title = "";
    String _DisplayProperty = "";//ten field se duoc hien thi
    String _ValueProperty = "";//ten field se duoc lay data

    public MyComboBox() {

    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public String getTitle() {
        return _title;
    }

    public HashSet getDatasource() {
        return _datasource;
    }

    public void setDatasource(HashSet _datasource) {
        this._datasource = _datasource;
    }

    public void setDisplayProperty(String _DisplayProperty) {
        this._DisplayProperty = _DisplayProperty;
    }

    public String getDisplayProperty() {
        return _DisplayProperty;
    }

    public void setValueProperty(String _ValueProperty) {
        this._ValueProperty = _ValueProperty;
    }

    public String getValueProperty() {
        return _ValueProperty;
    }

//    private void setList(HashSet<Object> ls) {
//        setItems(FXCollections.observableArrayList(ls));
//    }
    public void Bind() {
        
        this.getItems().clear(); // Clear existing items
    this.getItems().add(_title); // Add title
    for (Object l : getDatasource()) {
        this.getItems().add(l); // Add each item from the datasource
    }
    this.getSelectionModel().select(0);
        
//        this.getItems().add(_title);
//        for (Object l : getDatasource()) {
//             this.getItems().add(l);// this to add the address to the combo
//            Class t = l.getClass();
//            try {
//                Method method = t.getMethod("get" + _DisplayProperty, null);
//               
//                Object obj = method.invoke(l, null);
////                this.getItems().add(obj);
//            } catch (NoSuchMethodException ex) {
//                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (SecurityException ex) {
//                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IllegalAccessException ex) {
//                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvocationTargetException ex) {
//                Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        this.getSelectionModel().select(0);
    }

    public Object selectedValue() {
        int index = this.getSelectionModel().getSelectedIndex();
        return this.getSelectionModel().getSelectedItem();
//        int currentIndex = 0;
//        if (index != 0) {
//            for (Object obj : _datasource) {
//                if (currentIndex == index - 1) {
//                    Class t = obj.getClass();
//                    try {
//                        Method method = t.getMethod("get" + _ValueProperty, null);
//                        Object key = method.invoke(obj, null);
//                        return key;
//                    } catch (NoSuchMethodException ex) {
//                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (SecurityException ex) {
//                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (IllegalAccessException ex) {
//                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//                    } catch (InvocationTargetException ex) {
//                        Logger.getLogger(MyComboBox.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                currentIndex++;
//            }
//        }
//        return -1;
    }
   
}
