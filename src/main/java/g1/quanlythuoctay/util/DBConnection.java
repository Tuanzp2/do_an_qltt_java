/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ADMIN
 */
public class DBConnection {

    private final String url = "jdbc:sqlserver://localhost:1433;databaseName=QLTT;encrypt=true;trustServerCertificate=true;";
//   private static final String url = "jdbc:sqlserver://26.163.138.229:1433;databaseName=QLTT;encrypt=true;trustServerCertificate=true;";

    private static DBConnection instance = null;

    //constructor private
    private DBConnection() {

    }

    public static DBConnection Instance() {
        if (instance == null) {
            instance = new DBConnection();
        }
        return instance;
    }

    public Connection DBConnect() {
        try {
            Connection conn = null;
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, "sa", "123");
//            conn = DriverManager.getConnection(url, "sa", "123");
            return conn;
        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {

        }
        return null;
    }


}
