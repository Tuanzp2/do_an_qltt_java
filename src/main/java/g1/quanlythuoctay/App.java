package g1.quanlythuoctay;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * JavaFX App
 */
public class App extends Application {
    public static Stage primaryStage;
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        primaryStage = stage; // Assign the primary stage
        primaryStage.setTitle("Pharmacy Retailer");
        scene = new Scene(loadFXML("login"), 640, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }


    public static void setRootPop(String fxml, String title, boolean resizable, Optional<Consumer<Void>> onHiddenAction) throws IOException { // hien thi hop thoai dialog
        Stage stage = new Stage();
        Scene newScene = new Scene(loadFXML(fxml), 789, 532);
        stage.setResizable(resizable);
        stage.setScene(newScene);
        stage.setTitle(title);


        scene = primaryStage.getScene();
        Stage parentStage = (Stage) scene.getWindow();
        parentStage.setOpacity(0.95);


        stage.setOnHidden(e -> {
            parentStage.setOpacity(1.0);
            onHiddenAction.ifPresent(action -> action.accept(null));
        });

        // Show the new stage as a dialog and wait for it to close
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    public static void main(String[] args) {
        launch();
    }

}