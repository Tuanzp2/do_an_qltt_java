/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class TonKho {

    private String _MaKhoTon;
    private String _MaKho;
    private int _SoLuongMin;
    private String _MaSP;
    private String _MaPhieuNhapKho;
    private int _SoLuong;
    public static String TBL_TonKho="TonKho";
    public static String ID_TonKho = "MaTonKho";
    public static String ID_Kho = "MaKho";
    public static String SL_Min = "TonKhoToiThieu";
    public static String ID_SP = "MaSP";
    public static String ID_PhieuNhapKho = "MaPhieuNhapKho";
    public static String SL = "SoLuong";

    public TonKho() {
    }

    public TonKho(String _MaKhoTon, String _MaKho, int _SoLuongMin, String _MaSP, int _SoLuong) {
        this._MaKhoTon = _MaKhoTon;
        this._MaKho = _MaKho;
        this._SoLuongMin = _SoLuongMin;
        this._MaSP = _MaSP;
        this._SoLuong = _SoLuong;
    }
    /**
     * @return the _MaKhoTon
     */
    public String getMaKhoTon() {
        return _MaKhoTon;
    }

    /**
     * @param _MaKhoTon the _MaKhoTon to set
     */
    public void setMaKhoTon(String _MaKhoTon) {
        this._MaKhoTon = _MaKhoTon;
    }

    /**
     * @return the _MaKho
     */
    public String getMaKho() {
        return _MaKho;
    }

    /**
     * @param _MaKho the _MaKho to set
     */
    public void setMaKho(String _MaKho) {
        this._MaKho = _MaKho;
    }

    /**
     * @return the _SoLuongMin
     */
    public int getSoLuongMin() {
        return _SoLuongMin;
    }

    /**
     * @param _SoLuongMin the _SoLuongMin to set
     */
    public void setSoLuongMin(int _SoLuongMin) {
        this._SoLuongMin = _SoLuongMin;
    }

    /**
     * @return the _MaSP
     */
    public String getMaSP() {
        return _MaSP;
    }

    /**
     * @return the _MaPhieuNhapKho
     */
    public String getMaPhieuNhapKho() {
        return _MaPhieuNhapKho;
    }

    /**
     * @param _MaPhieuNhapKho the _MaPhieuNhapKho to set
     */
    public void setMaPhieuNhapKho(String _MaPhieuNhapKho) {
        this._MaPhieuNhapKho = _MaPhieuNhapKho;
    }

    /**
     * @param _MaSP the _MaSP to set
     */
    public void setMaSP(String _MaSP) {
        this._MaSP = _MaSP;
    }

    /**
     * @return the _SoLuong
     */
    public int getSoLuong() {
        return _SoLuong;
    }

    /**
     * @param _SoLuong the _SoLuong to set
     */
    public void setSoLuong(int _SoLuong) {
        this._SoLuong = _SoLuong;
    }
}
