/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class PhieuChi {
    private String maPhieuChi;
    private String maCongNo;
    private double tienNo;
    private double soTien;
    private String note;
    private String maNV;
    private String thoiGian;
    private String nameNV;
    private double TienDaTra;
    public static String TBL_PhieuChi="PhieuChi";
    public static String id_PhieuChi="MaPhieuChi";
    public static String id_CongNo="MaCongNo";
    public static String soTienTra="SoTien";
    public static String ghiChu="GhiChu";
    public static String id_NV="MaNV";
    public static String time="ThoiGian";

    public PhieuChi() {
    }
//
//    public PhieuChi(String maPhieuChi, String maCongNo, double tienNo, double soTien, String note, String maNV, String thoiGian, String nameNV, double TienDaTra) {
//        this.maPhieuChi = maPhieuChi;
//        this.maCongNo = maCongNo;
//        this.tienNo = tienNo;
//        this.soTien = soTien;
//        this.note = note;
//        this.maNV = maNV;
//        this.thoiGian = thoiGian;
//        this.nameNV = nameNV;
//        this.TienDaTra = TienDaTra;
//    }
    
    public PhieuChi(String maPhieuChi, String maCongNo, double soTien, String note, String maNV, String thoiGian) {
        this.maPhieuChi = maPhieuChi;
        this.maCongNo = maCongNo;
        this.soTien = soTien;
        this.note = note;
        this.maNV = maNV;
        this.thoiGian = thoiGian;
    }
    
    /**
     * @return the maPhieuChi
     */
    public String getMaPhieuChi() {
        return maPhieuChi;
    }

    /**
     * @param maPhieuChi the maPhieuChi to set
     */
    public void setMaPhieuChi(String maPhieuChi) {
        this.maPhieuChi = maPhieuChi;
    }

    /**
     * @return the maCongNo
     */
    public String getMaCongNo() {
        return maCongNo;
    }

    /**
     * @param maCongNo the maCongNo to set
     */
    public void setMaCongNo(String maCongNo) {
        this.maCongNo = maCongNo;
    }

    /**
     * @return the soTien
     */
    public double getSoTien() {
        return soTien;
    }

    /**
     * @param soTien the soTien to set
     */
    public void setSoTien(double soTien) {
        this.soTien = soTien;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the maNV
     */
    public String getMaNV() {
        return maNV;
    }

    /**
     * @param maNV the maNV to set
     */
    public void setMaNV(String maNV) {
        this.maNV = maNV;
    }
        /**
     * @return the nameNV
     */
    public String getNameNV() {
        return nameNV;
    }

    /**
     * @param nameNV the nameNV to set
     */
    public void setNameNV(String nameNV) {
        this.nameNV = nameNV;
    }
        /**
     * @return the thoiGian
     */
    public String getThoiGian() {
        return thoiGian;
    }

    /**
     * @param thoiGian the thoiGian to set
     */
    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }
    /**
     * @return the TienDaTra
     */
    public double getTienDaTra() {
        return TienDaTra;
    }

    /**
     * @param TienDaTra the TienDaTra to set
     */
    public void setTienDaTra(double TienDaTra) {
        this.TienDaTra = TienDaTra;
    }
        /**
     * @return the tienNo
     */
    public double getTienNo() {
        return tienNo;
    }

    /**
     * @param tienNo the tienNo to set
     */
    public void setTienNo(double tienNo) {
        this.tienNo = tienNo;
    }
}
