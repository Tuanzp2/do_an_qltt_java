/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class NhaCungUng {
    private String MaNCU;
    private String NameNCU;
    public static String TBL_NCU="NhaCungUng";
    public static String id_NCU="MaNhaCungUng";
    public static String name_NCU="TenNhaCungUng";
        /**
     * @return the MaNCU
     */
    public String getMaNCU() {
        return MaNCU;
    }

    /**
     * @param MaNCU the MaNCU to set
     */
    public void setMaNCU(String MaNCU) {
        this.MaNCU = MaNCU;
    }

    /**
     * @return the NameNCU
     */
    public String getNameNCU() {
        return NameNCU;
    }

    /**
     * @param NameNCU the NameNCU to set
     */
    public void setNameNCU(String NameNCU) {
        this.NameNCU = NameNCU;
    }
}
