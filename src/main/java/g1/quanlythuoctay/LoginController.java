/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay;

import static g1.quanlythuoctay.App.primaryStage;
import g1.quanlythuoctay.Repositories.UserRepository;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;

import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.PhieuNhapKhoController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.PhieuXuatKhoController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.PhieuYeuCauNhapHangController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.SubController.DonHangNhapChiTietController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class LoginController implements Initializable {

    @FXML
    TextField tf_user;
    @FXML
    TextField tf_password;
    @FXML
    Button bt_login;
    @FXML
    Label lb_output;

    public static String idNV;
    public static String idKho;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        bt_login.addEventHandler(ActionEvent.ACTION, eh -> {
            String username = tf_user.getText();
            String password = tf_password.getText(); // This should be hashed before comparing
            System.out.println("User " + username);

            Repository repo = new Repository();
            User user = repo.validateUser(username, password);

            if (user != null) {
                lb_output.setText("Xin chao: " + user.getTenNV());
                switchSceneBasedOnRole(repo.getUserRole(user));
                lb_output.setText("role:" + repo.getUserRole(user));
                idNV = user.getMaNV();
                idKho = user.getMaKho();
                DI(idNV, idKho);

            } else {
                lb_output.setText("Invalid user or password for: " + username);
            }
        });

    }

    private void switchSceneBasedOnRole(String role) {

        try {
            FXMLLoader loader = new FXMLLoader();
            switch (role) {
                case "admin":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/admin/adminMain.fxml"));
                    break;
                case "quanlykinhdoanh":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/quanlykinhdoanh/quanlykinhdoanh.fxml"));
                    break;

                case "kinhdoanh":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/kinhdoanhkiemkho/kinhDoanhKiemKho.fxml"));
                    break;
                case "kiemkho":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/kinhdoanhkiemkho/kinhDoanhKiemKho.fxml"));
                    break;
                case "kinhdoanhkiemkho":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/kinhdoanhkiemkho/kinhDoanhKiemKho.fxml"));
                    break;
                case "banthuoc":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/banthuoc/BanThuocMainPage.fxml"));
                    break;
                case "ketoan":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/ketoan/KeToanMain.fxml"));
                    break;
                // Add cases for other roles as necessary
                default:
                    // handle unknown role
                    return;
            }
            Parent root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            // handle exception
        }
    }

    private void DI(String maNhanVien, String maKho){
        PhieuNhapKhoController.setMaNhanVien(maNhanVien);
        PhieuXuatKhoController.setMaNhanVien(maNhanVien);
        PhieuXuatKhoController.setMaKho(maKho);
        PhieuNhapKhoController.setMaKho(maKho);
        PhieuYeuCauNhapHangController.setMaKho(maKho);
        PhieuYeuCauNhapHangController.setMaNhanVien(maNhanVien);
    }
}
