package g1.quanlythuoctay;

public class JasperData {


    private String tenSP;  // thay đổi tên trường thành chữ thường
    private int quantity;
    private double total;
    private double price;

    public JasperData(String tenSP, int quantity, double total, double price) {
        this.tenSP = tenSP;
        this.quantity = quantity;
        this.total = total;
        this.price = price;
    }
    
    

    public String getTenSP() {
        return tenSP;
    }

    public int getQuantity() {
        return quantity;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }
        /**
     * @param total the total to set
     */
    
}

