/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

import g1.quanlythuoctay.admin.adminUser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.PhieuNhapKhoController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.PhieuXuatKhoController;
import g1.quanlythuoctay.kinhdoanhkiemkho.Controller.PhieuYeuCauNhapHangController;
import g1.quanlythuoctay.util.DBConnection;

import java.util.HashSet;

/**
 * @author MININT-QHS5KT4-local
 */
public class Repository {

    private static Repository Instance = null;


//    public static String maKho;

    public Repository() {
    }

    public static Repository Instance() {
        return Instance == null ? Instance = new Repository() : Instance;
    }

    public User validateUser(String username, String password) {
        User validUser = null;
        Connection conn = DBConnection.Instance().DBConnect();
        PreparedStatement stmt = null;
        try {

            // It's better to use a PreparedStatement to protect against SQL injection
            String strQuery = "SELECT * FROM NhanVien WHERE UserName = ? AND MatKhau = ?";
            stmt = conn.prepareStatement(strQuery);
            stmt.setString(1, username);
            stmt.setString(2, password); // This should be a hashed password

            ResultSet rs = stmt.executeQuery();

            // If user is found
            if (rs.next()) {
                validUser = new User();
                validUser.setMaNV(rs.getString("MaNV"));
                validUser.setTenNV(rs.getString("TenNV"));
                validUser.setMaKho(rs.getString("MaKho"));
                validUser.setUserName(rs.getString("UserName"));
                validUser.setMatKhau(rs.getString("MatKhau"));
                validUser.setMaca(rs.getInt("Maca"));
                // Found user with matching credentials
            }
        } catch (SQLException e) {
            // Handle exception
            String err = e.getMessage();
            e.printStackTrace(); // It's better to log errors
        }
        return validUser;
    }

    public String getUserRole(User user) {
        // Ensure DBConnection and User classes are properly imported and used
        Connection conn = DBConnection.Instance().DBConnect();
        PreparedStatement stmt = null;
        String role = null;

        // Check if the user object or its MaNV field is null
        if (user == null || user.getMaNV() == null) {
            return null; // Or throw an IllegalArgumentException
        }

        // SQL query
        String query = "SELECT GiaoDien.TenGiaoDien\n"
                + "FROM NhanVien\n"
                + "JOIN NhanVienVaiTro ON NhanVien.MaNV = NhanVienVaiTro.MaNV\n"
                + "JOIN VaiTro ON NhanVienVaiTro.MaVaiTro = VaiTro.MaVaiTro\n"
                + "JOIN GiaoDien ON VaiTro.MaGiaoDien = GiaoDien.MaGiaoDien\n"
                + "WHERE NhanVien.MaNV = ?";

        try {
            stmt = conn.prepareStatement(query);
            // Set the parameter for the MaNV
            stmt.setString(1, user.getMaNV());

            try (ResultSet rs = stmt.executeQuery()) {
                // If a role is found, set it to the role variable
                if (rs.next()) {
                    role = rs.getString("TenGiaoDien");
                }
            }
        } catch (SQLException e) {
            // Handle exception
            e.printStackTrace(); // It's better to log errors in a production environment
        } finally {
            // Close the PreparedStatement
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            // Close the Connection
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return role;
    }


}
