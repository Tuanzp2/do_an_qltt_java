/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class CTPhieuYC {
    private String id_PhieuCT;
    private String id_PhieuNhap;
    private String id_SP;
    private int quantity;
    public static String TBL_CTPhieuYC="DonHangNhapChiTiet";
    public static String MaPhieuCT="MaDHNCT";
    public static String soluong="Soluong";
    public static String maSP="MaSP";
    public static String MaPhieuNhap="MaDonHangNhap";

    public CTPhieuYC() {
    }

    public CTPhieuYC(String id_PhieuCT, String id_PhieuNhap, String id_SP, int quantity) {
        this.id_PhieuCT = id_PhieuCT;
        this.id_PhieuNhap = id_PhieuNhap;
        this.id_SP = id_SP;
        this.quantity = quantity;
    }
    
    /**
     * @return the id_PhieuCT
     */
    public String getId_PhieuCT() {
        return id_PhieuCT;
    }

    /**
     * @param id_PhieuCT the id_PhieuCT to set
     */
    public void setId_PhieuCT(String id_PhieuCT) {
        this.id_PhieuCT = id_PhieuCT;
    }

    /**
     * @return the id_PhieuNhap
     */
    public String getId_PhieuNhap() {
        return id_PhieuNhap;
    }

    /**
     * @param id_PhieuNhap the id_PhieuNhap to set
     */
    public void setId_PhieuNhap(String id_PhieuNhap) {
        this.id_PhieuNhap = id_PhieuNhap;
    }

    /**
     * @return the id_SP
     */
    public String getId_SP() {
        return id_SP;
    }

    /**
     * @param id_SP the id_SP to set
     */
    public void setId_SP(String id_SP) {
        this.id_SP = id_SP;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
