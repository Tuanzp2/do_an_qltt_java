/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.SanPham;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneListAddController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btn_cong;

    @FXML
    private Button btn_remove;

    @FXML
    private Button btn_tru;

    @FXML
    private Text tx_index;

    @FXML
    private Text tx_sl;
    @FXML
    private Text tx_tenSP;
    int quantity;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btn_cong.addEventHandler(ActionEvent.ACTION, (event) -> {
            quantity++;
            tx_sl.setText(String.valueOf(quantity));
        });
        btn_tru.addEventHandler(ActionEvent.ACTION, (event) -> {
            quantity--;
            if (quantity >= 1) {
                tx_sl.setText(String.valueOf(quantity));
            } else {
                quantity = 1;
            }

        });
    }

    public void LoadSP(int index, SanPham Item, Integer quantity) {
        System.out.println("alo");
        tx_index.setText(String.valueOf(index));
        tx_tenSP.setText(String.valueOf(Item.getTenSP()));
        tx_sl.setText(String.valueOf(quantity)); // Set the quantity
        this.quantity = quantity;

    }
}
