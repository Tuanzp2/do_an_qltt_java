/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.SanPham;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneOrderController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btn_minus;

    @FXML
    private Button btn_plus;

    @FXML
    private Button btn_remove;

    @FXML
    private Label tx_index;

    @FXML
    private Label tx_name;

    @FXML
    private Label tx_sl;
    @FXML
    private Label tx_price;
    int quantity;
    static RowPaneOrderController a;

    public RowPaneOrderController() {
        a = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btn_plus.addEventHandler(ActionEvent.ACTION, (event) -> {
            quantity++;
            tx_sl.setText(String.valueOf(quantity));
            RowPaneBanThuocController.a.updateTotalAmount();
        });
        btn_minus.addEventHandler(ActionEvent.ACTION, (event) -> {
            quantity--;
            if (quantity >= 1) {
                tx_sl.setText(String.valueOf(quantity));
                RowPaneBanThuocController.a.updateTotalAmount();
            } else {
                quantity = 1;
            }

        });

    }

    public Button getBtnRemove() {
        return btn_remove;

    }
//    private HashSet<Order> orderItems = new HashSet<>();

    public void LoadDataOrder(int index, SanPham Item, Integer quantity) {
        tx_index.setText(String.valueOf(index));
        tx_name.setText(String.valueOf(Item.getTenSP()));
        tx_sl.setText(String.valueOf(quantity)); // Set the quantity
        this.quantity = quantity;
        tx_price.setText(String.valueOf(Item.getGiaban() * quantity));


    }

}
