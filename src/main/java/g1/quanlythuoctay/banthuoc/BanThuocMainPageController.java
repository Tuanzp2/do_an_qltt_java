/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import g1.quanlythuoctay.TonKho;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class BanThuocMainPageController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private MenuItem mn_cashier;

    @FXML
    private MenuItem mn_storage;
    @FXML
    private MenuItem mn_listbill;
    @FXML
    private MenuItem mn_ycNKHO;
    @FXML
    private ScrollPane sp_mainPane;
    static BanthuocController bt_control = null;
    static HistoryBillController hb_control=null;
    static PhieuYeuCauNhapKhoController pycnk_control=null;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        mn_cashier.addEventHandler(ActionEvent.ACTION, (eh) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/banthuoc.fxml"));

            try {
                AnchorPane contentBanThuoc = fxmlLoader.load();//get root tag fxml
                bt_control = fxmlLoader.getController();
                sp_mainPane.setContent(contentBanThuoc);
                BanthuocController.BT.DeleteGrid();
            } catch (IOException ex) {
                Logger.getLogger(BanThuocMainPageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        mn_storage.addEventHandler(ActionEvent.ACTION, (eh) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/TonKho.fxml"));
            try {
                AnchorPane contentTonKho = fxmlLoader.load();//get root tag fxml
//                bt_control = fxmlLoader.getController();
                sp_mainPane.setContent(contentTonKho);
            } catch (IOException ex) {
                Logger.getLogger(BanThuocMainPageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        mn_listbill.addEventHandler(ActionEvent.ACTION, (eh) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/HistoryBill.fxml"));
            try {
                AnchorPane contentTonKho = fxmlLoader.load();//get root tag fxml
                hb_control = fxmlLoader.getController();
                sp_mainPane.setContent(contentTonKho);
            } catch (IOException ex) {
                Logger.getLogger(BanThuocMainPageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        mn_ycNKHO.addEventHandler(ActionEvent.ACTION, (eh) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/PhieuYeuCauNhapKho.fxml"));
            try {
                AnchorPane content = fxmlLoader.load();//get root tag fxml
                pycnk_control= fxmlLoader.getController();
                sp_mainPane.setContent(content);
            } catch (IOException ex) {
                Logger.getLogger(PhieuYeuCauNhapKhoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
}
