/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

/**
 *
 * @author ADMIN
 */

import java.io.InputStream;
import g1.quanlythuoctay.SanPham;
import java.util.HashSet;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
public class JasperReportGenerator {

    public void generateReport(HashSet<SanPham> sp) {
        try {
            // Load Jasper Report Template
            InputStream reportTemplate = getClass().getResourceAsStream("C:/Users/ADMIN/JaspersoftWorkspace/Test/REPORT.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(reportTemplate);

            // Convert List<Product> to JRBeanCollectionDataSource
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(sp);

            // Fill Jasper Report with data
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);

            // Xuất báo cáo ra PDF (hoặc các định dạng khác)
//            JasperExportManager.exportReportToPdfFile(jasperPrint, "/path/to/output/report.pdf");

        } catch (JRException e) {
            e.printStackTrace();
        }
    }
}
