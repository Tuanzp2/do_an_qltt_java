/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.CTPhieuYC;
import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.LoginController;
import g1.quanlythuoctay.PhieuYCNhapKho;
import g1.quanlythuoctay.Repositories.KhoRepository;
import g1.quanlythuoctay.Repositories.PhieuYCRepository;
import g1.quanlythuoctay.Repositories.SanPhamRepository;
import g1.quanlythuoctay.Repositories.UserRepository;
import g1.quanlythuoctay.SanPham;
import g1.quanlythuoctay.User;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class PhieuYeuCauNhapKhoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btn_add;

    @FXML
    private Button btn_delete;

    @FXML
    private TitledPane tp_sanpham;

    @FXML
    private GridPane gp_listAdd;
    @FXML
    private GridPane gp_listSP;
    @FXML
    private GridPane gp_listBill;

    @FXML
    private TextField tx_date;

    @FXML
    private TextField tx_nameKho;

    @FXML
    private TextField tx_nameNV;

    @FXML
    private TextField tx_note;
    static HashSet<SanPham> addedPro = new HashSet<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        HashSet<PhieuYCNhapKho> yc = PhieuYCRepository.Instance().findAll();
        int index = 0;
        for (PhieuYCNhapKho item : yc) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneListBill.fxml"));
            try {
                GridPane contentnhacungung = fxmlLoader.load();//get root tag fxml
                RowPaneListBillController controller = fxmlLoader.getController();
               
                controller.LoadList(item, index);
                gp_listBill.addRow(index, contentnhacungung);

                index++;
            } catch (IOException ex) {
                Logger.getLogger(PhieuYeuCauNhapKhoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        HashSet<User> user = UserRepository.Instance().findAll();
        for (User usr : user) {
            if (usr.getMaNV().equalsIgnoreCase(LoginController.idNV)) {
                tx_nameNV.setText(String.valueOf(usr.getTenNV()));
                tx_nameNV.setMouseTransparent(true);
            }
        }
        HashSet<Kho> kho = KhoRepository.Instance().findAll();
        for (Kho kh : kho) {
            if (kh.getMaKho().equalsIgnoreCase(LoginController.idKho)) {
                tx_nameKho.setText(String.valueOf(kh.getTenKho()));
                tx_nameKho.setMouseTransparent(true);
            }
        }
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedDateStart = currentDate.format(formatter);
        tx_date.setText(formattedDateStart);
        tx_date.setMouseTransparent(true);

        //titledpane for "NhaCungUng"//
        tp_sanpham.setExpanded(false);
        HashSet<SanPham> sp = SanPhamRepository.Instance().findAll();
        int ind = 0;
        for (SanPham item : sp) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneSanPham.fxml"));
            try {
                GridPane contentnhacungung = fxmlLoader.load();//get root tag fxml
                RowPaneSanPhamController controller = fxmlLoader.getController();
                controller.setGpOrder(gp_listAdd);
                controller.LoadData(item, ind);
                gp_listSP.addRow(ind, contentnhacungung);

                ind++;
            } catch (IOException ex) {
                Logger.getLogger(PhieuYeuCauNhapKhoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        btn_add.addEventHandler(ActionEvent.ACTION, (eh) -> {
            AddPhieu();
            AddPhieuDetail();
        });

    }

    private int AddPhieu() {
        try {
            String maNV = LoginController.idNV;
            String note = tx_note.getText();
            LocalDate currentDate = LocalDate.now();
            String date = currentDate.format(DateTimeFormatter.ofPattern("yyyy-dd-MM"));
            PhieuYCNhapKho newHD = new PhieuYCNhapKho("", 1, date, note, maNV);
            SanPhamRepository.Instance().InsertPhieu(newHD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void AddPhieuDetail() {
        for (SanPham sp : addedPro) {
            String maSP = sp.getMaSP();
            // Lấy quantity từ đối tượng SanPham
            int quantity = RowPaneSanPhamController.a.quantityMASP(sp.getTenSP());
            System.out.println("so luong " +quantity);
            CTPhieuYC newHD = new CTPhieuYC("", "", maSP, quantity);
            SanPhamRepository.Instance().InsertCTPhieu(newHD);
        }

    }

}
