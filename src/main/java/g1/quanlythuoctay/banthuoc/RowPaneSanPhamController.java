/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.Repositories.SanPhamRepository;
import g1.quanlythuoctay.SanPham;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import g1.quanlythuoctay.banthuoc.BanThuocMainPageController;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneSanPhamController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btn_add;
    private GridPane gp_listAdd;

    public void setGpOrder(GridPane gp_listAdd) {
        this.gp_listAdd = gp_listAdd;
    }
    static HashMap<String, Integer> addedItems = new HashMap<>();

//    @FXML
//    private GridPane gp_listSP;
    static RowPaneSanPhamController a;
//    HashSet<SanPham> addedProducts = new HashSet<>();

    public RowPaneSanPhamController() {
        a = this;
    }
    static int i = 0;
    @FXML
    private Text tx_nameSP;
    public SanPham selectedSanPham;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void LoadData(SanPham item, int index) {
        tx_nameSP.setText(String.valueOf(item.getTenSP()));
        selectedSanPham = item;
        btn_add.addEventHandler(ActionEvent.ACTION, (eh) -> {

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneListAdd.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneListAddController controller = fxmlLoader.getController();
                if (!addedItems.containsKey(selectedSanPham.getTenSP())) {
                    System.out.println("test");
                    System.out.println("sl " + selectedSanPham.getSoluong());
                    addedItems.put(selectedSanPham.getTenSP(), 1);
                    controller.LoadSP(gp_listAdd.getRowCount(), selectedSanPham, addedItems.get(selectedSanPham.getTenSP()));
                    gp_listAdd.addRow(gp_listAdd.getRowCount(), contentItem);
                    System.out.println("a " + selectedSanPham.getTenSP());
                    PhieuYeuCauNhapKhoController.addedPro.add(selectedSanPham);

                } else {
                    System.out.println("Da co");
                }
            } catch (IOException ex) {
                Logger.getLogger(PhieuYeuCauNhapKhoController.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
    }

    public int quantityMASP(String TenSP) {
        for (Node node : gp_listAdd.getChildren()) {
            if (node instanceof GridPane) {
                GridPane rowPane = (GridPane) node;
                Text maSPLabel = (Text) rowPane.lookup("#tx_tenSP");

                if (maSPLabel != null && maSPLabel.getText().equals(TenSP)) {
                    // If the MaSP matches, retrieve the corresponding index
                    Text quantityLabel = (Text) rowPane.lookup("#tx_sl");
                    return quantityLabel != null ? Integer.parseInt(quantityLabel.getText()) : 0;
                }
            }
        }
        return 0;
    }
}
