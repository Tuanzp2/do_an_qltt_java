/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.LoginController;
import g1.quanlythuoctay.StringValue;
import g1.quanlythuoctay.Repository.*;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneBillController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Text tx_index;
    @FXML
    private Text tx_dateHD;

    @FXML
    private Text tx_detail;

    @FXML
    private Text tx_maHD;

    @FXML
    private Text tx_maNV;

    @FXML
    private Text tx_nameHT;

    @FXML
    private Text tx_sdt;

    @FXML
    private Text tx_totalBill;
    static String maHDON;
    public static RowPaneBillController rbb=null;
    public RowPaneBillController() {
        rbb = this;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void LoadData(int index, HoaDon hd) {
        tx_index.setText(String.valueOf(index+1));
        tx_maHD.setText(String.valueOf(hd.getMaHD()));
        tx_sdt.setText(String.valueOf(hd.getSDT()));
        tx_maNV.setText(String.valueOf(hd.getMaNV()));
        tx_dateHD.setText(String.valueOf(hd.getNgayHoaDon()));
      
        HashSet<HoaDon> hdon = g1.quanlythuoctay.Repositories.HoaDonRepository.Instance().findAll();
        for (HoaDon hoadon : hdon) {
            if (hd.getMaKho().equalsIgnoreCase(hoadon.getMaKho())) {
                tx_nameHT.setText(String.valueOf(hoadon.getTenKho()));
                break;
            }
        }
        tx_totalBill.setText(String.valueOf(hd.getTotal()));
        tx_detail.setOnMouseClicked((eh) -> {
            maHDON = BanThuocMainPageController.hb_control.getMAHOADON(index);
            System.out.println("ma hoa don" +maHDON);
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.NEW_CLASS_ROOM_PAGE));
            try {
                System.out.println("test");
                System.out.println(StringValue.NEW_CLASS_ROOM_PAGE);
                AnchorPane newClassRoom = loader.load();
                BillDetailController control = loader.getController();
                control.init();
                Stage subStage = new Stage();
                subStage.initModality(Modality.WINDOW_MODAL);
                subStage.setScene(new Scene(newClassRoom));
                subStage.show();
            } catch (Exception e) {
            }
        });
    }


}
