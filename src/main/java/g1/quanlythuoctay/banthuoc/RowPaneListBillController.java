/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.PhieuYCNhapKho;
import g1.quanlythuoctay.SanPham;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneListBillController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Text tx_TenNV;

    @FXML
    private Text tx_dayLap;

    @FXML
    private Text tx_detail;

    @FXML
    private Text tx_idPhieu;

    @FXML
    private Text tx_intDex;

    @FXML
    private Text tx_ttrang;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    public void LoadList(PhieuYCNhapKho yc,int index){
        tx_intDex.setText(String.valueOf(index+1));
        tx_dayLap.setText(String.valueOf(yc.getNgayLap()));
        tx_idPhieu.setText(String.valueOf(yc.getId_MaYC()));
        
        if(String.valueOf(yc.getTrangThai()).equalsIgnoreCase("1")){
            tx_ttrang.setText(String.valueOf("Đang chờ"));
        }
        else{
            tx_ttrang.setText(String.valueOf("Hoàn thành"));
        }
    }
}
