/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.LoginController;
import g1.quanlythuoctay.Repositories.HoaDonRepository;
import g1.quanlythuoctay.util.MyComboBox2;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class HistoryBillController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private MyComboBox2 cp_sort;

    @FXML
    private GridPane gp_listBill;

    @FXML
    private TextField tf_search;
    @FXML
    private DatePicker dp_dayStart;
    @FXML
    private DatePicker dp_dayEnd;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        dp_dayEnd.setValue(LocalDate.now());
        dp_dayEnd.addEventHandler(ActionEvent.ACTION, (eh) -> {
            java.time.LocalDate currentDate = LocalDate.now();
            try {
                if (dp_dayEnd != null) {
                    if (dp_dayEnd.getValue().isBefore(currentDate) || dp_dayEnd.getValue().isEqual(currentDate)) {
                        if (dp_dayEnd.getValue().isAfter(dp_dayStart.getValue()) || dp_dayEnd.getValue().isEqual(dp_dayStart.getValue())) {
                            java.time.LocalDate dateEnd = dp_dayEnd.getValue();
                            System.out.println("Date End: " + dateEnd);
                            displayFilteredData();
                        } else {
                            dp_dayEnd.setValue(dp_dayStart.getValue());
                        }
                    } else {
                        dp_dayEnd.setValue(currentDate);
                    }
                } else {
                    dp_dayEnd.setValue(currentDate);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        dp_dayStart.addEventHandler(ActionEvent.ACTION, (eh) -> {
            try {
                if (dp_dayStart != null) {
                    if (dp_dayStart.getValue().isBefore(dp_dayEnd.getValue()) || dp_dayStart.getValue().isEqual(dp_dayEnd.getValue())) {
                        java.time.LocalDate dateStart = dp_dayStart.getValue();
                        System.out.println("Date Start: " + dateStart);
                        displayFilteredData();
                    } else {
                        dp_dayStart.setValue(dp_dayEnd.getValue());
                    }
                } else {
                    dp_dayStart.setValue(dp_dayEnd.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        HashSet<HoaDon> data = g1.quanlythuoctay.Repositories.HoaDonRepository.Instance().findAll();
        tf_search.addEventHandler(ActionEvent.ACTION, (event) -> {
            String searchTerm = tf_search.getText().trim().toLowerCase();
            if (!searchTerm.isEmpty()) {
                // Clear existing rows before loading filtered data
                gp_listBill.getChildren().clear();

                HashSet<HoaDon> filteredData = data.stream()
                        .filter(hd -> hd.getMaHD().toLowerCase().contains(searchTerm))
                        .collect(Collectors.toCollection(HashSet::new));

                int index = 0;
                for (HoaDon hd : filteredData) {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneBill.fxml"));
                    try {
                        GridPane contentItem = fxmlLoader.load();
                        RowPaneBillController billController = fxmlLoader.getController();
                        billController.LoadData(index, hd);
//                        if (hd.getNgayHoaDon().) {
                        gp_listBill.addRow(index, contentItem);
//                        }
                        index++;
                    } catch (IOException ex) {
                        Logger.getLogger(HistoryBillController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                // Clear existing rows before loading all data
                gp_listBill.getChildren().clear();
                Reload();
            }
        });

        System.out.println("data size:" + data.size());
        int index = 0;
        for (HoaDon hd : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneBill.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneBillController billController = fxmlLoader.getController();
                billController.LoadData(index, hd);
                gp_listBill.addRow(index, contentItem);

                index++;
            } catch (IOException ex) {
                Logger.getLogger(HistoryBillController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        cbBox();
//        tf_search.setOnAction(eh -> {
//            handleSearch();
//        });
    }

    public void cbBox() {
        String[] comBo = {"Mã Hóa Đơn ↓", "Mã Hóa Đơn ↑", "Ngày ↓", "Ngày ↑"};
        LinkedHashSet<String> comBoSet = new LinkedHashSet<>();
        for (String item : comBo) {
            comBoSet.add(item);
        }
        cp_sort.setTitle("Choose ");
        cp_sort.setDisplayText(comBoSet);
        cp_sort.tx();
        cp_sort.setOnAction(eh -> {
            try {
                cp_sort.handleComboBoxSelection();
                Object selectedSort = cp_sort.getSelectedSort();
                System.out.println(selectedSort);
                if (selectedSort.equals("Mã Hóa Đơn ↓")) {
                    HoaDon.column_sort = "MaHD";
                    HoaDon.Sort = "DESC";
                    if (dp_dayStart.getValue() != null && dp_dayEnd.getValue() != null) {
                        displayFilteredData();
                    } else {
                        Reload();
                    }
                } else if (selectedSort.equals("Mã Hóa Đơn ↑")) {
                    HoaDon.column_sort = "MaHD";
                    HoaDon.Sort = "ASC";
                    if (dp_dayStart.getValue() != null && dp_dayEnd.getValue() != null) {
                        displayFilteredData();
                    } else {
                        Reload();
                    }
                } else if (selectedSort.equals("Ngày ↓")) {
                    HoaDon.column_sort = "NgayHoaDon";
                    HoaDon.Sort = "DESC";
                    if (dp_dayStart.getValue() != null && dp_dayEnd.getValue() != null) {
                        displayFilteredData();
                    } else {
                        Reload();
                    }
                } else if (selectedSort.equals("Ngày ↑")) {
                    HoaDon.column_sort = "NgayHoaDon";
                    HoaDon.Sort = "ASC";
                    if (dp_dayStart.getValue() != null && dp_dayEnd.getValue() != null) {
                        displayFilteredData();
                    } else {
                        Reload();
                    }
                }
            } catch (Exception e) {
            }

        });
    }

    public String getMAHOADON(int index) {
        if (index >= 0 && index < gp_listBill.getChildren().size()) {
            Node nodeAtIndex = gp_listBill.getChildren().get(index);

            if (nodeAtIndex instanceof GridPane) {
                GridPane rowPane = (GridPane) nodeAtIndex;
                Text maHDLabel = (Text) rowPane.lookup("#tx_maHD");

                if (maHDLabel != null) {
                    System.out.println(maHDLabel.getText());
                    return maHDLabel.getText();
                }
            }
        }
        return null;
    }

    public void Reload() {
        LinkedHashSet<HoaDon> newData = HoaDonRepository.Instance().findAllSort();
        gp_listBill.getChildren().clear();
        int index = 0;
        for (HoaDon hd : newData) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneBill.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneBillController billController = fxmlLoader.getController();
                billController.LoadData(index, hd);
                if (hd.getMaKho().equalsIgnoreCase(LoginController.idKho)) {
                    gp_listBill.addRow(index, contentItem);
                    index++;
                }

            } catch (IOException ex) {
                Logger.getLogger(HistoryBillController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void renewIndex() {
        int index = 1;
        for (Node node : gp_listBill.getChildren()) {
            GridPane rowItem = (GridPane) node;
            Text lblNo = (Text) rowItem.getChildren().get(0);
            lblNo.setText(String.valueOf(index));
            index++;

        }
    }

    public void displayFilteredData() {
        gp_listBill.getChildren().clear();
        LinkedHashSet<HoaDon> filteredData = g1.quanlythuoctay.Repositories.HoaDonRepository.Instance().findAllSort().stream()
                .filter(hd -> {
                    LocalDate hdDate = parseDate(hd.getNgayHoaDon());
                    return (hdDate.isEqual(dp_dayStart.getValue()) || hdDate.isAfter(dp_dayStart.getValue()))
                            && (hdDate.isEqual(dp_dayEnd.getValue()) || hdDate.isBefore(dp_dayEnd.getValue()));
                })
                .collect(Collectors.toCollection(LinkedHashSet::new));

        int index = 0;
        for (HoaDon hd : filteredData) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneBill.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneBillController billController = fxmlLoader.getController();
                billController.LoadData(index, hd);
                gp_listBill.addRow(index, contentItem);
                index++;
            } catch (IOException ex) {
                Logger.getLogger(HistoryBillController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private LocalDate parseDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-MM");
        return LocalDate.parse(dateString, formatter);
    }

}
