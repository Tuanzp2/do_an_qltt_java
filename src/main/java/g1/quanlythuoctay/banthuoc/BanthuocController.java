package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.*;
import g1.quanlythuoctay.Repositories.*;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class BanthuocController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btn_buy;

    @FXML
    private Button btn_reset;

    @FXML
    private AnchorPane gp_listOrder;

    @FXML
    private GridPane gp_order;

    @FXML
    private GridPane gp_sp;

    @FXML
    private MenuItem mn_logout;
    @FXML
    private Text lb_total;

    @FXML
    private TextField tf_MoneyBuy;

    @FXML
    private TextField tf_MoneyRefund;

    @FXML
    private TextField tf_sdt;

    static HashSet<SanPham> addedProducts = new HashSet<>();
    static BanthuocController BT = null;
    HoaDon hdon = new HoaDon();
    ChiTietHD ct = new ChiTietHD();
    private boolean updateQuantitySuccess = false;
    static String maKho;
    static HashSet<String> idTonKho = new HashSet<>();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        HashSet<SanPham> data = g1.quanlythuoctay.Repositories.BanThuocRepository.Instance().findAll();
        int index = 0;
        for (SanPham sp : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneBanThuoc.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneBanThuocController controller = fxmlLoader.getController();
                controller.setGpOrder(gp_order);
                controller.LoadData(index, sp);
                gp_sp.addRow(index, contentItem);
                BT = this;
                controller.setTotal(lb_total);
                index++;
            } catch (IOException ex) {
                Logger.getLogger(BanthuocController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        btn_reset.addEventHandler(ActionEvent.ACTION, (event) -> {
            DeleteGrid();
        });
        btn_buy.addEventHandler(ActionEvent.ACTION, (event) -> {
            try {
                UpdateQuantity();
                JasperReport();
                // Đặt biến updateQuantitySuccess là true
                updateQuantitySuccess = true;
                getTonKhoVaMaKho();
                AddBill();
                AddBillDetail();
                DeleteGrid();
                addedProducts.clear();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void JasperReport() {
        try {
            Set<JasperData> jasperDataList = new HashSet<>();

            for (Node node : gp_order.getChildren()) {
                if (node instanceof GridPane) {
                    GridPane rowPane = (GridPane) node;
                    Label tenSPLabel = (Label) rowPane.lookup("#tx_name");
                    Label quantityLabel = (Label) rowPane.lookup("#tx_sl");
                    Label priceLabel = (Label) rowPane.lookup("#tx_price");
                    if (tenSPLabel != null && quantityLabel != null) {
                        String TenSP = tenSPLabel.getText();
                        int quantity = Integer.parseInt(quantityLabel.getText());
                        double price = Double.valueOf(Double.parseDouble(priceLabel.getText()) * Integer.parseInt(quantityLabel.getText()));
                        double total = Double.valueOf(lb_total.getText());

                        JasperData jasperData = new JasperData(TenSP, quantity, total, price);
                        jasperDataList.add(jasperData);
                    }
                }
            }
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(jasperDataList);
            String workingDirectory = System.getProperty("user.dir");
            String relativePath = "\\jasperreport\\Blank_A4.jrxml";
            String absolutePath = workingDirectory + relativePath;

            JasperReport jr = JasperCompileManager.compileReport(absolutePath);

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("DataSource", dataSource);
            JasperPrint jp = JasperFillManager.fillReport(jr, parameters, dataSource);
            JasperViewer.viewReport(jp, false);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    public void DeleteGrid() {
        gp_order.getChildren().clear();
        RowPaneBanThuocController.addedItems.clear();
        RowPaneBanThuocController.a.updateTotalAmount();
    }

    private void UpdateQuantity() {
        System.out.println(addedProducts.size());
        Map<String, Integer> quantities = new HashMap<>();
        for (SanPham addedProduct : addedProducts) {
            System.out.println("MaSP: " + addedProduct.getMaSP());
            System.out.println(RowPaneBanThuocController.a.quantityMASP(addedProduct.getTenSP()));
            int quantity = RowPaneBanThuocController.a.quantityMASP(addedProduct.getTenSP());
            quantities.put(addedProduct.getMaSP(), quantity);

        }
        BanThuocRepository.Instance().BuyProducts(addedProducts, quantities);
        UpdateSoLuongGrid(addedProducts, quantities);
    }

    private int AddBill() {
        try {

            String maHD = hdon.getMaHD();
            String phone = tf_sdt.getText();
            String maNV = LoginController.idNV;
            String idKho = maKho;
            double total = Double.parseDouble(lb_total.getText());
            System.out.println("Tong tien" + total);
            LocalDate currentDate = LocalDate.now();
            String date = currentDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            HoaDon newHD = new HoaDon(maHD, phone, maNV, date, idKho, total);
            BanThuocRepository.Instance().InsertBill(newHD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int getTonKhoVaMaKho() {
        HashSet<TonKho> krs = TonKhoRepository.Instance().findAll();
        try {
            for (TonKho tkho : krs) {
                if (LoginController.idKho.equalsIgnoreCase(tkho.getMaKho())) {
                    maKho = tkho.getMaKho();
                    idTonKho.add(tkho.getMaKhoTon());
                    System.out.println("maKho" + maKho);
                } else {
                    System.out.println("Khong tim thay");
                }
            }
        } catch (Exception e) {
        }
        return 0;
    }

    private void AddBillDetail() {
        Iterator<String> iterator = idTonKho.iterator();
        for (SanPham sp : addedProducts) {
            // Lấy MaChiTietHD từ datMaChiTietHD của đối tượng ChiTietHD
            String maChiTietHD = ct.getMaChiTietHD();
            System.out.println(ct.getMaChiTietHD());
            // Lấy MaHD từ datMaHD của đối tượng HoaDon
            String maHD = hdon.getMaHD();
            // Lấy MaSP từ đối tượng SanPham
            String maSP = sp.getMaSP();
            System.out.println("su" + idTonKho.size());
            String idTonKhoValue = iterator.hasNext() ? iterator.next() : "";

            // Lấy quantity từ đối tượng SanPham
            int quantity = RowPaneBanThuocController.a.quantityMASP(sp.getTenSP());

            ChiTietHD newBillDetail = new ChiTietHD(maChiTietHD, maHD, maSP, idTonKhoValue, quantity);

            // Add ChiTietHD to HoaDon
            hdon.addNewCTHD(newBillDetail);

            // Gọi phương thức InsertBillDetail với đối tượng ChiTietHD mới
            BanThuocRepository.Instance().InsertBillDetail(newBillDetail);
        }

    }

    private void UpdateSoLuongGrid(HashSet<SanPham> products, Map<String, Integer> quantities) {
        HashSet<String> processedProducts = new HashSet<>();
        for (Node node : gp_sp.getChildren()) {
            if (node instanceof GridPane) {
                GridPane rowPane = (GridPane) node;
                Text maSPLabel = (Text) rowPane.lookup("#tx_name");
                for (SanPham sp : products) {
                    if (!processedProducts.contains(sp.getMaSP())) {
                        int quantity = quantities.getOrDefault(sp.getMaSP(), 0);
                        if (maSPLabel != null && maSPLabel.getText().equals(sp.getTenSP())) {
                            Text quantityLabel = (Text) rowPane.lookup("#tx_sl");
                            if (quantityLabel != null) {
                                int currentQuantity = Integer.parseInt(quantityLabel.getText());
                                int soluong = currentQuantity - quantity;
                                quantityLabel.setText(String.valueOf(soluong));
                            } else {
                                System.out.println("Unexpected node type for quantity: " + quantityLabel.getClass().getName());
                            }
                            processedProducts.add(sp.getMaSP());
                            break;
                        }
                    }
                }
            }
        }
    }

}
