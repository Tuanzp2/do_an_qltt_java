/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.ChiTietHD;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneBillDetailController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Text tx_index;

    @FXML
    private Text tx_nameNV;

    @FXML
    private Text tx_nameSP;

    @FXML
    private Text tx_quantitySP;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    public void LoadData(ChiTietHD cthd, int index) {
            tx_index.setText(String.valueOf(index+1));
            tx_nameSP.setText(String.valueOf(cthd.getTenSP()));
            tx_nameNV.setText(String.valueOf(cthd.getTenNV()));
            tx_quantitySP.setText(String.valueOf(cthd.getSLMua()));

    }
}
