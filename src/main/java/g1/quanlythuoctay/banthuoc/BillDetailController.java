/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.ChiTietHD;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class BillDetailController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private GridPane gp_listBillDetail;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        HashSet<ChiTietHD> data = g1.quanlythuoctay.Repositories.CTHoaDonRepository.Instance().findAll();
        int index = 0;
        int indexRow = 1;
        for (ChiTietHD cthd : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneBillDetail.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneBillDetailController billDetailController = fxmlLoader.getController();
                billDetailController.LoadData(cthd, index);

                if (RowPaneBillController.maHDON.equalsIgnoreCase(cthd.getMaHD())) {
                    gp_listBillDetail.addRow(indexRow, contentItem);
                    index++;
                    indexRow++;
                }
            } catch (IOException ex) {
                Logger.getLogger(HistoryBillController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    @FXML
    public void init() {
    }
}
