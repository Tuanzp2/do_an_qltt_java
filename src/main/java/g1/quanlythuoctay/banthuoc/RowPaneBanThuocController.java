/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.banthuoc;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.JasperData;
import g1.quanlythuoctay.SanPham;
import g1.quanlythuoctay.banthuoc.BanthuocController;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneBanThuocController implements Initializable {

    @FXML
    private GridPane gp_sp;
    private GridPane gp_order;

    @FXML
    private Text tx_active;

    @FXML
    private Text tx_name;

    @FXML
    private Text tx_note;

    @FXML
    private Text tx_price;

    @FXML
    Text tx_sl;

    @FXML
    private Button btn_add;
//    HashSet<SanPham> selectedSanPham;
    SanPham selectedSanPham;
    private Text lb_total;

    // Keep track of added items and their quantities
    static HashMap<String, Integer> addedItems = new HashMap<>();
    static RowPaneBanThuocController a;
//    HashSet<SanPham> addedProducts = new HashSet<>();

    public RowPaneBanThuocController() {
        a = this;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btn_add.addEventHandler(ActionEvent.ACTION, (event) -> {
            if (selectedSanPham != null) {
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/banthuoc/RowPaneOrder.fxml"));
                    GridPane contentItem = fxmlLoader.load();
                    RowPaneOrderController controller = fxmlLoader.getController();
                    Button btnRemove = controller.getBtnRemove();
                    btnRemove.setOnAction(e -> {
                        int index = gp_order.getChildren().indexOf(contentItem);
                        addedItems.remove(selectedSanPham.getMaSP());

                        gp_order.getChildren().remove(index);
                        updateTotalAmount();
                        renewIndex();

                    });

                    if (!addedItems.containsKey(selectedSanPham.getMaSP())) {
                        if (selectedSanPham.getSoluong() > 0) {
                            addedItems.put(selectedSanPham.getMaSP(), 1);
                            controller.LoadDataOrder(gp_order.getRowCount(), selectedSanPham, addedItems.get(selectedSanPham.getMaSP()));
                            gp_order.addRow(gp_order.getRowCount(), contentItem);
                            double total = RowPaneBanThuocController.totalAmount;
                            System.out.println(total);
                            System.out.println(selectedSanPham.getMaSP());

                            lb_total.setText(String.valueOf(total));
                            updateTotalAmount();
                            renewIndex();
                            BanthuocController.addedProducts.add(selectedSanPham);

                        } else {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Lỗi");
                            alert.setHeaderText("Không thể thêm sản phẩm vào hóa đơn");
                            alert.setContentText("Sản phẩm đã hết!!");
                            alert.showAndWait();
                        }

                    } else {
                        System.out.println("Da co");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(RowPaneBanThuocController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void setGpOrder(GridPane gp_order) {
        this.gp_order = gp_order;
    }

    public void setTotal(Text lb_total) {
        this.lb_total = lb_total;
    }

    public void LoadData(int index, SanPham Item) {
        tx_name.setText(String.valueOf(Item.getTenSP()));
        tx_price.setText(String.valueOf(Item.getGiaban()));
        tx_sl.setText(String.valueOf(Item.getSoluong()));
        tx_note.setText(String.valueOf(Item.getChiTiet()));
        tx_active.setText(String.valueOf(Item.getTinhtrang()));
        selectedSanPham = Item;

    }
    static double totalAmount = 0.0;
    
    public void updateTotalAmount() {
        double totalAmount = 0.0;
        System.out.println("Update size:" + gp_order.getChildren().size());
        // Loop through the children of gp_order to calculate the total amount
        for (Node node : gp_order.getChildren()) {
            if (node instanceof GridPane) {
                GridPane rowPane = (GridPane) node;

                // Extract quantity and price from the row
                Label quantityLabel = (Label) rowPane.lookup("#tx_sl");  // Assuming the ID is tx_sl
                Label priceLabel = (Label) rowPane.lookup("#tx_price");  // Assuming the ID is tx_price

                if (quantityLabel != null && priceLabel != null) {
                    int quantity = Integer.parseInt(quantityLabel.getText());
                    double price = Double.parseDouble(priceLabel.getText());

                    // Update the total amount
                    totalAmount += quantity * price;
                }
            }
        }

        // Update the total amount in the lb_total Text element
        lb_total.setText(String.valueOf(totalAmount));
    }

    public void renewIndex() {
        int index = 1;
        for (Node node : gp_order.getChildren()) {
            GridPane rowItem = (GridPane) node;
            Label lblNo = (Label) rowItem.getChildren().get(0);
            lblNo.setText(String.valueOf(index));
            index++;

        }
    }

    public int quantityMASP(String TenSP) {
        for (Node node : gp_order.getChildren()) {
            if (node instanceof GridPane) {
                GridPane rowPane = (GridPane) node;
                Label maSPLabel = (Label) rowPane.lookup("#tx_name");

                if (maSPLabel != null && maSPLabel.getText().equals(TenSP)) {
                    // If the MaSP matches, retrieve the corresponding index
                    Label quantityLabel = (Label) rowPane.lookup("#tx_sl");
                    return quantityLabel != null ? Integer.parseInt(quantityLabel.getText()) : 0;
                }
            }
        }
        return 0;
    }
}
