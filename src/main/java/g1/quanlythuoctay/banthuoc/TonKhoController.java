package g1.quanlythuoctay.banthuoc;

import static g1.quanlythuoctay.App.primaryStage;
import g1.quanlythuoctay.LoginController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import g1.quanlythuoctay.TonKho;
import g1.quanlythuoctay.Repositories.TonKhoRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class TonKhoController implements Initializable {

    @FXML
    private GridPane gp_dataTK;
    @FXML
    private MenuItem mn_logout;
    @FXML
    private Button btn_save;
    @FXML
    private MenuItem MI_reload;
    @FXML
    private TextField tf_search;
    private HashSet<TonKho> filteredTonKhoList = new HashSet<>();
    private HashSet<TonKho> tonKhoList;
    TonKhoRepository tkr = new TonKhoRepository();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        HashSet<TonKho> tonKhoList = tkr.findAll();
        System.out.println(tonKhoList);
        loadData(tonKhoList);
//        MI_reload.addEventHandler(ActionEvent.ACTION, (event) -> {
//            reloadAndLoadData();
//        });

        tf_search.addEventHandler(ActionEvent.ACTION, (event) -> {
            String searchTerm = tf_search.getText().trim().toLowerCase();

            if (!searchTerm.isEmpty()) {
                // Clear existing rows before loading filtered data
                gp_dataTK.getChildren().clear();

                HashSet<TonKho> filteredTonKhoList = tonKhoList.stream()
                        .filter(tonKho -> tonKho.getMaSP().toLowerCase().contains(searchTerm))
                        .collect(Collectors.toCollection(HashSet::new));

                loadData(filteredTonKhoList);
            } else {
                // Clear existing rows before loading all data
                gp_dataTK.getChildren().clear();
                loadData(tonKhoList);
            }
        });

    }

    private void reloadAndLoadData() {
        // Clear existing rows before loading all data
        gp_dataTK.getChildren().clear();

        // Reload data from SQL
        HashSet<TonKho> reloadedTonKhoList = tkr.findAll();

        // Load the reloaded data
        loadData(reloadedTonKhoList);
        tf_search.setText(""); // Clear the search text field
    }

    private void loadData(HashSet<TonKho> tonKhoList) {
        gp_dataTK.getChildren().clear();
        cellValues.clear();  // Clear cellValues map
        int rowIndex = 0;
        for (TonKho tonKho : tonKhoList) {
            if (LoginController.idKho.equalsIgnoreCase(tonKho.getMaKho())) {
                Text maTonKhoText = createText(tonKho.getMaKhoTon(), 0, rowIndex, tonKho.getMaKhoTon());
                Text maKhoText = createText(tonKho.getMaKho(), 1, rowIndex, tonKho.getMaKhoTon());
                Text maSPText = createText(tonKho.getMaSP(), 2, rowIndex, tonKho.getMaKhoTon());
                Text minText = createText(String.valueOf(tonKho.getSoLuongMin()), 3, rowIndex, tonKho.getMaKhoTon());
                Text maNhapKhoText = createText(tonKho.getMaPhieuNhapKho(), 4, rowIndex, tonKho.getMaKhoTon());
                Text soLuongText = createText(String.valueOf(tonKho.getSoLuong()), 5, rowIndex, tonKho.getMaKhoTon());

                gp_dataTK.add(maTonKhoText, 0, rowIndex);
                gp_dataTK.add(maKhoText, 1, rowIndex);
                gp_dataTK.add(maSPText, 2, rowIndex);
                gp_dataTK.add(minText, 3, rowIndex);
                gp_dataTK.add(maNhapKhoText, 4, rowIndex);
                gp_dataTK.add(soLuongText, 5, rowIndex);
                rowIndex++;
            }

        }
    }

    private Map<Integer, Map<Integer, String>> cellValues = new HashMap<>();

    private Text createText(String text, int columnIndex, int rowIndex, String maTonKho) {
        Text newText = new Text(text);

//        newText.setOnMouseClicked(event -> {
//            TextField textField = new TextField(newText.getText());
//            gp_dataTK.add(textField, columnIndex, rowIndex);
//            gp_dataTK.getChildren().remove(newText);
//
//            textField.setOnMouseClicked(e -> {
//                e.consume();
//            });
//
//            // Save the value when TextField is replaced
//            cellValues.computeIfAbsent(rowIndex, HashMap::new).put(columnIndex, newText.getText());
//            cellValues.computeIfAbsent(rowIndex, HashMap::new).put(-1, maTonKho); // Save MaTonKho with key -1
//        });
        return newText;
    }

    public String getMaSP() {
        return null;
    }
}
