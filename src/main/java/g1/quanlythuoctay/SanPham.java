/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class SanPham {

    private String _MaSP;
    private String _TenSP;
    private String _MaNhaCungUng;
    private String _MaLoai;
    private String _ChiTiet;
    private double _giaban;
    private double _giaNhapKho;
    private int _soluong;
    private String _tinhtrang;
    public static String TBL_SP = "SanPham";
    public static String Id_SP = "MaSP";
    public static String Name_SP = "TenSP";
    public static String ID_CungUng = "MaNhaCungUng";
    public static String ID_loai = "MaLoai";
    public static String detail = "ChiTietSanPham";
    public static String price = "DonGiaBan";
    public static String priceNhapKho = "GiaNhapKho";

    /**
     * @return the _MaSP
     */
    public String getMaSP() {
        return _MaSP;
    }

    /**
     * @param _MaSP the _MaSP to set
     */
    public void setMaSP(String _MaSP) {
        this._MaSP = _MaSP;
    }

    /**
     * @return the _TenSP
     */
    public String getTenSP() {
        return _TenSP;
    }

    /**
     * @param _TenSP the _TenSP to set
     */
    public void setTenSP(String _TenSP) {
        this._TenSP = _TenSP;
    }

    /**
     * @return the _MaNhaCungUng
     */
    public String getMaNhaCungUng() {
        return _MaNhaCungUng;
    }

    /**
     * @param _MaNhaCungUng the _MaNhaCungUng to set
     */
    public void setMaNhaCungUng(String _MaNhaCungUng) {
        this._MaNhaCungUng = _MaNhaCungUng;
    }

    /**
     * @return the _MaLoai
     */
    public String getMaLoai() {
        return _MaLoai;
    }

    /**
     * @param _MaLoai the _MaLoai to set
     */
    public void setMaLoai(String _MaLoai) {
        this._MaLoai = _MaLoai;
    }

    /**
     * @return the _ChiTiet
     */
    public String getChiTiet() {
        return _ChiTiet;
    }

    /**
     * @param _ChiTiet the _ChiTiet to set
     */
    public void setChiTiet(String _ChiTiet) {
        this._ChiTiet = _ChiTiet;
    }

    /**
     * @return the _giaban
     */
    public double getGiaban() {
        return _giaban;
    }

    /**
     * @param _giaban the _giaban to set
     */
    public void setGiaban(double _giaban) {
        this._giaban = _giaban;
    }

    /**
     * @return the _soluong
     */
    public int getSoluong() {
        return _soluong;
    }

    /**
     * @param _soluong the _soluong to set
     */
    public void setSoluong(int _soluong) {
        this._soluong = _soluong;
    }

    /**
     * @return the _tinhtrang
     */
    public String getTinhtrang() {
        return _tinhtrang;
    }

    /**
     * @param _tinhtrang the _tinhtrang to set
     */
    public void setTinhtrang(String _tinhtrang) {
        this._tinhtrang = _tinhtrang;
    }

    /**
     * @return the _giaNhapKho
     */
    public double getGiaNhapKho() {
        return _giaNhapKho;
    }

    /**
     * @param _giaNhapKho the _giaNhapKho to set
     */
    public void setGiaNhapKho(double _giaNhapKho) {
        this._giaNhapKho = _giaNhapKho;
    }
}
