/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.HashSet;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminRoleRepository {

    private static adminRoleRepository Instance = null;

    public adminRoleRepository() {
    }

    public static adminRoleRepository Instance() {
        return Instance == null ? Instance = new adminRoleRepository() : Instance;
    }
    public HashSet<adminRole> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<adminRole> ls = new HashSet();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select MaVaiTro,TenVT,VaiTro.MaGiaoDien,TenGiaoDien from VaiTro\n"
                    + "join GiaoDien \n"
                    + "On vaitro.MaGiaoDien=GiaoDien.MaGiaoDien;");
            while (rs.next()) {
                adminRole item = new adminRole();
                item.setMaVaiTro(rs.getInt("" + "MaVaiTro"));
                item.setTenVT(rs.getString("TenVT"));
                item.setMaGiaoDien(rs.getInt("MaGiaoDien"));
                item.setTenGiaoDien(rs.getString("TenGiaoDien"));
                ls.add(item);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exceptions, e.g., by logging or throwing an error.
        }
        return ls;
    }

    public boolean deleteRole(int MaVaiTro) {
        boolean isDeleted = false;

        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DBConnection.Instance().DBConnect();
            // It's better to use a PreparedStatement to protect against SQL injection
            String query = "DELETE FROM VaiTro WHERE MaVaiTro = ?";
            stmt = conn.prepareStatement(query);

            // Set the parameters
            stmt.setInt(1, MaVaiTro);
            int affectedRows = stmt.executeUpdate();

            if (affectedRows > 0) {
                isDeleted = true;
                System.out.println("A record was deleted successfully!");
            } else {
                System.out.println("No record was found with MaNV: " + MaVaiTro);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return isDeleted;
    }

    public boolean addRole(int MaVaiTro, String TenVT, int MaGiaoDien) {
        boolean isadded = false;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            // Initialize the connection
            conn = DBConnection.Instance().DBConnect();

            // Check if role already exists
            String query = "SELECT * FROM VaiTro WHERE MaVaiTro = ?";
            stmt = conn.prepareStatement(query);
            stmt.setInt(1, MaVaiTro);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                System.out.println("Role already available!");
            } else {
                // Role does not exist, proceed with insertion
                query = "INSERT INTO VaiTro (MaVaiTro, TenVT, MaGiaoDien) VALUES (?, ?, ?)";
                stmt = conn.prepareStatement(query);

                // Set the parameters
                stmt.setInt(1, MaVaiTro);
                stmt.setString(2, TenVT);
                stmt.setInt(3, MaGiaoDien);

                int affectedRows = stmt.executeUpdate();

                if (affectedRows > 0) {

                    isadded = true;
                    System.out.println("A record was added successfully!");
                } else {
                    System.out.println("can not add the record with MaVaiTro: " + MaVaiTro);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return isadded;
    }

    public boolean updateRole(int MaVaiTro, String TenVT, int MaGiaoDien) {
        boolean isupdated = false;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            conn = DBConnection.Instance().DBConnect();
            String query = "select * from VaiTro where MaVaiTro=?";
            stmt = conn.prepareStatement(query);
            stmt.setInt(1, MaVaiTro);
            resultSet = stmt.executeQuery();
            if (!resultSet.next()) {
                System.out.println("Role is not available!");
            } else {
                query = "update VaiTro set TenVT=?,MaGiaoDien=? where MaVaiTro=?";
                stmt = conn.prepareStatement(query);
                // Set the parameters
                stmt.setString(1, TenVT);
                stmt.setInt(2, MaGiaoDien);
                stmt.setInt(3, MaVaiTro);
                int affectedRows = stmt.executeUpdate();
                if (affectedRows > 0) {
                    isupdated = true;
                    System.out.println("A record was updated successfully!");
                } else {
                    System.out.println("Cannot update the record with MaVaiTro: " + MaVaiTro);
                }
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            // Close resources
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return isupdated;
    }
}
