/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.admin.adminStore;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author khiemle
 */
public class RowpaneadminStoreController implements Initializable {

    @FXML
    Label lb_MaKho;
    @FXML
    Label lb_TenKho;
    @FXML
    CheckBox ck_KhoTong;

    @FXML
    ImageView imgEdit;
    @FXML
    ImageView imgDel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO     
    }

    public void LoadData(int index, adminStore Item) {
        lb_MaKho.setText(Item.getMaKho());
        lb_TenKho.setText(Item.getTenKho());
      
//        lblName.setText(Item.getName());
        ck_KhoTong.setSelected(Item.getKhoTong() == 1 ? true : false);
        imgDel.setOnMouseClicked(eh -> {
            String MaKho = Item.getMaKho();
            adminStoreRepository.Instance().deleteStore(MaKho);
            AdminMainController.store_control.deleteRow(index);
           
            //update index other rows
            //call function in cate_control
            AdminMainController.store_control.renewIndex();
        });
        imgEdit.setOnMouseClicked(eh->{
            AdminMainController.store_control.LoadData(Item);
            AdminStoreController.tempindex=index;
            AdminMainController.store_control.bt_UPDATE.setDisable(false);
            AdminMainController.store_control.bt_Reset.setDisable(false);
        });
        
    }

}
