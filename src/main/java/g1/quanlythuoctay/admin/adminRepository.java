/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminRepository {
       private static adminRoleRepository Instance = null;
    public adminRepository(){}
        public static adminRoleRepository Instance() {
        return Instance == null ? Instance = new adminRoleRepository() : Instance;
    }     
    
    
    public ObservableList<admin> findAll() {
         Statement stmt = null;
        ResultSet rs = null;
        ObservableList<admin> ls = FXCollections.observableArrayList();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT NhanVien.MaNV, NhanVien.TenNV, NhomQuyen.MaVaiTro, NhomQuyen.MaNhomQuyen, NhomQuyen.TenNhomQuyen\n" +
"FROM NhanVien\n" +
"JOIN NhomQuyen ON NhanVien.MaNV = NhomQuyen.MaNV;");
            while (rs.next()) {
                admin item = new admin();
                item.setMaNV(rs.getString("MaNV"));
                item.setTenNV(rs.getString("TenNV"));
                item.setMaVaiTro(rs.getInt("MaVaiTro"));
                item.setMaNhomQuyen(rs.getInt("MaNhomQuyen"));
                item.setTenNhomQuyen(rs.getString("TenNhomQuyen"));
                                          
                ls.add(item);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exceptions, e.g., by logging or throwing an error.
        }
        return ls;
    }
}
