/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminUserRoles {
    
    private String TenNV;
    private String UserName;
    private String TenVT;
    private String TenGiaoDien;
    
   public adminUserRoles(){};
   
   public String getTenNV() {
        return this.TenNV;
    }

  
    public void setTenNV(String TenNV) {
        this.TenNV = TenNV;
    }

    /**
     * @return the _cat_name
     */
    public String getUserName() {
        return this.UserName;
    }

    
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }
    public String getTenVT() {
        return this.TenVT;
    }
     public void setTenVT(String TenVT) {
        this.TenVT = TenVT;
    }
    public String getTenGiaoDien() {
        return this.TenGiaoDien;
    }
   public void setTenGiaoDien(String TenGiaoDien) {
        this.TenGiaoDien =TenGiaoDien;
    }
  
   
}
