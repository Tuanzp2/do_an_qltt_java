/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import g1.quanlythuoctay.admin.adminRoleRepository;
import java.io.IOException;

import java.util.HashSet;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class AdminRoleController implements Initializable {

@FXML
TextField tf_MaVaiTro;
@FXML
TextField tf_TenVT;
@FXML
TextField tf_MaGiaoDien;
@FXML
TextField tf_TenGiaoDien;
@FXML
Button bt_NEW;
@FXML
Button bt_UPDATE;

@FXML
GridPane gr_vaitro;
@FXML
Button bt_Reset;

private boolean isFirstClick = true;

    /**
     * Initializes the controller class.
     */
    adminRoleRepository mcr = new adminRoleRepository();
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        HashSet<adminRole> ls = mcr.findAll();

        initializeGridPane(ls);
        
        }
       
    private void initializeGridPane(HashSet<adminRole> ls){

        int row = 0;
        
        for (adminRole item : ls) {
             StackPane paneMaVaiTro=new StackPane(new Label(""+item.getMaVaiTro()));
             paneMaVaiTro.getStyleClass().add("cell-border");
             gr_vaitro.add(paneMaVaiTro,0,row);
             StackPane paneTenVT = new StackPane(new Label(item.getTenVT()));
             paneTenVT.getStyleClass().add("cell-border");
             gr_vaitro.add(paneTenVT,1,row);
             StackPane paneMaGiaoDien=new StackPane(new Label(""+item.getMaGiaoDien()));
             paneMaGiaoDien.getStyleClass().add("cell-border");
             gr_vaitro.add(paneMaGiaoDien,2,row);
             StackPane paneTenGiaoDien = new StackPane(new Label(item.getTenGiaoDien()));
             paneTenGiaoDien.getStyleClass().add("cell-border");
             gr_vaitro.add(paneTenGiaoDien,3,row);
             Button showButton = new Button("Show");
             showButton.setOnAction(event -> {
                // Handle edit button click event
                System.out.println("Show button clicked for item: " + item.getMaVaiTro());
                // Perform actions for showing:
                tf_MaVaiTro.setText(""+item.getMaVaiTro());
                tf_TenVT.setText(item.getTenVT());
                tf_MaGiaoDien.setText(""+(item.getMaGiaoDien()));
                tf_TenGiaoDien.setText(item.getTenGiaoDien());

                bt_UPDATE.setDisable(false);
                bt_Reset.setDisable(false);
                tf_MaVaiTro.setStyle("-fx-background-color: grey;");

                
            });
            StackPane paneshow=new StackPane(showButton);
            paneshow.getStyleClass().add("cell-border");
            gr_vaitro.add(paneshow,4,row);
            
            Button deleteButton = new Button("Delete");
            deleteButton.setOnAction(event -> {
                // Handle delete button click event
                System.out.println("Delete button clicked for item: " + item.getMaVaiTro());
                // Perform actions for deletion
                boolean isDeleted = mcr.deleteRole(item.getMaVaiTro());
                if (isDeleted) {
                    ls.remove(item); // Remove the item from the list
                   updateGridPane(mcr.findAll()); // Call a method to update the GridPane
                }
            });
            StackPane panedelete=new StackPane(deleteButton);
            panedelete.getStyleClass().add("cell-border");
            gr_vaitro.add(panedelete,5,row);
             row++;
        }    
    }
    


    private void updateGridPane(HashSet<adminRole> updatedList) {

        gr_vaitro.getChildren().removeIf(node -> GridPane.getRowIndex(node) != null ); // Clear all the elements from the GridPane
        initializeGridPane(updatedList); // Call the method that initializes the GridPane with the updated list
    }
    
    @FXML
   private void addNewRole(ActionEvent event){
       System.out.println("prepare to add role "+tf_TenVT.getText());
       boolean isadded = mcr.addRole(Integer.parseInt( tf_MaVaiTro.getText()), tf_TenVT.getText(), Integer.parseInt(tf_MaGiaoDien.getText()));
       if (isadded) {
                    
                    updateGridPane(mcr.findAll()); // Call a method to update the GridPane
                }
   }
   @FXML
   private void updateRole(ActionEvent event){
       System.out.println("prepare to update role "+tf_TenVT.getText());
       boolean isupdated = mcr.updateRole(Integer.parseInt( tf_MaVaiTro.getText()), tf_TenVT.getText(), Integer.parseInt(tf_MaGiaoDien.getText()));
       if (isupdated) {
                    
                    updateGridPane(mcr.findAll()); // Call a method to update the GridPane
                }
   }
     @FXML
    private void handleBackToLoginAction(ActionEvent event) {
      try {
           // Load the login scene FXML
            Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

           // Get the current stage from the event source
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Set the login scene to the stage
            stage.setScene(new Scene(loginRoot));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            // handle exception
        }
    }
}
