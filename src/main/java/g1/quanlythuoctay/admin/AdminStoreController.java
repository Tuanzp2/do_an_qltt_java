/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.App;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import g1.quanlythuoctay.admin.adminStore;
import g1.quanlythuoctay.admin.RowpaneadminStoreController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class AdminStoreController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    Label lblNo;
    @FXML
    TextField tf_MaKho;
    @FXML
    TextField tf_TenKho;
    @FXML
    CheckBox ck_KhoTong;
    @FXML
    TextField tf_DiaChi;
    @FXML
    Button bt_NEW;
    @FXML
    public Button bt_UPDATE;
    @FXML
    Button bt_Reset;
    @FXML
    ScrollPane mainStore;
    @FXML
    GridPane gr_Store;
    public static adminStore oldItem = null;
    public static int tempindex;
    private boolean isFirstClick = true;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //load data from table category
        HashSet<adminStore> data = g1.quanlythuoctay.admin.adminStoreRepository.Instance().findAll();
        int index = 0;
        for (adminStore item : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("admin/rowpaneadminStore" + ".fxml"));
            try {
                GridPane contentadminStore = fxmlLoader.load();//get root tag fxml
                RowpaneadminStoreController controller = fxmlLoader.getController();
                controller.LoadData(index, item);
                gr_Store.addRow(index, contentadminStore);

                index++;
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //-------------------
        bt_NEW.addEventHandler(ActionEvent.ACTION, (eh) -> {
            System.out.println("button new is clicked!");
            if (isFirstClick) {
                bt_NEW.setText("Add New");
                tf_MaKho.setText("AutoGen");
                tf_MaKho.setStyle("-fx-background-color: grey;");
                isFirstClick = false;
                bt_UPDATE.setDisable(true);
                bt_Reset.setDisable(false);
            }
            else{
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("admin/rowpaneadminStore" + ".fxml"));
            try {
                GridPane contentadminStore = fxmlLoader.load();//get root tag fxml
                RowpaneadminStoreController controller = fxmlLoader.getController();


                adminStore newitem = new adminStore();
                newitem.setMaKho(tf_MaKho.getText());
                newitem.setTenKho(tf_TenKho.getText());
                newitem.setKhoTong(ck_KhoTong.isSelected() ? 1 : 0);
                int lastRowIndex = findLastRowIndex(gr_Store);
                controller.LoadData(lastRowIndex, newitem);
                gr_Store.addRow(lastRowIndex, contentadminStore);
                adminStoreRepository repository = adminStoreRepository.Instance();
                boolean isadded = repository.newStore(newitem);
                bt_NEW.setText("New");
                isFirstClick = true;

            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
                    }
        });
        bt_UPDATE.addEventHandler(ActionEvent.ACTION, (eh) -> {
            System.out.println("button update is clicked!");
            adminStore item = new adminStore();
            item.setMaKho(tf_MaKho.getText());
            item.setTenKho(tf_TenKho.getText());
            item.setKhoTong(ck_KhoTong.isSelected() ? 1 : 0);
            deleteRow(tempindex);
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("admin/rowpaneadminStore" + ".fxml"));
            try {
                Node row = fxmlLoader.load(); // This should be the root node of your row
                RowpaneadminStoreController controller = fxmlLoader.getController();
                controller.LoadData(tempindex, item); // Load the new data into the controller

                // Add the new row at the correct index
                // Note: You may need to handle the placement of nodes within the row if they are not automatically handled by the FXML
                gr_Store.addRow(tempindex, row);
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
            adminStoreRepository repository = adminStoreRepository.Instance();
                boolean isupdated = repository.updateStore(item);

        });

      bt_Reset.addEventHandler(ActionEvent.ACTION, eh->{
      tf_MaKho.setText("");tf_TenKho.setText("");tf_DiaChi.setText("");
      ck_KhoTong.setSelected(false);
      bt_UPDATE.setDisable(true);
      bt_Reset.setDisable(true);
       bt_NEW.setText("New");
       tf_MaKho.setStyle("");
        isFirstClick = true;
      });
            
    }

    public void LoadData(adminStore itemEdit) {

        tf_MaKho.setText(itemEdit.getMaKho());
        tf_TenKho.setText(itemEdit.getTenKho());
        ck_KhoTong.setSelected(itemEdit.getKhoTong() == 1 ? true : false);
        oldItem = itemEdit;
    }

    public void deleteRow(int index) {
        gr_Store.getChildren().remove(index);
    }

    public void renewIndex() {
        int index = 1;
        for (Node node : gr_Store.getChildren()) {
            //parse node to RowPaneCategory
            GridPane rowItem = (GridPane) node;
            Label lblNo = (Label) rowItem.getChildren().get(0);
            lblNo.setText(String.valueOf(index));
            index++;
            //change label index
        }
    }

    private int findLastRowIndex(GridPane gridPane) {
        int lastRowIndex = 0;
        for (Node child : gridPane.getChildren()) {
            // Get the gridpane's row index for the child, default to 0 if not set
            Integer rowIndex = GridPane.getRowIndex(child);
            if (rowIndex != null) {
                lastRowIndex = Math.max(lastRowIndex, rowIndex);
            }
        }
        return lastRowIndex + 1; // Return the next row index (one after the last non-empty)
    }

//   

    @FXML
    private void handleBackToLoginAction(ActionEvent event) {
        System.out.println("logout");
        try {

            // Load the login scene FXML
            Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

            // Get the current stage from the event source
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Set the login scene to the stage
            stage.setScene(new Scene(loginRoot));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            // handle exception
        }
    }
}
