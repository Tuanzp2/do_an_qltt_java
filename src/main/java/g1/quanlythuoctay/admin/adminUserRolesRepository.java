/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminUserRolesRepository {

    private static adminUserRolesRepository instance = null;

    private adminUserRolesRepository() {
    }

    public static adminUserRolesRepository Instance() {
        return instance == null ? instance = new adminUserRolesRepository() : instance;
    }

    /**
     * *
     * get all category
     */
    public HashSet<adminUserRoles> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<adminUserRoles> ls = new HashSet<adminUserRoles>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select NhanVien.TenNV,NhanVien.UserName,VaiTro.TenVT,GiaoDien.TenGiaoDien from NhanVienVaiTro LEFT JOIN NhanVien on NhanVienVaiTro.MaNV=NhanVien.MaNV LEFT JOIN VaiTro on NhanVienVaiTro.MaVaiTro=VaiTro.MaVaiTro LEFT JOIN GiaoDien\n"
                    + " on VaiTro.MaGiaoDien=GiaoDien.MaGiaoDien");
            // Now do something with the ResultSet ....
            while (rs.next()) {
                adminUserRoles item = new adminUserRoles();
                item.setTenNV(rs.getString("TenNV"));
                item.setUserName(rs.getString("UserName"));
                item.setTenVT(rs.getString("TenVT"));
                item.setTenGiaoDien(rs.getString("TenGiaoDien"));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public boolean newUserRoles(adminUserRoles newItem) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select * from (select MaNV from NhanVien where TenNV=?) NhanVien \n"
                        + "CROSS JOIN\n"
                        + "			(select MaVaiTro from VaiTro where TenVT= ?) VaiTro;";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, newItem.getTenNV());
                stmt.setString(2, newItem.getTenVT());
                ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    System.out.println("this UserRole is already available!");
                    return false; // If store is not available, return false immediately.
                } else {
                    String strQuery = "INSERT INTO NhanVienVaiTro (MaNV, MaVaiTro)\n"
                            + "SELECT \n"
                            + "    NhanVien.MaNV, \n"
                            + "    VaiTro.MaVaiTro \n"
                            + "FROM \n"
                            + "    (SELECT MaNV FROM NhanVien WHERE TenNV = ?) NhanVien\n"
                            + "CROSS JOIN \n"
                            + "    (SELECT MaVaiTro FROM VaiTro WHERE TenVT = ?) VaiTro;";
                    stmt = conn.prepareStatement(strQuery);
                    stmt.setString(1, newItem.getTenNV());
                    stmt.setString(2, newItem.getTenVT());

                    boolean ck = stmt.execute();
                    return ck;
                    //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean deleteUserRoles(adminUserRoles deleteitem) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select * from (select MaNV from NhanVien where TenNV=?) NhanVien \n"
                        + "CROSS JOIN\n"
                        + "			(select MaVaiTro from VaiTro where TenVT= ?) VaiTro;";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, deleteitem.getTenNV());
                stmt.setString(2, deleteitem.getTenVT());
                ResultSet resultSet = stmt.executeQuery();
                if (!resultSet.next()) {
                    System.out.println("this UserRole is NOT available!");
                    return false; // If store is not available, return false immediately.
                } else {
                    String strQuery = "DELETE FROM NhanVienVaiTro\n"
                            + "WHERE \n"
                            + "    MaNV IN (SELECT MaNV FROM NhanVien WHERE TenNV = 'Manager')\n"
                            + "    AND MaVaiTro IN (SELECT MaVaiTro FROM VaiTro WHERE TenVT = 'Ketoan');";
                    stmt = conn.prepareStatement(strQuery);
                    stmt.setString(1, deleteitem.getTenNV());
                    stmt.setString(2, deleteitem.getTenVT());

                    boolean ck = stmt.execute();
                    return ck;
                    //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }
}
