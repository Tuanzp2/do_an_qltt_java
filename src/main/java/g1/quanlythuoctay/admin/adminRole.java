/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminRole {
    
    private int MaVaiTro;
    private String TenVT;
    private int MaGiaoDien;
    private String TenGiaoDien;
   
   public adminRole(){};
   @Override
    public String toString() {
        return getTenVT(); // Replace this with the actual method to get the TenNV property
    }
   public int getMaVaiTro() {
        return this.MaVaiTro;
    }

    /**
     * @param _id the _id to set
     */
    public void setMaVaiTro(int MaVaiTro) {
        this.MaVaiTro = MaVaiTro;
    }

    /**
     * @return the _cat_name
     */
    public String getTenVT() {
        return this.TenVT;
    }

    /**
     * @param _cat_name the _cat_name to set
     */
    public void setTenVT(String TenVT) {
        this.TenVT = TenVT;
    }

    public int getMaGiaoDien() {
        return this.MaGiaoDien;
    }
     public void setMaGiaoDien(int MaGiaoDien) {
        this.MaGiaoDien = MaGiaoDien;
    }
    public String getTenGiaoDien() {
        return this.TenGiaoDien;
    }
   public void setTenGiaoDien(String TenGiaoDien) {
        this.TenGiaoDien =TenGiaoDien;
    }
    
    /**
     * @return the _active
     */
   
       
}
