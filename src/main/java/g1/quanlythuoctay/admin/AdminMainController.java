/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class AdminMainController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    MenuItem MenuUser;
    @FXML

    MenuItem MenuUserRoles;
    @FXML

    MenuItem MenuRole;
    @FXML
    MenuItem MenuShopStore;
    @FXML
    Pane adminmain;
    static AdminStoreController store_control = null;

    static AdminUserRolesController userroles_control = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MenuUser.addEventHandler(ActionEvent.ACTION, eh -> {
            System.out.println("menuUser is clicked!");
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/admin/adminUser" + ".fxml"));
            try {
                Pane Userpane = fxmlLoader.load();//get root tag fxml

                adminmain.getChildren().setAll(Userpane);
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        MenuUserRoles.addEventHandler(ActionEvent.ACTION, eh -> {
            System.out.println("menuUserRoles is clicked!");
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/admin/adminUserRoles" + ".fxml"));
            try {
                Pane Userpane = fxmlLoader.load();//get root tag fxml
                userroles_control = fxmlLoader.getController();
                adminmain.getChildren().setAll(Userpane);
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        MenuRole.addEventHandler(ActionEvent.ACTION, eh -> {
            System.out.println("menuRole is clicked!");
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/admin/adminRole" + ".fxml"));
            try {
                Pane Userpane = fxmlLoader.load();//get root tag fxml
                adminmain.getChildren().setAll(Userpane);
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        MenuShopStore.addEventHandler(ActionEvent.ACTION, eh -> {
            System.out.println("menuShopStore is clicked!");
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/admin/adminStore" + ".fxml"));
            try {
                Pane Userpane = fxmlLoader.load();//get root tag fxml
                store_control = fxmlLoader.getController();
                adminmain.getChildren().setAll(Userpane);
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

}
