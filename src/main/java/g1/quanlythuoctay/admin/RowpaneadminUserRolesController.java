/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class RowpaneadminUserRolesController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    Label lb_Staff;
    @FXML
    Label lb_UserName;
    @FXML
    Label lb_RoleName;
    @FXML
    Label lb_LoginScreen;
    @FXML
    ImageView imgEdit;
    @FXML
    ImageView imgDel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void LoadData(int index, adminUserRoles Item) {
        lb_Staff.setText(Item.getTenNV());
        lb_UserName.setText(Item.getUserName());
        lb_RoleName.setText(Item.getTenVT());
        lb_LoginScreen.setText(Item.getTenGiaoDien());
        imgDel.setOnMouseClicked(eh -> {

            adminUserRolesRepository.Instance().deleteUserRoles(Item);
            AdminMainController.userroles_control.deleteRow(index);

            //update index other rows
            //call function in cate_control
//            AdminMainController.userroles_control.renewIndex();
        });
        imgEdit.setOnMouseClicked(eh -> {
            AdminMainController.userroles_control.LoadData(Item);
            AdminUserRolesController.tempindex = index;
        });

    }

}
