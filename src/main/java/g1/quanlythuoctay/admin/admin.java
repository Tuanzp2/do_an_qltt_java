/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class admin {
    private String MaNV;
    private String TenNV;
    private int MaVaiTro;
    private int MaNhomQuyen;
    private String TenNhomQuyen;
    
   public admin(){};
   
   public String getMaNV() {
        return this.MaNV;
    }

    /**
     * @param _id the _id to set
     */
    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    /**
     * @return the _cat_name
     */
    public String getTenNV() {
        return this.TenNV;
    }

    /**
     * @param _cat_name the _cat_name to set
     */
    public void setTenNV(String TenNV) {
        this.TenNV = TenNV;
    }
    public int getMaVaiTro() {
        return this.MaVaiTro;
    }

    /**
     * @param _cat_name the _cat_name to set
     */
    public void setMaVaiTro(int MaVaiTro) {
        this.MaVaiTro = MaVaiTro;
    }
    /**
     * @return the _active
     */
    public int getMaNhomQuyen(int MaNhomQuyen) {
        return this.MaNhomQuyen;
    }

   
    public void setMaNhomQuyen(int MaNhomQuyen) {
        this.MaNhomQuyen =MaNhomQuyen;
    }

    public String getTenNhomQuyen() {
        return this.TenNhomQuyen;
    }
   public void setTenNhomQuyen(String TenNhomQuyen) {
        this.TenNhomQuyen =TenNhomQuyen;
    }
   
}

