/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import g1.quanlythuoctay.admin.adminUserRepository;
import java.util.HashSet;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class AdminUserController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private GridPane gr_nhanvien;
    @FXML
    private TextField tf_MaNV;
    @FXML
    private TextField tf_TenNV;
    @FXML
    private TextField tf_MaKho;
    @FXML
    private TextField tf_UserName;
    @FXML
    private TextField tf_MatKhau;
    @FXML
    private TextField tf_Maca;
    @FXML
    private Button bt_NEW;
    @FXML
    private Button bt_UPDATE;
    @FXML
    private Button bt_Reset;
    private boolean isFirstClick = true;
    adminUserRepository mcr = new adminUserRepository();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        adminUserRepository mcr = new adminUserRepository();

        HashSet<adminUser> ls = g1.quanlythuoctay.admin.adminUserRepository.Instance().findAll();

        int row = 2;
        System.out.println("ls size is:" + ls.size());
        for (adminUser item : ls) {
            StackPane paneMaNV = new StackPane(new Label(item.getMaNV()));
            paneMaNV.getStyleClass().add("cell-border");
            StackPane paneTenNV = new StackPane(new Label(item.getTenNV()));
            paneTenNV.getStyleClass().add("cell-border");
            StackPane paneMaKho = new StackPane(new Label("" + item.getMaKho()));
            paneMaKho.getStyleClass().add("cell-border");
            StackPane paneUserName = new StackPane(new Label(item.getUserName()));
            paneUserName.getStyleClass().add("cell-border");
            StackPane paneMatKhau = new StackPane(new Label(item.getMatKhau()));
            paneMatKhau.getStyleClass().add("cell-border");
            StackPane paneMaca = new StackPane(new Label("" + item.getMaca()));
            paneMaca.getStyleClass().add("cell-border");
            // Add more labels for other columns as needed
            gr_nhanvien.add(paneMaNV, 0, row);
            gr_nhanvien.add(paneTenNV, 1, row);
            gr_nhanvien.add(paneMaKho, 2, row);
            gr_nhanvien.add(paneUserName, 3, row);
            gr_nhanvien.add(paneMatKhau, 4, row);
            gr_nhanvien.add(paneMaca, 5, row);

            HBox hbox = new HBox(5); // 5 is the spacing between elements
            hbox.setAlignment(Pos.CENTER); // This will center the buttons in the HBox

            // Create the first button and set an action event
            Button showButton = new Button("Show");
            showButton.setOnAction(event -> {
                // Handle edit button click event
                System.out.println("Show button clicked for item: " + item.getMaNV());
                // Perform actions for showing:
                
                tf_MaNV.setText(item.getMaNV());
                tf_TenNV.setText(item.getTenNV());
                tf_MaKho.setText(item.getMaKho());
                tf_UserName.setText(item.getUserName());
                tf_MatKhau.setText(item.getMatKhau());
                tf_Maca.setText("" + item.getMaca());
                bt_UPDATE.setDisable(false);
                bt_Reset.setDisable(false);
                tf_MaNV.setStyle("-fx-background-color: grey;");
            });

            // Create the second button and set an action event
            Button deleteButton = new Button("Delete");
            deleteButton.setOnAction(event -> {
                // Handle delete button click event
                System.out.println("Delete button clicked for item: " + item.getMaNV());
                // Perform actions for deletion
                boolean isDeleted = mcr.deleteUser(item.getMaNV());
                if (isDeleted) {
                    ls.remove(item); // Remove the item from the list
                    updateGridPane(mcr.Instance().findAll()); // Call a method to update the GridPane
                    
                }
            });

            // Add buttons to the HBox
            hbox.getChildren().addAll(showButton, deleteButton);

            // Add the HBox to the GridPane
            gr_nhanvien.add(hbox, 6, row);
            // Add more labels to GridPane with appropriate column and row values
            row++;
        }

        bt_NEW.addEventHandler(ActionEvent.ACTION, eh -> {

            if (isFirstClick) {
                bt_NEW.setText("Add New");
                tf_MaNV.setText("AutoGen");
                tf_MaNV.setStyle("-fx-background-color: grey;");
                isFirstClick = false;
                bt_UPDATE.setDisable(true);
            } else {

                System.out.println("prepare to add user " + tf_TenNV.getText());
                boolean isadded = mcr.addUser(tf_TenNV.getText(), tf_MaKho.getText(), tf_UserName.getText(), tf_MatKhau.getText(), Integer.parseInt(tf_Maca.getText()));
                if (isadded) {

                    updateGridPane(mcr.findAll()); // Call a method to update the GridPane
                    bt_NEW.setText("New");
                    isFirstClick = true;
                }

            }
        });

        bt_UPDATE.addEventHandler(ActionEvent.ACTION, eh -> {

            System.out.println("prepare to update user " + tf_TenNV.getText());
            boolean isupdated = mcr.updateUser(tf_MaNV.getText(), tf_TenNV.getText(), tf_MaKho.getText(), tf_UserName.getText(), tf_MatKhau.getText(), Integer.parseInt(tf_Maca.getText()));
            if (isupdated) {

                updateGridPane(mcr.findAll());         // Call a method to update the GridPane
                bt_UPDATE.setDisable(true);
            }

        });

        bt_Reset.addEventHandler(ActionEvent.ACTION, eh -> {

            tf_MaNV.setText("");
            tf_TenNV.setText("");
            tf_MaKho.setText("");
            tf_UserName.setText("");
            tf_MatKhau.setText("");
            tf_Maca.setText("");
            bt_UPDATE.setDisable(true);
            bt_NEW.setText("New");
            isFirstClick = true;
            tf_MaNV.setStyle("");
            bt_Reset.setDisable(true);

        });

    }

    private void updateGridPane(HashSet<adminUser> updatedList) {
        gr_nhanvien.getChildren().removeIf(node -> GridPane.getRowIndex(node) != null && GridPane.getRowIndex(node) > 0); // Clear all the elements from the GridPane
        initializeGridPane(updatedList); // Call the method that initializes the GridPane with the updated list
    }

    private void initializeGridPane(HashSet<adminUser> ls) {

        int row = 2;

        for (adminUser item : ls) {
            StackPane paneMaNV = new StackPane(new Label(item.getMaNV()));
            paneMaNV.getStyleClass().add("cell-border");
            StackPane paneTenNV = new StackPane(new Label(item.getTenNV()));
            paneTenNV.getStyleClass().add("cell-border");
            StackPane paneMaKho = new StackPane(new Label("" + item.getMaKho()));
            paneMaKho.getStyleClass().add("cell-border");
            StackPane paneUserName = new StackPane(new Label(item.getUserName()));
            paneUserName.getStyleClass().add("cell-border");
            StackPane paneMatKhau = new StackPane(new Label(item.getMatKhau()));
            paneMatKhau.getStyleClass().add("cell-border");
            StackPane paneMaca = new StackPane(new Label("" + item.getMaca()));
            paneMaca.getStyleClass().add("cell-border");
            // Add more labels for other columns as needed
            gr_nhanvien.add(paneMaNV, 0, row);
            gr_nhanvien.add(paneTenNV, 1, row);
            gr_nhanvien.add(paneMaKho, 2, row);
            gr_nhanvien.add(paneUserName, 3, row);
            gr_nhanvien.add(paneMatKhau, 4, row);
            gr_nhanvien.add(paneMaca, 5, row);

            HBox hbox = new HBox(5); // 5 is the spacing between elements
            hbox.setAlignment(Pos.CENTER); // This will center the buttons in the HBox

            // Create the first button and set an action event
            Button showButton = new Button("Show");
            showButton.setOnAction(event -> {
                // Handle edit button click event
                System.out.println("Show button clicked for item: " + item.getMaNV());
                // Perform actions for showing:
                tf_MaNV.setText(item.getMaNV());
                tf_TenNV.setText(item.getTenNV());
                tf_MaKho.setText(item.getMaKho());
                tf_UserName.setText(item.getUserName());
                tf_MatKhau.setText(item.getMatKhau());
                tf_Maca.setText("" + item.getMaca());
                bt_UPDATE.setDisable(false);
            });

            // Create the second button and set an action event
            Button deleteButton = new Button("Delete");
            deleteButton.setOnAction(event -> {
                // Handle delete button click event
                System.out.println("Delete button clicked for item: " + item.getMaNV());
                // Perform actions for deletion
                boolean isDeleted = mcr.deleteUser(item.getMaNV());
                if (isDeleted) {
                    ls.remove(item); // Remove the item from the list
                    updateGridPane(ls); // Call a method to update the GridPane
                }
            });

            // Add buttons to the HBox
            hbox.getChildren().addAll(showButton, deleteButton);

            // Add the HBox to the GridPane
            gr_nhanvien.add(hbox, 6, row);
            row++;
        }

    }

//   private void addNewUser(ActionEvent event){
//   }
//   @FXML
//    private void updateUser(ActionEvent event) {
//        System.out.println("prepare to update user " + tf_TenNV.getText());
//        boolean isupdated = mcr.updateUser(tf_MaNV.getText(), tf_TenNV.getText(), tf_MaKho.getText(), tf_UserName.getText(), tf_MatKhau.getText(), Integer.parseInt(tf_Maca.getText()));
//        if (isupdated) {
//
//            updateGridPane(mcr.findAll()); // Call a method to update the GridPane
//        }
//    }
//    @FXML
//private void handleBackToLoginAction(ActionEvent event) {
//        try {
//            // Load the login scene FXML
//            Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));
//
//            // Get the current stage from the event source
//            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//
//            // Set the login scene to the stage
//            stage.setScene(new Scene(loginRoot));
//            stage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//            // handle exception
//        }
//    }
}
