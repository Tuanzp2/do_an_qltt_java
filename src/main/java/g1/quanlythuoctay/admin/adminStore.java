/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminStore {
    
    private String MaKho;
    private String TenKho;
    private int KhoTong;
    private String DiaChi;
    public static String STORE_TBL = "KHO";
    public static String ID_COL = "MaKho";
    public static String Name_COL = "TenKho";
    public static String TK_COL = "KhoTong";
   
   public adminStore(){};
   
   public String getMaKho() {
        return this. MaKho;
    }

    /**
     * @param _id the _id to set
     */
    public void setMaKho(String MaKho) {
        this.MaKho = MaKho;
    }

    /**
     * @return the _cat_name
     */
    public String getTenKho() {
        return this.TenKho;
    }

    /**
     * @param _cat_name the _cat_name to set
     */
    public void setTenKho(String TenKho) {
        this.TenKho = TenKho;
    }

    public int getKhoTong() {
        return this.KhoTong;
    }
     public void setKhoTong(int KhoTong) {
        this.KhoTong = KhoTong;
    }
    public String getDiaChi() {
        return this.DiaChi;
    }
   public void setDiaChi(String TenGiaoDien) {
        this.DiaChi =DiaChi;
    }
    
}
