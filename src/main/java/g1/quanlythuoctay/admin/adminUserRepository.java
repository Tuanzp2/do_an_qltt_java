/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;


/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminUserRepository {
    private static adminUserRepository Instance = null;
    public adminUserRepository() {
    }
    public static adminUserRepository Instance() {
        return Instance == null ? Instance = new adminUserRepository() : Instance;
    }
    
//    private String url = "jdbc:sqlserver://127.0.0.1:1433; databaseName = QLTT; encrypt=true; trustServerCertificate=true";
//    private String username = "kynv";
//    private String password = "123";
//    private Connection conn;
    
//    public adminUserRepository() {
//        try {
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            conn = DriverManager.getConnection(url, username, password);
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    public HashSet<adminUser> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<adminUser> ls = new HashSet<adminUser>();
        Connection conn = DBConnection.Instance().DBConnect();
        
        try {

            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT *from NhanVien");
            while (rs.next()) {
                adminUser item = new adminUser();
                item.setMaNV(rs.getString("MaNV"));
                item.setTenNV(rs.getString("TenNV"));
                item.setMaKho(rs.getString("MaKho"));
                item.setUserName(rs.getString("UserName"));
                item.setMatKhau(rs.getString("MatKhau"));
                item.setMaca(rs.getInt("Maca"));
                ls.add(item);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exceptions, e.g., by logging or throwing an error.
        }
        return ls;
    }

 public boolean deleteUser(String MaNV) {
    boolean isDeleted = false;
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
        // Initialize the connection
        conn = DBConnection.Instance().DBConnect();

        // It's better to use a PreparedStatement to protect against SQL injection
        String query = "DELETE FROM NhanVien WHERE MaNV = ?";
        stmt = conn.prepareStatement(query);

        // Set the parameters
        stmt.setString(1, MaNV);
        int affectedRows = stmt.executeUpdate();

        if (affectedRows > 0) {
            isDeleted = true;
            System.out.println("A record was deleted successfully!");
        } else {
            System.out.println("No record was found with MaNV: " + MaNV);
        }
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    } finally {
        // Close resources
        try {
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    return isDeleted;
}

   public boolean addUser( String TenNV, String MaKho, String UserName, String MatKhau, int Maca) {
    boolean isAdded = false;
    Connection conn = null;
    PreparedStatement stmt = null;
    ResultSet resultSet = null;
    try {
        // Initialize the connection
        conn = DBConnection.Instance().DBConnect();

        // Check if user already exists
        String query = "SELECT * FROM NhanVien WHERE UserName = ?";
        stmt = conn.prepareStatement(query);
        stmt.setString(1, UserName);
        resultSet = stmt.executeQuery();

        if (!resultSet.next()) {
            // User does not exist, proceed with insertion
            query = "INSERT INTO NhanVien (TenNV, MaKho, UserName, MatKhau, Maca) VALUES (?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);

            // Set the parameters
           
            stmt.setString(1, TenNV);
            stmt.setString(2, MaKho);
            stmt.setString(3, UserName);
            stmt.setString(4, MatKhau);
            stmt.setInt(5, Maca);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows > 0) {
                isAdded = true;
                System.out.println("A record was added successfully!");
            } else {
                System.out.println("Cannot add the record with TenNV: " + TenNV);
            }
        } else {
            System.out.println("User already available!");
        }
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    } finally {
        // Close resources
        try {
            if (resultSet != null) resultSet.close();
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    return isAdded;
}


    public boolean updateUser(String MaNV, String TenNV, String MaKho, String UserName, String MatKhau, int Maca) {
    boolean isUpdated = false;
    Connection conn = null;
    PreparedStatement stmt = null;
    ResultSet resultSet = null;
    try {
        // Initialize the connection
        conn = DBConnection.Instance().DBConnect();

        // Check if user exists
        String query = "SELECT * FROM NhanVien WHERE MaNV = ?";
        stmt = conn.prepareStatement(query);
        stmt.setString(1, MaNV);
        resultSet = stmt.executeQuery();

        if (!resultSet.next()) {
            System.out.println("User is not already available!");
        } else {
            // User exists, proceed with update
            query = "UPDATE NhanVien SET TenNV = ?, MaKho = ?, UserName = ?, MatKhau = ?, Maca = ? WHERE MaNV = ?";
            stmt = conn.prepareStatement(query);

            // Set the parameters
            stmt.setString(1, TenNV);
            stmt.setString(2, MaKho);
            stmt.setString(3, UserName);
            stmt.setString(4, MatKhau);
            stmt.setInt(5, Maca);
            stmt.setString(6, MaNV);

            int affectedRows = stmt.executeUpdate();

            if (affectedRows > 0) {
                isUpdated = true;
                System.out.println("A record was updated successfully!");
            } else {
                System.out.println("Cannot update the record with MaNV: " + MaNV);
            }
        }
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    } finally {
        // Close resources
        try {
            if (resultSet != null) resultSet.close();
            if (stmt != null) stmt.close();
            if (conn != null) conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    return isUpdated;
}


}
