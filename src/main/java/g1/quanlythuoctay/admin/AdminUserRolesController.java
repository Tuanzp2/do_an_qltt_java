/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.util.MyComboBox;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class AdminUserRolesController implements Initializable {

    @FXML
    Button bt_Logout;
    @FXML
    Button bt_New;
    @FXML
    Button bt_Assign;
    @FXML
    TextField tf_StaffName;
    @FXML
    TextField tf_RoleName;
    @FXML
    ScrollPane sc_userroles;
    @FXML
    ScrollPane sc_adminroles;
    @FXML
    GridPane gr_userroles;
    @FXML
    MyComboBox cbostaffname;
    @FXML
    MyComboBox cborolename;
    @FXML
    Label lb_TenNV;
    @FXML
    Label lb_TenVT;
    @FXML
    Label UserName;
    @FXML
    Label LoginScreen;
    public static adminUserRoles oldItem = null;
    public static int tempindex;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //load data from table category
        HashSet<adminUserRoles> data = g1.quanlythuoctay.admin.adminUserRolesRepository.Instance().findAll();
        int index = 0;
        for (adminUserRoles item : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("admin/rowpaneadminUserRoles" + ".fxml"));
            try {
                GridPane contentadminStore = fxmlLoader.load();//get root tag fxml
                RowpaneadminUserRolesController controller = fxmlLoader.getController();
                controller.LoadData(index, item);
                gr_userroles.addRow(index, contentadminStore);

                index++;
            } catch (IOException ex) {
                Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        HashSet<adminUser> hs = adminUserRepository.Instance().findAll();
        cbostaffname.setTitle("Choose Staffname");
//        cbostaffname.setDisplayProperty("TenNV");
//        cbostaffname.setValueProperty("TenNV");
        cbostaffname.setDatasource(hs);
        cbostaffname.Bind();
        cbostaffname.setOnAction(et -> {

            Object selectedItem = cbostaffname.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem instanceof adminUser) {
                adminUser selectedUser = (adminUser) selectedItem;
                tf_StaffName.setText(selectedUser.getTenNV());
                UserName.setText(selectedUser.getUserName());
                // You can access other properties of selectedUser as needed
            }

//    
        });

        HashSet<adminRole> ls = adminRoleRepository.Instance().findAll();
        cborolename.setTitle("Choose Rolename");
//        cborolename.setDisplayProperty("TenVT");
//        cborolename.setValueProperty("TenVT");
        cborolename.setDatasource(ls);
        cborolename.Bind();
        cborolename.setOnAction(et
                -> {
            Object selectedItem = cborolename.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem instanceof adminRole) {
                adminRole selectedUser = (adminRole) selectedItem;
                tf_RoleName.setText(selectedUser.getTenVT());
                LoginScreen.setText(selectedUser.getTenGiaoDien());
                // You can access other properties of selectedUser as needed
            }
        });

        bt_New.addEventHandler(ActionEvent.ACTION, e -> {
                tf_StaffName.setText("");
                tf_RoleName.setText("");
            });
            bt_Assign.addEventHandler(ActionEvent.ACTION, eh -> {
                System.out.println("button new is clicked!");

                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("admin/rowpaneadminUserRoles" + ".fxml"));
                try {
                    GridPane contentadminStore = fxmlLoader.load();//get root tag fxml
                    RowpaneadminUserRolesController controller = fxmlLoader.getController();

                    adminUserRoles newitem = new adminUserRoles();
                    newitem.setTenNV(tf_StaffName.getText());
                    newitem.setTenVT(tf_RoleName.getText());
                    newitem.setUserName(UserName.getText());
                    newitem.setTenGiaoDien(LoginScreen.getText());

                    int lastRowIndex = findLastRowIndex(gr_userroles);
                    controller.LoadData(lastRowIndex, newitem);
                    gr_userroles.addRow(lastRowIndex, contentadminStore);
                    adminUserRolesRepository repository = adminUserRolesRepository.Instance();
                    boolean isadded = repository.newUserRoles(newitem);

                } catch (IOException ex) {
                    Logger.getLogger(AdminMainController.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

        }

    public void LoadData(adminUserRoles itemEdit) {

        tf_StaffName.setText(itemEdit.getTenNV());
        tf_RoleName.setText(itemEdit.getTenVT());
        oldItem = itemEdit;
    }

    public void deleteRow(int index) {
        gr_userroles.getChildren().remove(index);
    }

    public void renewIndex() {
        int index = 1;
        for (Node node : gr_userroles.getChildren()) {
            //parse node to RowPaneCategory
            GridPane rowItem = (GridPane) node;
            Label lblNo = (Label) rowItem.getChildren().get(0);
            lblNo.setText(String.valueOf(index));
            index++;
            //change label index
        }
    }

    private int findLastRowIndex(GridPane gridPane) {
        int lastRowIndex = 0;
        for (Node child : gridPane.getChildren()) {
            // Get the gridpane's row index for the child, default to 0 if not set
            Integer rowIndex = GridPane.getRowIndex(child);
            if (rowIndex != null) {
                lastRowIndex = Math.max(lastRowIndex, rowIndex);
            }
        }
        return lastRowIndex + 1; // Return the next row index (one after the last non-empty)
    }

//   
    @FXML
    private void handleBackToLoginAction(ActionEvent event) {
        System.out.println("logout");
        try {

            // Load the login scene FXML
            Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

            // Get the current stage from the event source
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Set the login scene to the stage
            stage.setScene(new Scene(loginRoot));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            // handle exception
        }
    }

}
