/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

/**
 *
 * @author MININT-QHS5KT4-local
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import g1.quanlythuoctay.util.DBConnection;

/**
 *
 * @author khiemle
 */
public class adminStoreRepository {

    private static adminStoreRepository instance = null;

    private adminStoreRepository() {
    }

    public static adminStoreRepository Instance() {
        return instance == null ? instance = new adminStoreRepository() : instance;
    }

    /**
     * *
     * get all category
     */
    public HashSet<adminStore> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<adminStore> ls = new HashSet<adminStore>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM " + adminStore.STORE_TBL);
            // Now do something with the ResultSet ....
            while (rs.next()) {
                adminStore item = new adminStore();
                item.setMaKho(rs.getString(adminStore.ID_COL));
                item.setTenKho(rs.getString(adminStore.Name_COL));
                item.setKhoTong(rs.getInt(adminStore.TK_COL));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

    public boolean newStore(adminStore newItem) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                String strQuery = "insert into KHO values(?,?)";
                stmt = conn.prepareStatement(strQuery);
               
                stmt.setString(1, newItem.getTenKho());
                stmt.setInt(2, newItem.getKhoTong());
                boolean ck=stmt.execute();
                return ck;
                //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                // Now do something with the ResultSet ....

            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }
    
   public boolean updateStore(adminStore newItem) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
        conn = DBConnection.Instance().DBConnect();
        String query = "SELECT * FROM KHO WHERE MaKho=?";
        stmt = conn.prepareStatement(query);
        stmt.setString(1, newItem.getMaKho());
        ResultSet resultSet = stmt.executeQuery();
        if (!resultSet.next()) {
            System.out.println("Store is not available!");
            return false; // If store is not available, return false immediately.
        } else {
            String strQuery = "UPDATE KHO SET TenKho=?, KhoTong=? WHERE MaKho=?";
            stmt.close(); // Close the previous statement before reassigning.
            stmt = conn.prepareStatement(strQuery);
            stmt.setString(1, newItem.getTenKho());
            stmt.setInt(2, newItem.getKhoTong());
            stmt.setString(3, newItem.getMaKho());
            
            int affectedRows = stmt.executeUpdate(); // Use executeUpdate for DML statements
            return affectedRows > 0; // If at least one row is affected, the update was successful.
        }
    } catch (SQLException ex) {
        // handle any errors
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        return false;
    } finally {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException sqlEx) { /* ignore */ }
        }
        if (conn != null) {
            try {
                conn.close(); // Don't forget to close the connection as well
            } catch (SQLException sqlEx) { /* ignore */ }
        }
    }
}


    public boolean deleteStore(String MaKho){
    try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                String strQuery = "delete from "+adminStore.STORE_TBL+" where "+adminStore.ID_COL+"=?";
                stmt = conn.prepareStatement(strQuery);
                
                stmt.setString(1, MaKho);
               boolean ck= stmt.execute();
                //getGenerateKeys return primary key increase value
                
                return ck;
                // Now do something with the ResultSet ....

            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }
}
