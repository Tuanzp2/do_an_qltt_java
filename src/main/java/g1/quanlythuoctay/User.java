/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class User {

    private String _MaNV;
    private String _TenNV;
    private String _MaKho;
    private String _UserName;
    private String _MatKhau;
    private int _Maca;
    public static String TBL_User="NhanVien";
    public static String idNV="MaNV";
    public static String nameNV="TenNV";
    public static String idKho="MaKho";
    public static String usr="UserName";
    public static String mk="MatKhau";
    public static String idCa="MaCa";

    public User() {
        // Constructor body here
    }

    /**
     * @return the _id
     */
    public String getMaNV() {
        return _MaNV;
    }

    /**
     * @param _id the _id to set
     */
    public void setMaNV(String _MaNV) {
        this._MaNV = _MaNV;
    }

    /**
     * @return the _cat_name
     */
    public String getTenNV() {
        return _TenNV;
    }

    /**
     * @param _cat_name the _cat_name to set
     */
    public void setTenNV(String _TenNV) {
        this._TenNV = _TenNV;
    }
    public String getMaKho() {
        return _MaKho;
    }

    /**
     * @param _cat_name the _cat_name to set
     */
    public void setMaKho(String _MaKho) {
        this._MaKho = _MaKho;
    }
    /**
     * @return the _active
     */
    public int getMaca(int _Maca) {
        return _Maca;
    }

   
    public void setMaca(int _Maca) {
        this._Maca =_Maca;
    }

    public String getUserName() {
        return _UserName;
    }
    public void setUserName(String _UserName) {
        this._UserName = _UserName;
    }
    
    public String getMatKhau() {
        return _MatKhau;
    }
    public void setMatKhau(String _MatKhau) {
        this._MatKhau = _MatKhau;
    }
    
}