/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

import g1.quanlythuoctay.Repositories.CTHoaDonRepository;
import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class ChiTietHD {
    private String MaChiTietHD;
    private String MaHD;
    private String MaSP;
    private String MaTK;
    private String MaKho;
    private int SLMua;
    private String TenSP;
    private String MaNV;
    private String TenNV;
    private Double GiaGoc;
    private String ngayHD;
    public static String TBL_CTHD = "ChiTietHoaDon";
    public static String ID_CHITIETHD = "MaChiTietHoaDon";
    public static String ID_HD = "MaHD";
    public static String ID_SP = "MaSP";
    public static String ID_TK = "MaTK";
    public static String quantity_buy = "SoLuongMua";

    public ChiTietHD() {
    }

    public ChiTietHD(String MaChiTietHD, String MaHD, String MaSP, String MaTK, int SLMua) {
        this.MaChiTietHD = MaChiTietHD;
        this.MaHD = MaHD;
        this.MaSP = MaSP;
        this.MaTK = MaTK;
        this.SLMua = SLMua;
    }

    /**
     * @return the MaChiTietHD
     */
    public String getMaChiTietHD() {
        return MaChiTietHD;
    }

    /**
     * @param MaChiTietHD the MaChiTietHD to set
     */
    public void setMaChiTietHD(String MaChiTietHD) {
        this.MaChiTietHD = MaChiTietHD;
    }

    /**
     * @return the MaHD
     */
    public String getMaHD() {
        return MaHD;
    }

    /**
     * @param MaHD the MaHD to set
     */
    public void setMaHD(String MaHD) {
        this.MaHD = MaHD;
    }

    /**
     * @return the MaSP
     */
    public String getMaSP() {
        return MaSP;
    }

    /**
     * @param MaSP the MaSP to set
     */
    public void setMaSP(String MaSP) {
        this.MaSP = MaSP;
    }

    /**
     * @return the MaTK
     */
    public String getMaTK() {
        return MaTK;
    }

    /**
     * @param MaTK the MaTK to set
     */
    public void setMaTK(String MaTK) {
        this.MaTK = MaTK;
    }

    /**
     * @return the SLMua
     */
    public int getSLMua() {
        return SLMua;
    }

    /**
     * @param SLMua the SLMua to set
     */
    public void setSLMua(int SLMua) {
        this.SLMua = SLMua;
    }

    /**
     * @return the TenSP
     */
    public String getTenSP() {
        return TenSP;
    }

    /**
     * @param TenSP the TenSP to set
     */
    public void setTenSP(String TenSP) {
        this.TenSP = TenSP;
    }

    /**
     * @return the TenNV
     */
    /**
     * @return the MaNV
     */
    public String getMaNV() {
        return MaNV;
    }

    /**
     * @param MaNV the MaNV to set
     */
    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    /**
     * @return the TenNV
     */
    public String getTenNV() {
        return TenNV;
    }

    /**
     * @param TenNV the TenNV to set
     */
    public void setTenNV(String TenNV) {
        this.TenNV = TenNV;
    }

    /**
     * @return the GiaGoc
     */
    public Double getGiaGoc() {
        return GiaGoc;
    }

    /**
     * @param GiaGoc the GiaGoc to set
     */
    public void setGiaGoc(Double GiaGoc) {
        this.GiaGoc = GiaGoc;
    }

    /**
     * @return the MaKho
     */
    public String getMaKho() {
        return MaKho;
    }

    /**
     * @param MaKho the MaKho to set
     */
    public void setMaKho(String MaKho) {
        this.MaKho = MaKho;
    }
        /**
     * @return the ngayHD
     */
    public String getNgayHD() {
        return ngayHD;
    }

    /**
     * @param ngayHD the ngayHD to set
     */
    public void setNgayHD(String ngayHD) {
        this.ngayHD = ngayHD;
    }
}
