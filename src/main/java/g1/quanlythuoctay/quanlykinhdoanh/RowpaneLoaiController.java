/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class RowpaneLoaiController implements Initializable {

     @FXML
    private GridPane gr_loai;

    @FXML
    private ImageView img_Edit;

    @FXML
    private ImageView img_Select;

    @FXML
    private ImageView img_Delete;
    
    @FXML
    private Label lb_Id;

    @FXML
    private Label lb_Loai;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
   public void LoadData(int index, loai Item) {
//        lb_Id.setText(String.valueOf(Item.getId()));
        lb_Id.setText(String.valueOf(index));
        lb_Loai.setText(Item.getTenLoai());
        
        img_Delete.setOnMouseClicked(eh -> {
            
            LoaiRepository.Instance().deleteLoai(Item);
            QuanLyKinhDoanhController.sanpham_control.deleteRowLoai(index-1);//do gr_Loai start tu 1
            QuanLyKinhDoanhController.sanpham_control.renewIndex(gr_loai);
        
        });
        img_Edit.setOnMouseClicked(eh->{
             TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Update LoaiThuoc");
//            dialog.setHeaderText("Enter your input");
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            dialog.setContentText("Cập Nhật thông tin Loai Thuoc:");
            DialogPane dialogPane = dialog.getDialogPane();
            dialogPane.setHeader(null); // Try to remove the header directly from the dialog pane
            dialogPane.getStyleClass().remove("header-panel"); // If there's a style class setting the header
            // Show the dialog and capture the input result.
            Optional<String> result = dialog.showAndWait();
            // Handle the result.
            result.ifPresent(inputValue -> {

                System.out.println("User input: " + inputValue);
              
                if (LoaiRepository.Instance().updateLoai(Item, inputValue)) {
                 
                    Item.setTenLoai(inputValue);
                    this.lb_Loai.setText(Item.getTenLoai());
                    
                }
            });
        });
        
        img_Select.setOnMouseClicked(eh->{
        QuanLyKinhDoanhController.sanpham_control.setLoaiTextField(Item.getTenLoai());
        });
    } 
}
