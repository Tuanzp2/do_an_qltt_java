/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.App;
import static g1.quanlythuoctay.quanlykinhdoanh.SanphamController.tempindex;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class RowpaneNhacungungController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ImageView img_Delete;

    @FXML
    private ImageView img_Edit;

    @FXML
    private ImageView img_Select;

    @FXML
    private Label lb_Id;

    @FXML
    private Label lb_Nhacungung;

    @FXML
    private GridPane gr_Nhacungung;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void LoadData(int index, nhacungung Item) {
      tempindex=index;
        lb_Id.setText(String.valueOf(index));
        lb_Nhacungung.setText(Item.getTenNhaCungUng());

        img_Delete.setOnMouseClicked(eh -> {

            NhacungungRepository.Instance().deleteNhaCungUng(Item);
            QuanLyKinhDoanhController.sanpham_control.deleteRowNhacungung(index - 1);//do gr_Loai start tu 1
            QuanLyKinhDoanhController.sanpham_control.renewIndex(gr_Nhacungung);

        });
        img_Edit.setOnMouseClicked(eh -> {

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Update NhaCungUng");
//            dialog.setHeaderText("Enter your input");
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            dialog.setContentText("Cập Nhật thông tin Nhà Cung Ứng:");
            DialogPane dialogPane = dialog.getDialogPane();
            dialogPane.setHeader(null); // Try to remove the header directly from the dialog pane
            dialogPane.getStyleClass().remove("header-panel"); // If there's a style class setting the header
            // Show the dialog and capture the input result.
            Optional<String> result = dialog.showAndWait();
            // Handle the result.
            result.ifPresent(inputValue -> {

                System.out.println("User input: " + inputValue);
//                  
                if (NhacungungRepository.Instance().updateNhaCungUng(Item, inputValue)) {
                 
                    Item.setTenNhaCungUng(inputValue);
                    this.lb_Nhacungung.setText(Item.getTenNhaCungUng());
                    
                }
            });
        });

        img_Select.setOnMouseClicked(eh -> {
            QuanLyKinhDoanhController.sanpham_control.setNhacungungTextField(Item.getTenNhaCungUng());
        });
    }

}
