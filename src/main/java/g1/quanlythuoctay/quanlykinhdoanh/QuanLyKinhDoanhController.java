/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.App;
import static g1.quanlythuoctay.App.primaryStage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class QuanLyKinhDoanhController implements Initializable {

    
    @FXML
    private MenuItem MenuItem_ProductManager;

    @FXML
    private MenuItem Menuitem_Logout;

    @FXML
    private MenuItem Menuitem_Report;

    @FXML
    private Button bt_logout;

    @FXML
  Pane mainpane;
    static SanphamController sanpham_control = null;
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
       MenuItem_ProductManager.addEventHandler(ActionEvent.ACTION,eh->{
        System.out.println("ProductManager is clicked!");
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/quanlykinhdoanh/sanpham" + ".fxml"));
            try {
                Pane Productpane=fxmlLoader.load();//get root tag fxml
                sanpham_control = fxmlLoader.getController();
                mainpane.getChildren().setAll(Productpane);
              
            } catch (IOException ex) {
                Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        
       Menuitem_Logout.setOnAction(event -> {
    try {
        // Load the login scene FXML
        Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

        // Assuming Main.primaryStage is your static reference to the primary stage
        Stage stage = primaryStage ;

        // Set the login scene to the stage
        stage.setScene(new Scene(loginRoot));
        stage.show();
    } catch (IOException e) {
        e.printStackTrace();
        // handle exception
    }
});


  
        
    }    
//    @FXML
//    
//    private void handleBackToLoginAction(ActionEvent event) {
//    try {
//        // Load the login scene FXML
//        Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));
//
//        // Get the current stage from the event source
//        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//
//        // Set the login scene to the stage
//        stage.setScene(new Scene(loginRoot));
//        stage.show();
//    } catch (IOException e) {
//        e.printStackTrace();
//        // handle exception
//    }
//}
}
