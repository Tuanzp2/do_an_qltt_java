/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class sanpham {
    private int id;
    private String  Loai;
    private String MaThuoc;
    private String TenThuoc;
    private String NhaCungUng;
    private String Donvi;
    private float DonGiaBan;
    private int TrangThai;
    private String ChiTietSanPham;
 
    public sanpham(){};
    
    public String getChiTietSanPham(){
        return ChiTietSanPham;
    }
    
    public void setChiTietSanPham(String ChiTietSanPham){
        this.ChiTietSanPham=ChiTietSanPham;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaThuoc() {
        return MaThuoc;
    }

    public void setMaThuoc(String MaThuoc) {
        this.MaThuoc = MaThuoc;
    }

    public String getTenThuoc() {
        return TenThuoc;
    }

    public void setTenThuoc(String TenThuoc) {
        this.TenThuoc = TenThuoc;
    }

    public String getLoai() {
        return Loai;
    }

    public void setLoai(String Loai) {
        this.Loai = Loai;
    }

    public String getNhaCungUng() {
        return NhaCungUng;
    }

    public void setNhaCungUng(String NhaCungUng) {
        this.NhaCungUng = NhaCungUng;
    }

    public String getDonvi() {
        return Donvi;
    }

    public void setDonvi(String Donvi) {
        this.Donvi = Donvi;
    }

    public float getDonGiaBan() {
        return DonGiaBan;
    }

    public void setDonGiaBan(float DonGiaBan) {
        this.DonGiaBan = DonGiaBan;
    }

    public int getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(int TrangThai) {
        this.TrangThai = TrangThai;
    }
     
    
  }
