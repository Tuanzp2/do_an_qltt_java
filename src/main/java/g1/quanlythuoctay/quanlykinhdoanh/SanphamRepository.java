/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class SanphamRepository {

    private static SanphamRepository instance = null;
  
    private SanphamRepository() {
    }

    public static SanphamRepository Instance() {
        return instance == null ? instance = new SanphamRepository() : instance;
    }

    public HashSet<sanpham> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<sanpham> ls = new HashSet<sanpham>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT \n"
                    + "    SanPham.id, \n"
                    + "    LoaiSanPham.TenLoai,\n"
                    + "    SanPham.MaSP, \n"
                    + "    SanPham.TenSP,\n"
                    + "    NhaCungUng.TenNhaCungUng, \n"
                    + "    SanPham.DonVi,\n"
                    + "    SanPham.DonGiaBan, \n"
                    + "    SanPham.Trangthai,\n"
                    + "    SanPham.ChiTietSanPham\n"
                    + "FROM \n"
                    + "    SanPham \n"
                    + "LEFT JOIN \n"
                    + "    LoaiSanPham ON SanPham.MaLoai = LoaiSanPham.MaLoai\n"
                    + "LEFT JOIN \n"
                    + "    NhaCungUng ON SanPham.MaNhaCungUng = NhaCungUng.MaNhaCungUng;");
            // Now do something with the ResultSet ....
            while (rs.next()) {
                sanpham item = new sanpham();
                item.setId(rs.getInt("id"));
                item.setLoai(rs.getString("TenLoai"));
                item.setMaThuoc(rs.getString("MaSP"));
                item.setTenThuoc(rs.getString("TenSP"));
                item.setNhaCungUng(rs.getString("TenNhaCungUng"));
                item.setDonvi(rs.getString("DonVi"));
                item.setDonGiaBan(Float.parseFloat(rs.getString("DonGiaBan")));
                item.setChiTietSanPham(rs.getString("ChiTietSanPham"));
                item.setTrangThai(rs.getInt("Trangthai"));

                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
   
    public boolean newSanpham(sanpham newItem) {
        try {
            Connection conn = null;
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select * from SanPham left join LoaiSanPham ON SanPham.MaLoai=LoaiSanPham.Maloai where TenSP=? and TenLoai=?;";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, newItem.getTenThuoc());
                stmt.setString(2, newItem.getLoai());
                ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    System.out.println("Thuoc da co!");
                    return false; // If store is not available, return false immediately.
                } else {
                    String strQuery = "INSERT INTO SanPham(TenSP, MaNhaCungUng, MaLoai, ChiTietSanPham, DonGiaBan, DonVi, TrangThai) "
                            + "VALUES ("
                            + " ?, " // Removed single quotes around the placeholder
                            + " (SELECT MaNhaCungUng FROM NhaCungUng WHERE TenNhaCungUng = ?),"
                            + " (SELECT MaLoai FROM LoaiSanPham WHERE TenLoai = ?),"
                            + " ?,"
                            + " ?,"
                            + " ?,"
                            + " ?)";
                    stmt.close();
                    stmt = conn.prepareStatement(strQuery);
                    stmt.setString(1, newItem.getTenThuoc());
                    stmt.setString(2, newItem.getNhaCungUng());
                    stmt.setString(3, newItem.getLoai());
                    stmt.setString(4, newItem.getChiTietSanPham());
                    stmt.setString(5, String.valueOf(newItem.getDonGiaBan()));
                    stmt.setString(6, newItem.getDonvi());
                    stmt.setInt(7, newItem.getTrangThai());

                   int affectedRows = stmt.executeUpdate();
                    return affectedRows > 0;
                    //getGenerateKeys return primary key increase value
//              int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public String getMaSP(String TenSP){
        String MaSP;
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select MaSP from SanPham where TenSP= ?";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, TenSP);

                ResultSet resultSet = stmt.executeQuery();
                if (!resultSet.next()) {
                    System.out.println("Ten thuốc này không có trong danh mục!");
                     // If store is not available, return false immediately.
                } else {
                    MaSP=resultSet.getString("MaSP");
                    return MaSP;
                    //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return "";
    
    }
    
    public boolean deleteSanPham(sanpham deleteitem) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select * from SanPHam where MaSP= ?";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, deleteitem.getMaThuoc());

                ResultSet resultSet = stmt.executeQuery();
                if (!resultSet.next()) {
                    System.out.println("Thuốc này không có trong danh mục!");
                    return false; // If store is not available, return false immediately.
                } else {
                    String strQuery = "Delete from SanPham where MaSP=?";
                    stmt = conn.prepareStatement(strQuery);
                    stmt.setString(1, deleteitem.getMaThuoc());

                    boolean ck = stmt.execute();
                    return ck;
                    //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean updateSanpham(sanpham newItem) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = DBConnection.Instance().DBConnect();
            String query = "SELECT * FROM SanPham WHERE MaSP=?";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, newItem.getMaThuoc());
            ResultSet resultSet = stmt.executeQuery();
            if (!resultSet.next()) {
                System.out.println("Product is not available!");
                return false; // If store is not available, return false immediately.
            } else {
                String strQuery = "UPDATE SanPham \n"
                        + "SET \n"
                        + "    TenSP = ?, \n"
                        + "    MaNhaCungUng = (SELECT MaNhaCungUng FROM NhaCungUng WHERE TenNhaCungUng = ?),\n"
                        + "    MaLoai = (SELECT MaLoai FROM LoaiSanPham WHERE TenLoai = ?),\n"
                        + "    ChiTietSanPham = ?,\n"
                        + "    DonGiaBan = ?,\n"
                        + "    DonVi = ?,\n"
                        + "    TrangThai = ?\n"
                        + "WHERE \n"
                        + "    MaSP = ?;";
                stmt.close(); // Close the previous statement before reassigning.
                stmt = conn.prepareStatement(strQuery);
                stmt.setString(1, newItem.getTenThuoc());
                stmt.setString(2, newItem.getNhaCungUng());
                stmt.setString(3, newItem.getLoai());
                stmt.setString(4, newItem.getChiTietSanPham());
                stmt.setString(5, String.valueOf(newItem.getDonGiaBan()));
                stmt.setString(6, newItem.getDonvi());
                stmt.setString(7, String.valueOf(newItem.getTrangThai()));
                stmt.setString(8, newItem.getMaThuoc());
               
                int affectedRows = stmt.executeUpdate(); // Use executeUpdate for DML statements
                return affectedRows > 0; // If at least one row is affected, the update was successful.
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                    /* ignore */ }
            }
            if (conn != null) {
                try {
                    conn.close(); // Don't forget to close the connection as well
                } catch (SQLException sqlEx) {
                    /* ignore */ }
            }
        }
    }

}
