/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class LoaiRepository {
   
      private static LoaiRepository instance = null;

    private LoaiRepository() {
    }

    public static LoaiRepository Instance() {
        return instance == null ? instance = new LoaiRepository() : instance;
    }
    
    public HashSet<loai> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<loai> ls = new HashSet<loai>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from LoaiSanPham");
            // Now do something with the ResultSet ....
            while (rs.next()) {
                loai item = new loai();
                item.setId(rs.getInt("id"));
                item.setMaLoai(rs.getString("MaLoai"));
                item.setTenLoai(rs.getString("TenLoai"));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }

//    public boolean newLoai(loai newItem) {
//        try {
//            Connection conn = DBConnection.Instance().DBConnect();
//            PreparedStatement stmt = null;
//            try {
//                conn = DBConnection.Instance().DBConnect();
//                String query = "Select * from LoaiSanPham where TenLoai=?";
//                stmt = conn.prepareStatement(query);
//                stmt.setString(1, newItem.getTenLoai());
//                
//                ResultSet resultSet = stmt.executeQuery();
//                if (resultSet.next()) {
//                    System.out.println("Loai da co!");
//                    return false; 
//                } else {
//                    String strQuery = "Insert into LoaiSanPham (TenLoai) values (?) ";
//                            
//                    stmt = conn.prepareStatement(strQuery);
//                    stmt.setString(1, newItem.getTenLoai());
//                    boolean ck = stmt.execute();
//                    return ck;
//                    //getGenerateKeys return primary key increase value
//                int idcate = stmt.getGeneratedKeys().getInt("Id");
//                
//                    // Now do something with the ResultSet ....
//                }
//            } catch (SQLException ex) {
//                // handle any errors
//                System.out.println("SQLException: " + ex.getMessage());
//                System.out.println("SQLState: " + ex.getSQLState());
//                System.out.println("VendorError: " + ex.getErrorCode());
//            } finally {
//
//                if (stmt != null) {
//                    try {
//                        stmt.close();
//                    } catch (SQLException sqlEx) {
//                    } // ignore
//
//                    stmt = null;
//                }
//            }
//        } catch (Exception e) {
//        }
//        return false;
//    }
public int newLoai(String TenLoai) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
        conn = DBConnection.Instance().DBConnect();
        String query = "Select * from LoaiSanPham where TenLoai=?";
        stmt = conn.prepareStatement(query);
        stmt.setString(1, TenLoai);
        
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            System.out.println("Loai da co!");
            return -1; // Item already exists
        } else {
            String strQuery = "Insert into LoaiSanPham (TenLoai) values (?)";
            stmt = conn.prepareStatement(strQuery, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, TenLoai);

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1); // Return the generated ID
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }
    } catch (SQLException ex) {
        // Handle any errors
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        return -1; // SQL error occurred
    } finally {
        // Clean up JDBC resources
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException sqlEx) { /* Ignored */ }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException sqlEx) { /* Ignored */ }
        }
    }
}

    public boolean deleteLoai(loai deleteitem) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select * from LoaiSanPham where TenLoai= ?";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, deleteitem.getTenLoai());

                ResultSet resultSet = stmt.executeQuery();
                if (!resultSet.next()) {
                    System.out.println("Loại này không có trong danh mục!");
                    return false; // If Loai is not available, return false immediately.
                } else {
                    String strQuery = "Delete from LoaiSanPham where TenLoai=?";
                    stmt = conn.prepareStatement(strQuery);
                    stmt.setString(1, deleteitem.getTenLoai());

                    boolean ck = stmt.execute();
                    return ck;
                    //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean updateLoai(loai Item,String newTenLoai) {
        Connection conn = null;
        String MaLoai;
        PreparedStatement stmt = null;
        try {
            conn = DBConnection.Instance().DBConnect();
            String query = "SELECT MaLoai FROM LoaiSanPham WHERE TenLoai=?";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, Item.getTenLoai());
            ResultSet resultSet = stmt.executeQuery();
            if (!resultSet.next()) {
                System.out.println("Loai is not available!");
                return false; // If store is not available, return false immediately.
            } else {
                MaLoai=resultSet.getString("MaLoai");
                String strQuery = "UPDATE LoaiSanPham SET TenLoai = ? WHERE MaLoai = ?;";
                stmt.close(); // Close the previous statement before reassigning.
                stmt = conn.prepareStatement(strQuery);
                stmt.setString(1, newTenLoai);
                stmt.setString(2, MaLoai);
               
                int affectedRows = stmt.executeUpdate(); // Use executeUpdate for DML statements
                return affectedRows > 0; // If at least one row is affected, the update was successful.
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                    /* ignore */ }
            }
            if (conn != null) {
                try {
                    conn.close(); // Don't forget to close the connection as well
                } catch (SQLException sqlEx) {
                    /* ignore */ }
            }
        }
    }
    
}
