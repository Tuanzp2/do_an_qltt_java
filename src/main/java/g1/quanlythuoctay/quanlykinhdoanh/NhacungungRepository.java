/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class NhacungungRepository {
    
    private static NhacungungRepository instance = null;

    private NhacungungRepository() {
    }

    public static NhacungungRepository Instance() {
        return instance == null ? instance = new NhacungungRepository() : instance;
    }
    
    public HashSet<nhacungung> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<nhacungung> ls = new HashSet<nhacungung>();
        Connection conn = DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from NhaCungUng");
            // Now do something with the ResultSet ....
            while (rs.next()) {
                nhacungung item = new nhacungung();
                item.setId(rs.getInt("id"));
                item.setMaNhaCungUng(rs.getString("MaNhaCungUng"));
                item.setTenNhaCungUng(rs.getString("TenNhaCungUng"));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }


public int newNhacungung(String TenNhaCungUng) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
        conn = DBConnection.Instance().DBConnect();
        String query = "Select * from LoaiSanPham where TenLoai=?";
        stmt = conn.prepareStatement(query);
        stmt.setString(1, TenNhaCungUng);
        
        ResultSet resultSet = stmt.executeQuery();
        if (resultSet.next()) {
            System.out.println("NhaCungUng da co!");
            return -1; // Item already exists
        } else {
            String strQuery = "Insert into NhaCungUng (TenNhaCungUng) values (?)";
            stmt = conn.prepareStatement(strQuery, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, TenNhaCungUng);

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1); // Return the generated ID
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }
    } catch (SQLException ex) {
        // Handle any errors
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
        return -1; // SQL error occurred
    } finally {
        // Clean up JDBC resources
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException sqlEx) { /* Ignored */ }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException sqlEx) { /* Ignored */ }
        }
    }
}

    public boolean deleteNhaCungUng(nhacungung deleteitem) {
        try {
            Connection conn = DBConnection.Instance().DBConnect();
            PreparedStatement stmt = null;
            try {
                conn = DBConnection.Instance().DBConnect();
                String query = "Select * from NhaCungUng where TenNhaCungUng= ?";
                stmt = conn.prepareStatement(query);
                stmt.setString(1, deleteitem.getTenNhaCungUng());

                ResultSet resultSet = stmt.executeQuery();
                if (!resultSet.next()) {
                    System.out.println("NhaCungUng này không có trong danh mục!");
                    return false; // If Loai is not available, return false immediately.
                } else {
                    String strQuery = "Delete from NhaCungUng where TenNhaCungUng=?";
                    stmt = conn.prepareStatement(strQuery);
                    stmt.setString(1, deleteitem.getTenNhaCungUng());

                    boolean ck = stmt.execute();
                    return ck;
                    //getGenerateKeys return primary key increase value
//                int idCate = stmt.getGeneratedKeys().getInt(Category.ID_COL);
//                return idCate;
                    // Now do something with the ResultSet ....
                }
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            } finally {

                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) {
                    } // ignore

                    stmt = null;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean updateNhaCungUng(nhacungung Item, String NewTenNhaCungUng) {
        Connection conn = null;
        String MaNhaCungUng;
        PreparedStatement stmt = null;
        try {
            conn = DBConnection.Instance().DBConnect();
            String query = "SELECT MaNhaCungUng FROM NhaCungUng WHERE TenNhaCungUng=?";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, Item.getTenNhaCungUng());
            ResultSet resultSet = stmt.executeQuery();
            if (!resultSet.next()) {
                System.out.println("NhaCungUng is not available!");
                return false; // If store is not available, return false immediately.
            } else {
                MaNhaCungUng=resultSet.getString("MaNhaCungUng");
                String strQuery = "UPDATE NhaCungUng SET TenNhaCungUng = ? WHERE MaNhaCungUng = ?;";
                stmt.close(); // Close the previous statement before reassigning.
                stmt = conn.prepareStatement(strQuery);
                stmt.setString(1,NewTenNhaCungUng);
                stmt.setString(2, MaNhaCungUng);
               
                int affectedRows = stmt.executeUpdate(); // Use executeUpdate for DML statements
                return affectedRows > 0; // If at least one row is affected, the update was successful.
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                    /* ignore */ }
            }
            if (conn != null) {
                try {
                    conn.close(); // Don't forget to close the connection as well
                } catch (SQLException sqlEx) {
                    /* ignore */ }
            }
        }
    }
    
}
