/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.App;
import static g1.quanlythuoctay.quanlykinhdoanh.SanphamController.tempindex;
import java.awt.Checkbox;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class RowpaneSanphamController implements Initializable {

    @FXML
    ImageView img_Delete;

    @FXML
    ImageView img_Detail;

    @FXML
    ImageView img_Edit;

    @FXML
    Label lb_DonGiaBan;

    @FXML
    Label lb_DonVi;

    @FXML
    Label lb_Loai;

    @FXML
    Label lb_MaSP;

    @FXML
    Label lb_NhaCungUng;

    @FXML
    Label lb_Stt;

    @FXML
    Label lb_TenSP;

    @FXML
    CheckBox ck_Trangthai;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//         Button bt_Save = new Button("Save");
//        bt_Save.setDisable(true); // The button is disabled initially
//        bt_Save.setLayoutX(176.0); // Set the X layout position
//        bt_Save.setLayoutY(127.0); // Set the Y layout position
//        bt_Save.setPrefHeight(25.0); // Set the preferred height
//        bt_Save.setPrefWidth(65.0); // Set the preferred width
//        
//        // If you want to add an action event to the button
//        bt_Save.setOnAction(event -> {
        // Your action handling code
//            saveAction();
//        });

//        QuanLyKinhDoanhController.sanpham_control.anc_Sanpham.getChildren().add(bt_Save);
    }

    public void LoadData(int index, sanpham Item) {
        lb_Stt.setText(String.valueOf(index));
        lb_Loai.setText(Item.getLoai());
        lb_MaSP.setText(Item.getMaThuoc());
        lb_TenSP.setText(Item.getTenThuoc());
        lb_NhaCungUng.setText(Item.getNhaCungUng());
        lb_DonVi.setText(Item.getDonvi());
//        lb_ChiTietSanPham.setText(Item.getChiTietSanPham());
        ck_Trangthai.setSelected(Item.getTrangThai() == 1 ? true : false);
        lb_DonGiaBan.setText(String.valueOf(Item.getDonGiaBan()));
        img_Delete.setOnMouseClicked(eh -> {

            SanphamRepository.Instance().deleteSanPham(Item);
            QuanLyKinhDoanhController.sanpham_control.deleteRow(index - 1);
            QuanLyKinhDoanhController.sanpham_control.renewIndex(QuanLyKinhDoanhController.sanpham_control.gr_sanpham);

        });
        img_Edit.setOnMouseClicked(eh -> {
            if (QuanLyKinhDoanhController.sanpham_control.bt_New.getText() != "Add New") {
                QuanLyKinhDoanhController.sanpham_control.tf_MaSP.setStyle("-fx-background-color: grey;");
                QuanLyKinhDoanhController.sanpham_control.bt_Save.setDisable(false);
                QuanLyKinhDoanhController.sanpham_control.LoadData(Item);
                SanphamController.tempindex = index;
                
                  QuanLyKinhDoanhController.sanpham_control.bt_Save.addEventHandler(ActionEvent.ACTION, et -> {

                    System.out.println("button save is clicked!");
                    sanpham item = new sanpham();
                    item.setMaThuoc(QuanLyKinhDoanhController.sanpham_control.tf_MaSP.getText());
                    item.setTenThuoc(QuanLyKinhDoanhController.sanpham_control.tf_TenSP.getText());
                    item.setLoai(QuanLyKinhDoanhController.sanpham_control.tf_TenLoai.getText());
                    item.setNhaCungUng(QuanLyKinhDoanhController.sanpham_control.tf_TenNhaCungUng.getText());
                    item.setDonvi(QuanLyKinhDoanhController.sanpham_control.tf_DonVi.getText());
                    item.setChiTietSanPham(QuanLyKinhDoanhController.sanpham_control.tf_ChiTietSanPham.getText());
                    item.setDonGiaBan(Float.parseFloat(QuanLyKinhDoanhController.sanpham_control.tf_DonGiaBan.getText()));
                    item.setTrangThai(QuanLyKinhDoanhController.sanpham_control.ck_inActive.isSelected() ? 0 : 1);
//            SanphamRepository repository = SanphamRepository.Instance();
//            boolean isupdated = repository.updateSanpham(item);
                    if (SanphamRepository.Instance().updateSanpham(item)) {
               
                        this.lb_Loai.setText(QuanLyKinhDoanhController.sanpham_control.tf_TenLoai.getText());
                        this.lb_MaSP.setText(QuanLyKinhDoanhController.sanpham_control.tf_MaSP.getText());
                        this.lb_TenSP.setText(QuanLyKinhDoanhController.sanpham_control.tf_TenSP.getText());
                        this.lb_NhaCungUng.setText(QuanLyKinhDoanhController.sanpham_control.tf_TenNhaCungUng.getText());
                        this.lb_DonVi.setText(QuanLyKinhDoanhController.sanpham_control.tf_DonVi.getText());
                        this.lb_DonGiaBan.setText(QuanLyKinhDoanhController.sanpham_control.tf_DonGiaBan.getText());
                        this.ck_Trangthai.setSelected(!QuanLyKinhDoanhController.sanpham_control.ck_inActive.isSelected());
//                        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneSanpham" + ".fxml"));
//                        try {
//                            Node row = fxmlLoader.load(); // This should be the root node of your row
//                            RowpaneSanphamController controller = fxmlLoader.getController();
//                            controller.LoadData(tempindex, item); // Load the new data into the controller

                        // Add the new row at the correct index
//                            // Note: You may need to handle the placement of nodes within the row if they are not automatically handled by the FXML
//                            deleteRow(tempindex - 1);
//                            gr_sanpham.addRow(tempindex - 1, row);
//                            renewIndex(gr_sanpham);
//                        } catch (IOException ex) {
//                            Logger.getLogger(SanphamController.class.getName()).log(Level.SEVERE, null, ex);
//                        }
                    }
//                 if(QuanLyKinhDoanhController.sanpham_control.updatesanpham) { 
//               this.lb_Loai.setText(QuanLyKinhDoanhController.sanpham_control.tf_TenLoai.getText());
//               this.lb_MaSP.setText(QuanLyKinhDoanhController.sanpham_control.tf_MaSP.getText());
//               this.lb_TenSP.setText(QuanLyKinhDoanhController.sanpham_control.tf_TenSP.getText());
//               this.lb_NhaCungUng.setText(QuanLyKinhDoanhController.sanpham_control.tf_TenNhaCungUng.getText());
//               this.lb_DonVi.setText(QuanLyKinhDoanhController.sanpham_control.tf_DonVi.getText());
//               this.lb_DonGiaBan.setText(QuanLyKinhDoanhController.sanpham_control.tf_DonGiaBan.getText());
//               this.ck_Trangthai.setSelected(!QuanLyKinhDoanhController.sanpham_control.ck_inActive.isSelected());
//                QuanLyKinhDoanhController.sanpham_control.updatesanpham=false;
//                 }
                });
                
            } else {
                QuanLyKinhDoanhController.sanpham_control.LoadData(Item);
                QuanLyKinhDoanhController.sanpham_control.tf_MaSP.setStyle("-fx-background-color: grey;");
                QuanLyKinhDoanhController.sanpham_control.tf_MaSP.setText("AutoGen");

              

            }
        });

      
    }
}
