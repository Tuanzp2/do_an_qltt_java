/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.quanlykinhdoanh;

import g1.quanlythuoctay.App;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class SanphamController implements Initializable {

    @FXML
    public AnchorPane anc_Sanpham;

    @FXML
    public ScrollPane sc_Sanpham;

    @FXML
    private Button bt_Loai;

    @FXML
    public Button bt_New;

    @FXML
    private Button bt_Reset;

    @FXML
    private Button bt_NhaCungUng;

    @FXML
    public Button bt_Save;

    @FXML
    public GridPane gr_sanpham;

    @FXML
    private ImageView img_Active;

    @FXML
    private ImageView img_Inactive;

    @FXML
    public TextField tf_Tenthuoc;

    @FXML
    public TextField tf_DonVi;

    @FXML
    public TextField tf_MaSP;

    @FXML
    public TextField tf_TenLoai;

    @FXML
    public TextField tf_TenNhaCungUng;

    @FXML
    public TextField tf_TenSP;

    @FXML
    public TextField tf_DonGiaBan;

    @FXML
    public TextField tf_ChiTietSanPham;

    @FXML
    public CheckBox ck_inActive;

    @FXML
    private TitledPane tp_Loai;

    @FXML
    private AnchorPane anc_Loai;

    @FXML
    private GridPane gr_Loai;

    @FXML
    private Button bt_Newrow;

    @FXML
    private GridPane gr_Nhacungung;

    @FXML
    private TitledPane tp_Nhacungung;

    @FXML
    private Button bt_newNhacungung;

    private boolean isFirstClick = true;
    public static sanpham oldItem = null;
    public static int tempindex;
    static boolean updatesanpham = false;
    public int rowNo;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        HashSet<sanpham> data = g1.quanlythuoctay.quanlykinhdoanh.SanphamRepository.Instance().findAll();
        int index = 1;

        for (sanpham item : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneSanpham" + ".fxml"));
            try {
                GridPane contentsanpham = fxmlLoader.load();//get root tag fxml
                RowpaneSanphamController controller = fxmlLoader.getController();
                controller.LoadData(index, item);
                gr_sanpham.addRow(index, contentsanpham);

                index++;
            } catch (IOException ex) {
                Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        tp_Loai.setExpanded(false);
        HashSet<loai> danhsachloai = g1.quanlythuoctay.quanlykinhdoanh.LoaiRepository.Instance().findAll();
        int inx = 1;
        for (loai item : danhsachloai) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneLoai" + ".fxml"));
            try {
                GridPane contentloai = fxmlLoader.load();//get root tag fxml
                RowpaneLoaiController controller = fxmlLoader.getController();
                controller.LoadData(inx, item);
                gr_Loai.addRow(inx, contentloai);

                inx++;
            } catch (IOException ex) {
                Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//add row inside loai titledpane//
        bt_Newrow.addEventHandler(ActionEvent.ACTION, nr -> {

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Them Loai Moi");
//            dialog.setHeaderText("Enter your input");
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            dialog.setContentText("Vui Lòng Nhập Loại Thuốc:");
            DialogPane dialogPane = dialog.getDialogPane();
            dialogPane.setHeader(null); // Try to remove the header directly from the dialog pane
            dialogPane.getStyleClass().remove("header-panel"); // If there's a style class setting the header
            // Show the dialog and capture the input result.
            Optional<String> result = dialog.showAndWait();

            // Handle the result.
            result.ifPresent(inputValue -> {
                System.out.println("User input: " + inputValue);
                loai item = new loai();
                int id = LoaiRepository.Instance().newLoai(inputValue);
                if (id != -1) {
                    item.setId(id);
                    item.setTenLoai(inputValue);
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneLoai" + ".fxml"));
                    try {
                        GridPane contentloai = fxmlLoader.load();//get root tag fxml
                        RowpaneLoaiController controller = fxmlLoader.getController();
//                controller.LoadData(id, item);
                        int lastRowIndex = findLastRowIndex(gr_Loai);
                        controller.LoadData(lastRowIndex, item);
                        gr_Loai.addRow(lastRowIndex, contentloai);

                    } catch (IOException ex) {
                        Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            });
        });

        //titledpane for "NhaCungUng"//
        tp_Nhacungung.setExpanded(false);
        HashSet<nhacungung> danhsachnhacungung = g1.quanlythuoctay.quanlykinhdoanh.NhacungungRepository.Instance().findAll();
        int ind = 1;
        for (nhacungung item : danhsachnhacungung) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneNhacungung" + ".fxml"));
            try {
                GridPane contentnhacungung = fxmlLoader.load();//get root tag fxml
                RowpaneNhacungungController controller = fxmlLoader.getController();
                controller.LoadData(ind, item);
                gr_Nhacungung.addRow(ind, contentnhacungung);

                ind++;
            } catch (IOException ex) {
                Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//add row inside nhacungung titledpane//
        bt_newNhacungung.addEventHandler(ActionEvent.ACTION, nr -> {

            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Them NhaCungUng Moi");
            dialog.setHeaderText(null);
            dialog.setGraphic(null);
            dialog.setContentText("Vui Lòng Nhập Tên Nhà Cung Ứng:");
            DialogPane dialogPane = dialog.getDialogPane();
            dialogPane.setHeader(null); // Try to remove the header directly from the dialog pane
            dialogPane.getStyleClass().remove("header-panel"); // If there's a style class setting the header
            // Show the dialog and capture the input result.
            Optional<String> result = dialog.showAndWait();

            // Handle the result.
            result.ifPresent(inputValue -> {
                System.out.println("User input: " + inputValue);
                nhacungung item = new nhacungung();
                int id = NhacungungRepository.Instance().newNhacungung(inputValue);
                if (id != -1) {
                    item.setId(id);
                    item.setTenNhaCungUng(inputValue);
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneNhacungung" + ".fxml"));
                    try {
                        GridPane contentnhacungung = fxmlLoader.load();//get root tag fxml
                        RowpaneNhacungungController controller = fxmlLoader.getController();
//                controller.LoadData(id, item);
                        int lastRowIndex = findLastRowIndex(gr_Nhacungung);
                        controller.LoadData(lastRowIndex, item);
                        gr_Nhacungung.addRow(lastRowIndex, contentnhacungung);

                    } catch (IOException ex) {
                        Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            });
        });
        //new san pham-thuoc//
        bt_New.addEventHandler(ActionEvent.ACTION, eh -> {
            if (isFirstClick) {
                bt_New.setText("Add New");
                tp_Loai.setDisable(false);
                tp_Nhacungung.setDisable(false);
                tf_MaSP.setText("AutoGen");
                tf_MaSP.setStyle("-fx-background-color: grey;");
                bt_Save.setDisable(true);
                isFirstClick = false;
            } else {
                //add thêm điều kiện để add san pham: MaSP !=""; TenSP!=""...etc//
                if ((tf_MaSP.getText() != "") && (tf_TenLoai.getText() != "") && (tf_TenSP.getText() != "") && (tf_TenNhaCungUng.getText() != "") && (tf_DonVi.getText() != "") && (tf_DonGiaBan.getText() != "")) {
                    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneSanpham" + ".fxml"));
                    try {
                        GridPane contentsanpham = fxmlLoader.load();//get root tag fxml
                        RowpaneSanphamController controller = fxmlLoader.getController();

                        sanpham newitem = new sanpham();
//                    newitem.setMaThuoc(tf_MaSP.getText());
                        newitem.setTenThuoc(tf_TenSP.getText());
                        newitem.setLoai(tf_TenLoai.getText());
                        newitem.setNhaCungUng(tf_TenNhaCungUng.getText());
                        newitem.setDonvi(tf_DonVi.getText());
                        newitem.setChiTietSanPham(tf_ChiTietSanPham.getText());
                        newitem.setDonGiaBan(Float.parseFloat(tf_DonGiaBan.getText()));
                        newitem.setTrangThai(ck_inActive.isSelected() ? 0 : 1);

//                    SanphamRepository repository = SanphamRepository.Instance();
                        if (SanphamRepository.Instance().newSanpham(newitem)) {
                            newitem.setMaThuoc(SanphamRepository.Instance().getMaSP(tf_TenSP.getText()));

                            int lastRowIndex = findLastRowIndex(gr_sanpham);
                            controller.LoadData(lastRowIndex, newitem);
                            gr_sanpham.addRow(lastRowIndex, contentsanpham);
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    bt_New.setText("New");
                    isFirstClick = true;
                }
            }
        });

        bt_Reset.addEventHandler(ActionEvent.ACTION, eh -> {

            tf_DonVi.setText("");

            tf_MaSP.setText("");

            tf_TenLoai.setText("");
            tf_TenNhaCungUng.setText("");
            tf_TenSP.setText("");
            tf_DonGiaBan.setText("");
            tf_ChiTietSanPham.setText("");
//            ck_inActive.setText("");
            bt_Save.setDisable(true);
            tp_Loai.setDisable(true);
            tp_Nhacungung.setDisable(true);
            tf_MaSP.setStyle("");
            bt_Reset.setDisable(true);
            bt_New.setText("New");
            isFirstClick = true;

        });

        tf_MaSP.textProperty().addListener((observable, oldValue, newValue) -> {
            if (tf_MaSP.getText() != "") {
                bt_Reset.setDisable(false);
            } else {
                bt_Reset.setDisable(true);
            }
        });

        tf_Tenthuoc.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("you input: " + newValue);

            if (newValue != null) {
                if (newValue.isEmpty()) {
                    sc_Sanpham.setContent(gr_sanpham);
                } else {
                    GridPane searchGrid = searchGrid(newValue, 1, gr_sanpham); // Calling method on text change
                    sc_Sanpham.setContent(searchGrid);
                }
            }
        });

    }

    public void LoadData(sanpham itemEdit) {

        tf_TenLoai.setText(itemEdit.getLoai());
        tf_TenSP.setText(itemEdit.getTenThuoc());
        tf_MaSP.setText(itemEdit.getMaThuoc());
        tf_DonVi.setText(itemEdit.getDonvi());
        tf_TenNhaCungUng.setText(itemEdit.getNhaCungUng());
        tf_DonGiaBan.setText(String.valueOf(itemEdit.getDonGiaBan()));
        tf_ChiTietSanPham.setText(itemEdit.getChiTietSanPham());
        ck_inActive.setSelected(itemEdit.getTrangThai() == 0 ? true : false);
        oldItem = itemEdit;
    }

    public void deleteRow(int index) {
        gr_sanpham.getChildren().remove(index);
    }

    public void deleteRowLoai(int index) {
        gr_Loai.getChildren().remove(index);
    }

    public void deleteRowNhacungung(int index) {
        gr_Nhacungung.getChildren().remove(index);
    }

    public void renewIndex(GridPane gr) {
        int index = 1;
        for (Node node : gr.getChildren()) {
            //parse node to RowPaneCategory

            GridPane rowItem = (GridPane) node;
            Label lblNo = (Label) rowItem.getChildren().get(0);
            lblNo.setText(String.valueOf(index));
            index++;

            //change label index
        }
    }

    private int findLastRowIndex(GridPane gridPane) {
        int lastRowIndex = 0;
        for (Node child : gridPane.getChildren()) {
            // Get the gridpane's row index for the child, default to 0 if not set
            Integer rowIndex = GridPane.getRowIndex(child);
            if (rowIndex != null) {
                lastRowIndex = Math.max(lastRowIndex, rowIndex);
            }
        }
        return lastRowIndex + 1; // Return the next row index (one after the last non-empty)
    }

    // Inside QuanLyKinhDoanhController
    public void setLoaiTextField(String text) {
        tf_TenLoai.setText(text);
    }

    public void setNhacungungTextField(String text) {
        tf_TenNhaCungUng.setText(text);
    }

    public GridPane searchGrid(String searchText, int colId, GridPane grid) {
        GridPane searchGrid = new GridPane();

        // Index for rows in the new grid

        for (Node node : grid.getChildren()) {
            // Check if node is a GridPane (or change as per your actual structure)
            if (node instanceof GridPane) {
                GridPane rowItem = (GridPane) node;

                // Check if the row has enough children
                if (rowItem.getChildren().size() > colId) {
                    Node targetNode = rowItem.getChildren().get(colId);

                    // Check if the targetNode is a Label and contains the search text
                    if (targetNode instanceof Label && ((Label) targetNode).getText().contains(searchText)) {
                        // Clone the entire row and add it to searchGrid
//                        GridPane clonedRow = cloneGridPaneRow(rowItem);
                    sanpham item=clonesanpham(rowItem);

                        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("quanlykinhdoanh/rowpaneSanpham" + ".fxml"));
                        try {
                            GridPane contentsanpham = fxmlLoader.load();//get root tag fxml
                            RowpaneSanphamController controller = fxmlLoader.getController();
                            controller.LoadData(rowNo, item);
                            searchGrid.addRow(rowNo, contentsanpham);

                           
                        } catch (IOException ex) {
                            Logger.getLogger(QuanLyKinhDoanhController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                                            
                    }
                }
            }
        }

        return searchGrid;
    }

    private GridPane cloneGridPaneRow(GridPane rowItem) {
        GridPane clonedRow = new GridPane();
        for (Node node : rowItem.getChildren()) {
            // Handle potential nulls for row and column indices
            Integer colIndex = GridPane.getColumnIndex(node);
            Integer rowIndex = GridPane.getRowIndex(node);

            // Default to 0 if null
            int col = colIndex != null ? colIndex : 0;
            int row = rowIndex != null ? rowIndex : 0;

            if (node instanceof Label) {
                clonedRow.add(new Label(((Label) node).getText()), col, row);
            }
            // Add cloning for other types as necessary
        }
        return clonedRow;
    }

    private sanpham clonesanpham(GridPane rowItem) {
        sanpham item = new sanpham();
        String s;
        for (Node node : rowItem.getChildren()) {
            // Handle potential nulls for row and column indices
            Integer colIndex = GridPane.getColumnIndex(node);
            Integer rowIndex = GridPane.getRowIndex(node);

            // Default to 0 if null
            int col = colIndex != null ? colIndex : 0;
            int row = rowIndex != null ? rowIndex : 0;

            if (node instanceof Label) {
                if (col == 0) {
                    rowNo = Integer.parseInt(((Label) node).getText());
                }
                if (col == 1) {
                    item.setLoai(((Label) node).getText());
                }
                if (col == 2) {
                    item.setMaThuoc(((Label) node).getText());
                }
                if (col == 3) {
                    item.setTenThuoc(((Label) node).getText());
                }
                if (col == 4) {
                    item.setNhaCungUng(((Label) node).getText());
                }
                if (col == 5) {
                    item.setDonvi(((Label) node).getText());
                }
                if (col == 6) {
                    s=((Label) node).getText();
                    item.setDonGiaBan(Float.parseFloat(s));
                }
                if (col == 7) {
                    item.setTrangThai(((CheckBox) node).isSelected() ? 1 : 0);
                }

            }
            // Add other types as necessary
        }
        return item;
    }

}
