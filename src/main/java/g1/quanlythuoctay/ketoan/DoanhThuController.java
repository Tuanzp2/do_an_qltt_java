/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.ChiTietHD;
import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.Repositories.CTHoaDonRepository;
import g1.quanlythuoctay.Repositories.DoanhThuRepository;
import g1.quanlythuoctay.Repositories.KhoRepository;
import g1.quanlythuoctay.util.MyComboBox2;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class DoanhThuController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    MyComboBox2 cb_chinhanh;

    @FXML
    private DatePicker dp_dateEnd;

    @FXML
    private DatePicker dp_dateStart;

    @FXML
    private Text tx_doanhThu;

    @FXML
    private Text tx_tienLai;
    @FXML
    private GridPane gp_listNV;
    @FXML
    private Text tx_tienVon;
    static double total = 0.0;
    static double sl = 0.0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        setInfor();
        HashSet<HoaDon> data = DoanhThuRepository.Instance().findAll();
        HashSet<Kho> dta = KhoRepository.Instance().findAll();
        HashSet<HoaDon> da = DoanhThuRepository.Instance().findNV();
        int index = 0;
        for (HoaDon hd : da) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/RowPaneDoanhThu.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneDoanhThuController controller = fxmlLoader.getController();
//                controller.setGpOrder(gp_listNV);
                controller.LoadData(hd, index);
                gp_listNV.addRow(index, contentItem);
//                BT = this;
//                controller.setTotal(lb_total);
                index++;
            } catch (IOException ex) {
                Logger.getLogger(RowPaneDoanhThuController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        cb_chinhanh.setTitle("Tất cả");
        cb_chinhanh.setDisplayProperty("TenKho");
        cb_chinhanh.setValueProperty("MaKho");
        cb_chinhanh.setDatasource(dta);
        cb_chinhanh.Bind();
        System.out.println("aaaa" + cb_chinhanh.selectedValue().toString());
        cb_chinhanh.setOnAction((eh) -> {
            total = 0.0;

            System.out.println("a" + cb_chinhanh.selectedValue().toString());
            SetVON();
            filterDataByDate(data);

        }
        );

        dp_dateEnd.setValue(LocalDate.now());
        dp_dateEnd.addEventHandler(ActionEvent.ACTION, (eh) -> {
            total = 0.0;

            java.time.LocalDate currentDate = LocalDate.now();
            try {
                if (dp_dateEnd != null) {
                    if (dp_dateEnd.getValue().isBefore(currentDate) || dp_dateEnd.getValue().isEqual(currentDate)) {
                        if (dp_dateEnd.getValue().isAfter(dp_dateStart.getValue()) || dp_dateEnd.getValue().isEqual(dp_dateStart.getValue())) {
                            java.time.LocalDate dateEnd = dp_dateEnd.getValue();
                            System.out.println("Date End: " + dateEnd);
                            filterDataByDate(data);
                            displayFilteredData();
//                            displayFilteredData();
                        } else {
                            dp_dateEnd.setValue(dp_dateStart.getValue());
                        }
                    } else {
                        dp_dateEnd.setValue(currentDate);
                    }
                } else {
                    dp_dateEnd.setValue(currentDate);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        dp_dateStart.addEventHandler(ActionEvent.ACTION, (eh) -> {
            total = 0.0;
            sl = 0.0;
            try {
                if (dp_dateStart != null) {
                    if (dp_dateStart.getValue().isBefore(dp_dateEnd.getValue()) || dp_dateStart.getValue().isEqual(dp_dateEnd.getValue())) {
                        java.time.LocalDate dateStart = dp_dateStart.getValue();
                        System.out.println("Date Start: " + dateStart);
                        filterDataByDate(data);
                        displayFilteredData();
                    } else {
                        dp_dateStart.setValue(dp_dateEnd.getValue());
                    }
                } else {
                    dp_dateStart.setValue(dp_dateEnd.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    public void setInfor() {
        sl = 0.0;
        total = 0.0;
        HashSet<ChiTietHD> d = CTHoaDonRepository.Instance().findAll();
        HashSet<HoaDon> data = DoanhThuRepository.Instance().findAll();
        for (ChiTietHD dt : d) {

            double a = dt.getGiaGoc() * dt.getSLMua();
            sl += a;
            tx_tienVon.setText(String.valueOf(sl));
        }

        for (HoaDon dt : data) {
            total += dt.getTotal();
            tx_doanhThu.setText(String.valueOf(total));
            System.out.println("TOTAL" + total);
        }
        double lai = total - sl;

        tx_tienLai.setText(String.valueOf(lai));
    }

    public void SetVON() {
        sl = 0.0;
        HashSet<ChiTietHD> d = CTHoaDonRepository.Instance().findAll();
        for (ChiTietHD dt : d) {
            java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-dd-MM");
            java.time.LocalDate ngayHoaDon = java.time.LocalDate.parse(dt.getNgayHD(), formatter);
            System.out.println("d " + ngayHoaDon);
            System.out.println("ttt" + cb_chinhanh.selectedValue().toString());
            System.out.println("getMK" + dt.getMaKho());
            if (dt.getMaKho().equalsIgnoreCase(cb_chinhanh.selectedValue().toString())) {
                System.out.println("A " + dt.getMaKho());
                if (ngayHoaDon.isAfter(dp_dateStart.getValue()) || ngayHoaDon.isEqual(dp_dateStart.getValue())) {
                    if (ngayHoaDon.isBefore(dp_dateEnd.getValue()) || ngayHoaDon.isEqual(dp_dateEnd.getValue())) {
                        double a = dt.getGiaGoc() * dt.getSLMua();
                        sl += a;
                        tx_tienVon.setText(String.valueOf(sl));
                    }
                }
            } else {
                if (cb_chinhanh.selectedValue().toString().equalsIgnoreCase("-1")) {
                    if (ngayHoaDon.isAfter(dp_dateStart.getValue()) || ngayHoaDon.isEqual(dp_dateStart.getValue())) {
                        if (ngayHoaDon.isBefore(dp_dateEnd.getValue()) || ngayHoaDon.isEqual(dp_dateEnd.getValue())) {
                            double a = dt.getGiaGoc() * dt.getSLMua();
                            sl += a;
                            tx_tienVon.setText(String.valueOf(sl));
                        }
                    } else {
                        tx_tienVon.setText(String.valueOf(sl));
                    }
                } else {
                    
                    tx_tienVon.setText(String.valueOf(sl));
                }

            }

        }
    }

    private void filterDataByDate(HashSet<HoaDon> data) {
        for (HoaDon dt : data) {
            java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-dd-MM");
            java.time.LocalDate ngayHoaDon = java.time.LocalDate.parse(dt.getNgayHoaDon(), formatter);
            if (cb_chinhanh.selectedValue().toString().equalsIgnoreCase(dt.getMaKho())) {
                if (ngayHoaDon.isAfter(dp_dateStart.getValue()) || ngayHoaDon.isEqual(dp_dateStart.getValue())) {
                    if (ngayHoaDon.isBefore(dp_dateEnd.getValue()) || ngayHoaDon.isEqual(dp_dateEnd.getValue())) {
                        total += dt.getTotal();
                        System.out.println("total " + total);
                        tx_doanhThu.setText(String.valueOf(total));
                        System.out.println("soluong " + sl);
                        double laitien = total - sl;
                        tx_tienLai.setText(String.valueOf(laitien));
                    }
                }
            } else {
                if (cb_chinhanh.selectedValue().toString().equalsIgnoreCase("-1")) {
                    if (ngayHoaDon.isAfter(dp_dateStart.getValue()) || ngayHoaDon.isEqual(dp_dateStart.getValue())) {
                        if (ngayHoaDon.isBefore(dp_dateEnd.getValue()) || ngayHoaDon.isEqual(dp_dateEnd.getValue())) {
                            total += dt.getTotal();
                            tx_doanhThu.setText(String.valueOf(total));
                            double laitien = total - sl;
                            tx_tienLai.setText(String.valueOf(laitien));

                        }
                    } else {

                        tx_doanhThu.setText(String.valueOf(total));
                        double laitien = total - sl;
                        tx_tienLai.setText(String.valueOf(laitien));
                    }
                } else {
                    tx_doanhThu.setText(String.valueOf(total));
                    tx_tienLai.setText(String.valueOf("0.0"));
                }

            }

        }

    }
    public void displayFilteredData() {
        HoaDon.dateStart=dp_dateStart.getValue().toString();
        System.out.println("aaaaAA" +HoaDon.dateStart);
        HoaDon.dateEnd=dp_dateEnd.getValue().toString();
        gp_listNV.getChildren().clear();
        LinkedHashSet<HoaDon> filteredData = g1.quanlythuoctay.Repositories.DoanhThuRepository.Instance().findSortNV();
//                .filter(hd -> {
//                    LocalDate hdDate = parseDate(hd.getNgayHoaDon());
//                    return (hdDate.isEqual(dp_dateStart.getValue()) || hdDate.isAfter(dp_dateStart.getValue()))
//                            && (hdDate.isEqual(dp_dateEnd.getValue()) || hdDate.isBefore(dp_dateEnd.getValue()));
//                })
//                .collect(Collectors.toCollection(LinkedHashSet::new));

        int index = 0;
        for (HoaDon hd : filteredData) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/RowPaneDoanhThu.fxml"));
            try {

                GridPane contentItem = fxmlLoader.load();
                RowPaneDoanhThuController Controller = fxmlLoader.getController();
                Controller.LoadData(hd, index);
                gp_listNV.addRow(index, contentItem);
                index++;
            } catch (IOException ex) {
                Logger.getLogger(DoanhThuController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private LocalDate parseDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(dateString, formatter);
    }

}
