/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.CongNo;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class CongNoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ComboBox<?> cb_sortBy;

    @FXML
    private DatePicker dp_dateEnd;

    @FXML
    private DatePicker dp_dateStart;

    @FXML
    private GridPane gp_listCongNo;

    @FXML
    private TextField tf_search;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        LinkedHashSet<CongNo> data = g1.quanlythuoctay.Repositories.CongNoRepository.Instance().findAll();
        int index = 0;
        for (CongNo cn : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/RowPaneCongNo.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneCongNoController controller = fxmlLoader.getController();
//                controller.setGpOrder(gp_listCongNo);
                controller.LoadData(index, cn);
                gp_listCongNo.addRow(index, contentItem);
//                BT = this;
//                controller.setTotal(lb_total);
                index++;
            } catch (IOException ex) {
                Logger.getLogger(CongNoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String getMACONGNO(int index) {
        if (index >= 0 && index < gp_listCongNo.getChildren().size()) {
            Node nodeAtIndex = gp_listCongNo.getChildren().get(index);

            if (nodeAtIndex instanceof GridPane) {
                GridPane rowPane = (GridPane) nodeAtIndex;
                Text maHDLabel = (Text) rowPane.lookup("#tx_idCNO");

                if (maHDLabel != null) {
                    System.out.println(maHDLabel.getText());
                    return maHDLabel.getText();
                }
            }
        }
        return null;
    }

    public String getTENNCU(int index) {
        if (index >= 0 && index < gp_listCongNo.getChildren().size()) {
            Node nodeAtIndex = gp_listCongNo.getChildren().get(index);

            if (nodeAtIndex instanceof GridPane) {
                GridPane rowPane = (GridPane) nodeAtIndex;
                Text tenNCULabel = (Text) rowPane.lookup("#tx_idNCU");

                if (tenNCULabel != null) {
                    System.out.println(tenNCULabel.getText());
                    return tenNCULabel.getText();
                }
            }
        }
        return null;
    }
    public String getHanTra(int index) {
        if (index >= 0 && index < gp_listCongNo.getChildren().size()) {
            Node nodeAtIndex = gp_listCongNo.getChildren().get(index);

            if (nodeAtIndex instanceof GridPane) {
                GridPane rowPane = (GridPane) nodeAtIndex;
                Text deadlineLabel = (Text) rowPane.lookup("#tx_deadline");

                if (deadlineLabel != null) {
                    System.out.println(deadlineLabel.getText());
                    return deadlineLabel.getText();
                }
            }
        }
        return null;
    }
    public String getTrangThai(int index) {
        if (index >= 0 && index < gp_listCongNo.getChildren().size()) {
            Node nodeAtIndex = gp_listCongNo.getChildren().get(index);

            if (nodeAtIndex instanceof GridPane) {
                GridPane rowPane = (GridPane) nodeAtIndex;
                Text TrangThaiLabel = (Text) rowPane.lookup("#tx_TrangThai");

                if (TrangThaiLabel != null) {
                    System.out.println(TrangThaiLabel.getText());
                    return TrangThaiLabel.getText();
                }
            }
        }
        return null;
    }
    public String getTongNo(int index) {
        if (index >= 0 && index < gp_listCongNo.getChildren().size()) {
            Node nodeAtIndex = gp_listCongNo.getChildren().get(index);

            if (nodeAtIndex instanceof GridPane) {
                GridPane rowPane = (GridPane) nodeAtIndex;
                Text TongNoLabel = (Text) rowPane.lookup("#tx_TongNo");

                if (TongNoLabel != null) {
                    System.out.println(TongNoLabel.getText());
                    return TongNoLabel.getText();
                }
            }
        }
        return null;
    }
}
