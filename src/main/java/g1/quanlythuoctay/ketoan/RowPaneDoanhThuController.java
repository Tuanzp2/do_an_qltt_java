/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.HoaDon;
import g1.quanlythuoctay.Kho;
import g1.quanlythuoctay.Repositories.KhoRepository;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneDoanhThuController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Text tx_doanhThu;

    @FXML
    private Text tx_idKho;

    @FXML
    private Text tx_index;

    @FXML
    private Text tx_nameNV;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void LoadData(HoaDon hd, int index) {
        // Check if data for this MaNV has already been loaded

        tx_index.setText(String.valueOf(index + 1));
        tx_idKho.setText(String.valueOf(hd.getTenKho()));
        tx_nameNV.setText(String.valueOf(hd.getTenNV()));
        tx_doanhThu.setText(String.valueOf(hd.getTotal()));

    }

}
