package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.CongNo;
import g1.quanlythuoctay.LoginController;
import g1.quanlythuoctay.PhieuChi;
import g1.quanlythuoctay.Repositories.CongNoRepository;
import g1.quanlythuoctay.Repositories.UserRepository;
import g1.quanlythuoctay.User;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;

import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ResourceBundle;

public class RowPaneCongNoDetailController implements Initializable {

    @FXML
    private Text tx_TienConNo;
    @FXML
    private Text tx_index;
    @FXML
    private Text tx_TienThanhToan;
    @FXML
    private Text tx_idPhieu;
    @FXML
    private Text tx_nameNV;
    @FXML
    private Text tx_note;
    @FXML
    private Text tx_time;
    private Text tx_total;

    private CongNoDetailController cndc;

    public RowPaneCongNoDetailController(CongNoDetailController cndc) {
        this.cndc = cndc;
    }

    public void setTOTAL(Text tx_total) {
        this.tx_total = tx_total;
    }
    private Text tx_daChi;

    public void setConThua(Text tx_daChi) {
        this.tx_daChi = tx_daChi;
    }
    private Text tx_conLai;

    public void setConNO(Text tx_conLai) {
        this.tx_conLai = tx_conLai;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }
    public static PhieuChi x;
    public static RowPaneCongNoDetailController a;

    public RowPaneCongNoDetailController() {
        a = this;
    }
    static double tiennoconlai = 0.0;
    static double tienDaTra=0.0;
    static String id="";
    public void LoadData(PhieuChi pc, int index) {
        DecimalFormat decimalFormat = new DecimalFormat("0 VNĐ");

//        // Hiển thị giá trị số đã định dạng
//        String formattedValue = decimalFormat.format(tien);
        tx_index.setText(String.valueOf(index + 1));

        tx_idPhieu.setText(String.valueOf(pc.getMaPhieuChi()));
        id=String.valueOf(pc.getMaPhieuChi());
        tx_time.setText(String.valueOf(pc.getThoiGian()));
        tx_TienThanhToan.setText(String.valueOf(decimalFormat.format(pc.getSoTien())));

        tx_nameNV.setText(String.valueOf(pc.getNameNV()));
        tx_note.setText(String.valueOf(pc.getNote()));
        if (pc.getMaCongNo().equalsIgnoreCase(RowPaneCongNoController.maCongNo)) {
            double tienConLai = calculateCumulativeRemainingAmount(pc);
            tiennoconlai = tienConLai;
            tx_TienConNo.setText(String.valueOf(decimalFormat.format(tienConLai)));
            
            System.out.println("tex" + pc.getTienDaTra());
            tx_daChi.setText(String.valueOf(decimalFormat.format(pc.getTienDaTra())));
            tienDaTra=pc.getTienDaTra();
            double conlai = pc.getTienNo() - pc.getTienDaTra();
            tx_conLai.setText(String.valueOf(decimalFormat.format(conlai)));
        }
        x = pc;
    }

    public void LoadGrid(int index) {
        DecimalFormat decimalFormat = new DecimalFormat("0 VNĐ");
//        System.out.println("id "+id);
        String prefix = id.replaceAll("[0-9]", "");
        String numberString = id.replaceAll("[^0-9]", "");
        int number = Integer.parseInt(numberString);
        number++;
        String newNumberString = String.format("%03d", number);
        String maPhieuChi = prefix + newNumberString;
        tx_index.setText(String.valueOf(index + 1));
        tx_idPhieu.setText(String.valueOf(maPhieuChi));
        LocalDateTime currentDate = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-MM HH:mm:ss.0");
        String datetime = currentDate.format(formatter);
        tx_time.setText(String.valueOf(datetime));

        tx_TienThanhToan.setText(String.valueOf(decimalFormat.format(CongNoDetailController.tienNhap)));
        HashSet<User> user = UserRepository.Instance().findAll();
        for(User usr: user){
            if(usr.getMaNV().equalsIgnoreCase(LoginController.idNV)){
                tx_nameNV.setText(String.valueOf(usr.getTenNV()));
            }
        
        }
        
        tx_note.setText(String.valueOf(CongNoDetailController.ghiCHU));
        System.out.println("aa" +tiennoconlai);
//        double tienNhap= CongNoDetailController.tienNhap;
        tiennoconlai=tiennoconlai-CongNoDetailController.tienNhap;
        tx_TienConNo.setText(decimalFormat.format(tiennoconlai));
        tienDaTra=tienDaTra+CongNoDetailController.tienNhap;
        tx_daChi.setText(decimalFormat.format(tienDaTra));
        double conlai=Double.parseDouble(RowPaneCongNoController.TongNo)-tienDaTra;
        tx_conLai.setText(decimalFormat.format(conlai));
    }

    public static double c = 0.0;

    private double calculateCumulativeRemainingAmount(PhieuChi pc) {
        double cumulativeRemainingAmount = Double.parseDouble(RowPaneCongNoController.TongNo);
        System.out.println("tien = " + cumulativeRemainingAmount);
        System.out.println("tien " + pc.getSoTien());
        c = c + pc.getSoTien();
        cumulativeRemainingAmount -= c;

        return cumulativeRemainingAmount;
    }

}
