/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.CongNo;
import g1.quanlythuoctay.StringValue;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RowPaneCongNoController implements Initializable {

    /**
     * Initializes the controller class.
     */
        @FXML
    private Text tx_TongNo;

    @FXML
    private Text tx_TrangThai;

    @FXML
    private Text tx_deadline;

    @FXML
    private Text tx_idCNO;

    @FXML
    private Text tx_idNCU;

    @FXML
    private Text tx_idPNK;

    @FXML
    private Text tx_index;
    
    @FXML
    private Text tx_choose;
    
    static String maCongNo;
    static String tenNCU;
    static String deadlinePay;
    static String trangThai;
    static String TongNo;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }
    public void LoadData(int index, CongNo cn ){
        tx_index.setText(String.valueOf(index+1));
        tx_idCNO.setText(String.valueOf(cn.getMaCongNo()));
        tx_TongNo.setText(String.format("%.0f", cn.getTotalNo()));
        tx_deadline.setText(String.valueOf(cn.getPayDeadline()));
        tx_idNCU.setText(String.valueOf(cn.getTenNCU()));
        tx_idPNK.setText(String.valueOf(cn.getMaPhieuKho()));
        tx_TrangThai.setText(String.valueOf(cn.getTrangThai()));


        tx_choose.setOnMouseClicked((eh) -> {
            maCongNo = KeToanMainController.cnc_control.getMACONGNO(index);
            tenNCU = KeToanMainController.cnc_control.getTENNCU(index);
            deadlinePay= KeToanMainController.cnc_control.getHanTra(index);
            trangThai = KeToanMainController.cnc_control.getTrangThai(index);
            TongNo=KeToanMainController.cnc_control.getTongNo(index);
            System.out.println("ma hoa don" +maCongNo);
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.NEW_CLASS_ROOM_KETOAN_PAGE));
            try {
                System.out.println("test");
                System.out.println(StringValue.NEW_CLASS_ROOM_KETOAN_PAGE);
                AnchorPane newClassRoom = loader.load();
                CongNoDetailController control = loader.getController();
                control.init();
                Stage subStage = new Stage();
                subStage.initModality(Modality.WINDOW_MODAL);
                subStage.setScene(new Scene(newClassRoom));
                subStage.show();
                RowPaneCongNoDetailController.c=0.0;

            } catch (Exception e) {
            }
        });
        
    }
}
