/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class KeToanMainController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ScrollPane sp_mainPane;
    @FXML
    private MenuItem mn_RevenueManage;
    @FXML
    private MenuItem mn_doanhthu;

    static CongNoController cnc_control = null;
    static DoanhThuController dt_control = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        mn_RevenueManage.addEventHandler(ActionEvent.ACTION, (eh) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/CongNo.fxml"));

            try {
                AnchorPane contentCongNo = fxmlLoader.load();//get root tag fxml
                cnc_control = fxmlLoader.getController();
                sp_mainPane.setContent(contentCongNo);
//                CongNoController.BT.DeleteGrid();
            } catch (IOException ex) {
                Logger.getLogger(CongNoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        mn_doanhthu.addEventHandler(ActionEvent.ACTION, (eh) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/DoanhThu.fxml"));

            try {
                AnchorPane contentCongNo = fxmlLoader.load();//get root tag fxml
                dt_control = fxmlLoader.getController();
                sp_mainPane.setContent(contentCongNo);
//                CongNoController.BT.DeleteGrid();
            } catch (IOException ex) {
                Logger.getLogger(CongNoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

}
