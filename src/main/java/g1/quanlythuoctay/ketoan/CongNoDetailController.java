/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import g1.quanlythuoctay.App;
import g1.quanlythuoctay.LoginController;
import g1.quanlythuoctay.PhieuChi;
import g1.quanlythuoctay.Repositories.PhieuChiRepository;
import g1.quanlythuoctay.Repositories.UserRepository;
import g1.quanlythuoctay.User;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class CongNoDetailController implements Initializable {

    @FXML
    private Button btn_accept;

    @FXML
    private Button btn_reset;

    @FXML
    private GridPane gp_listPay;

    @FXML
    private TextField tf_NguoiLap;

    @FXML
    private TextField tf_NguoiNhan;

    @FXML
    private TextField tf_money;

    public String getMoney() {
        System.out.println(tf_money.getText());
        return tf_money.getText();
    }
    @FXML
    private TextField tf_note;

    @FXML
    private TextField tf_time;

    @FXML
    private Text tx_TrangThai;

    @FXML
    private Text tx_conLai;

    @FXML
    private Text tx_daChi;

    @FXML
    private Text tx_deadline;

    @FXML
    private Text tx_idCN;

    @FXML
    private Text tx_total;
    static int i = 0;
    public static double tienNhap=0.0;
    public static String ghiCHU="";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        setINFORMATION();
        LinkedHashSet<PhieuChi> data = PhieuChiRepository.Instance().findAll();
        int index = 0;
        int indexRow = 0;

        for (PhieuChi cn : data) {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/RowPaneCongNoDetail.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneCongNoDetailController controller = fxmlLoader.getController();
                controller.setConThua(tx_daChi);
//                controller.setTOTAL(tx_total);
                controller.setConNO(tx_conLai);
                controller.LoadData(cn, index);

                if (RowPaneCongNoController.maCongNo.equalsIgnoreCase(cn.getMaCongNo())) {
                    gp_listPay.addRow(indexRow, contentItem);
                    index++;
                    i = index;
                    indexRow++;
                }
            } catch (IOException ex) {
                Logger.getLogger(CongNoDetailController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        tf_money.addEventHandler(ActionEvent.ACTION, (eh) -> {

        });

        tf_note.addEventHandler(ActionEvent.ACTION, (eh) -> {

        });
        
        btn_accept.addEventHandler(ActionEvent.ACTION, (eh) -> {
            String maCN = RowPaneCongNoController.maCongNo;
            String maNV = LoginController.idNV;

            Double money = Double.valueOf(String.valueOf(tf_money.getText()));
            tienNhap=money;
            String note = String.valueOf(tf_note.getText());
            ghiCHU=note;
            LocalDateTime currentDate = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-MM HH:mm:ss");
            String datetime = currentDate.format(formatter);
            PhieuChi newPC = new PhieuChi("", maCN, money, note, maNV, datetime);
            PhieuChiRepository.Instance().InsertBill(newPC);

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/g1/quanlythuoctay/ketoan/RowPaneCongNoDetail.fxml"));
            try {
                GridPane contentItem = fxmlLoader.load();
                RowPaneCongNoDetailController controller = fxmlLoader.getController();
                controller.setConThua(tx_daChi);
                controller.setConNO(tx_conLai);
                controller.LoadGrid(i);
                gp_listPay.addRow(i, contentItem);
                i++;
            } catch (IOException ex) {
                Logger.getLogger(CongNoDetailController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    @FXML
    public void init() {
    }
//    public void renewIndex(){
//        int index=1;
//        for (Node node :  grCates.getChildren()) {
//            //parse node to RowPaneCategory
//            GridPane rowItem=(GridPane) node;
//            Label lblNo= (Label)rowItem.getChildren().get(0);
//            lblNo.setText(String.valueOf(index));
//            index++;
//            //change label index
//        }
//    }

    public void setINFORMATION() {
        DecimalFormat decimalFormat = new DecimalFormat("0 VNĐ");
        Double no = Double.valueOf(RowPaneCongNoController.TongNo);
        String id_cn = RowPaneCongNoController.maCongNo;
        tx_idCN.setText(String.valueOf(id_cn));
        tf_NguoiLap.setMouseTransparent(true);
        tf_NguoiNhan.setMouseTransparent(true);
        tf_time.setMouseTransparent(true);
        HashSet<User> user = UserRepository.Instance().findAll();
        for (User usr : user) {
            if (usr.getMaNV().equalsIgnoreCase(LoginController.idNV)) {
                tf_NguoiLap.setText(String.valueOf(usr.getTenNV()));
            }
        }
        tf_NguoiNhan.setText(RowPaneCongNoController.tenNCU);
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedDateStart = currentDate.format(formatter);
        tf_time.setText(formattedDateStart);
        tx_TrangThai.setText(String.valueOf(RowPaneCongNoController.trangThai));
        tx_deadline.setText(String.valueOf(RowPaneCongNoController.deadlinePay));
        tx_total.setText(String.valueOf(decimalFormat.format(no)));

    }
}
