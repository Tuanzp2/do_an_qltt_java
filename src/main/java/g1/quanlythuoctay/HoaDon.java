/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

import java.util.HashSet;

/**
 *
 * @author ADMIN
 */
public class HoaDon {
    private String MaHD;
    private String SDT;
    private String MaNV;
    private String NgayHoaDon;
    private String MaKho;
    private Double Total;
    private String TenKho;
    private String TenNV;
    public static String dateStart ="";
    public static String dateEnd="";
    public static String where="";
    public static String between="";
    public static String and="";
    public static String TBL_HD = "HoaDonBanHang";
    public static String ID_HD = "MaHD";
    public static String phone_num = "SDT";
    public static String ID_NV = "MaNV";
    public static String Date = "NgayHoaDon";
    public static String ID_Kho = "MaKho";
    public static String Total_HD = "TongTienHD";
    public static String Name_Kho="TenKho";
    public static String column_sort="MaHD";
    public static String Sort="";
    private HashSet<ChiTietHD> cthd;

    public HoaDon() {
        if (this.cthd == null) {
            this.cthd = new HashSet<>();
        }
    }

    public HoaDon(String MaHD, String SDT, String MaNV, String NgayHoaDon, String MaKho, Double Total) {
        this.MaHD = MaHD;
        this.SDT = SDT;
        this.MaNV = MaNV;
        this.NgayHoaDon = NgayHoaDon;
        this.MaKho = MaKho;
        this.Total = Total;
    }

    

    /**
     * b
     *
     * @return the MaHD
     */
    public String getMaHD() {
        return MaHD;
    }

    /**
     * @param MaHD the MaHD to set
     */
    public void setMaHD(String MaHD) {
        this.MaHD = MaHD;
    }

    /**
     * @return the SDT
     */
    public String getSDT() {
        return SDT;
    }

    /**
     * @param SDT the SDT to set
     */
    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    /**
     * @return the MaNV
     */
    public String getMaNV() {
        return MaNV;
    }

    /**
     * @param MaNV the MaNV to set
     */
    public void setMaNV(String MaNV) {
        this.MaNV = MaNV;
    }

    /**
     * @return the NgayHoaDon
     */
    public String getNgayHoaDon() {
        return NgayHoaDon;
    }

    /**
     * @param NgayHoaDon the NgayHoaDon to set
     */
    public void setNgayHoaDon(String NgayHoaDon) {
        this.NgayHoaDon = NgayHoaDon;
    }

    /**
     * @return the MaKho
     */
    public String getMaKho() {
        return MaKho;
    }

    /**
     * @param MaKho the MaKho to set
     */
    public void setMaKho(String MaKho) {
        this.MaKho = MaKho;
    }

    /**
     * @return the cthd
     */
    public HashSet<ChiTietHD> getCthd() {
        return cthd;
    }

    /**
     * @param cthd the cthd to set
     */
    public void setCthd(HashSet<ChiTietHD> cthd) {
        this.cthd = cthd;
    }

    public void addNewCTHD(ChiTietHD newCTHD) {
        this.cthd.add(newCTHD);
    }
        /**
     * @return the Total
     */
    public Double getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(Double Total) {
        this.Total = Total;
    }
        /**
     * @return the TenKho
     */
    public String getTenKho() {
        return TenKho;
    }

    /**
     * @param TenKho the TenKho to set
     */
    public void setTenKho(String TenKho) {
        this.TenKho = TenKho;
    }
        /**
     * @return the TenNV
     */
    public String getTenNV() {
        return TenNV;
    }

    /**
     * @param TenNV the TenNV to set
     */
    public void setTenNV(String TenNV) {
        this.TenNV = TenNV;
    }
}
