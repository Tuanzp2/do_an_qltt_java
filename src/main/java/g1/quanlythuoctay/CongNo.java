/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class CongNo {    
    private String maCongNo;
    private Double totalNo;
    private String payDeadline;
    private String maNCU;
    private String tenNCU;
    private int TrangThai;
    private String MaPhieuKho;
    private Double TienDaTra;
    public static String TBL_CongNo="CongNo";
    public static String id_CongNo="MaCongNo";
    public static String tienNo="TongNo";
    public static String hanTra="HanTra";
    public static String id_NCU="MaNhaCungUng";
    public static String trangthai="TrangThai";
    public static String id_phieuNhapKho="MaPhieuNhapKho";
    public static String name_NCU="TenNhaCungUng";
    public static String money_DaTra="TienDaTra";
    /**
     * @return the maCongNo
     */
    public String getMaCongNo() {
        return maCongNo;
    }

    /**
     * @param maCongNo the maCongNo to set
     */
    public void setMaCongNo(String maCongNo) {
        this.maCongNo = maCongNo;
    }

    /**
     * @return the totalNo
     */
    public Double getTotalNo() {
        return totalNo;
    }

    /**
     * @param totalNo the totalNo to set
     */
    public void setTotalNo(Double totalNo) {
        this.totalNo = totalNo;
    }

    /**
     * @return the payDeadline
     */
    public String getPayDeadline() {
        return payDeadline;
    }

    /**
     * @param payDeadline the payDeadline to set
     */
    public void setPayDeadline(String payDeadline) {
        this.payDeadline = payDeadline;
    }

    /**
     * @return the maNCU
     */
    public String getMaNCU() {
        return maNCU;
    }

    /**
     * @param maNCU the maNCU to set
     */
    public void setMaNCU(String maNCU) {
        this.maNCU = maNCU;
    }

    /**
     * @return the tenNCU
     */
    public String getTenNCU() {
        return tenNCU;
    }

    /**
     * @param tenNCU the tenNCU to set
     */
    public void setTenNCU(String tenNCU) {
        this.tenNCU = tenNCU;
    }

    /**
     * @return the TrangThai
     */
    public int getTrangThai() {
        return TrangThai;
    }

    /**
     * @param TrangThai the TrangThai to set
     */
    public void setTrangThai(int TrangThai) {
        this.TrangThai = TrangThai;
    }

    /**
     * @return the MaPhieuKho
     */
    public String getMaPhieuKho() {
        return MaPhieuKho;
    }

    /**
     * @param MaPhieuKho the MaPhieuKho to set
     */
    public void setMaPhieuKho(String MaPhieuKho) {
        this.MaPhieuKho = MaPhieuKho;
    }
        /**
     * @return the TienDaTra
     */
    public Double getTienDaTra() {
        return TienDaTra;
    }

    /**
     * @param TienDaTra the TienDaTra to set
     */
    public void setTienDaTra(Double TienDaTra) {
        this.TienDaTra = TienDaTra;
    }
}
