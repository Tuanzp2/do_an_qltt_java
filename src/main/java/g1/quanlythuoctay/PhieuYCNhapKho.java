/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

/**
 *
 * @author ADMIN
 */
public class PhieuYCNhapKho {
    private String id_MaYC;
    private int trangThai;
    private String ngayLap;
    private String note;
    private String maNV;
    private String nameNV;
    public static String TBL_PYCNHAPKHO="PhieuYeuCauNhapHang";
    public static String Ma_Phieu="MaDonHangNhap";
    public static String trang_thai="TrangThai";
    public static String ghiChu="GhiChu";
    public static String id_NV="MaNV";
    public static String date="Ngay";

    public PhieuYCNhapKho() {
    }

    public PhieuYCNhapKho(String id_MaYC, int trangThai, String ngayLap, String note, String maNV) {
        this.id_MaYC = id_MaYC;
        this.trangThai = trangThai;
        this.ngayLap = ngayLap;
        this.note = note;
        this.maNV = maNV;
    }
    
    /**
     * @return the id_MaYC
     */
    public String getId_MaYC() {
        return id_MaYC;
    }

    /**
     * @param id_MaYC the id_MaYC to set
     */
    public void setId_MaYC(String id_MaYC) {
        this.id_MaYC = id_MaYC;
    }

    /**
     * @return the trangThai
     */
    public int getTrangThai() {
        return trangThai;
    }

    /**
     * @param trangThai the trangThai to set
     */
    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    /**
     * @return the ngayLap
     */
    public String getNgayLap() {
        return ngayLap;
    }

    /**
     * @param ngayLap the ngayLap to set
     */
    public void setNgayLap(String ngayLap) {
        this.ngayLap = ngayLap;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the maNV
     */
    public String getMaNV() {
        return maNV;
    }

    /**
     * @param maNV the maNV to set
     */
    public void setMaNV(String maNV) {
        this.maNV = maNV;
    }
    
    /**
     * @return the nameNV
     */
    public String getNameNV() {
        return nameNV;
    }

    /**
     * @param nameNV the nameNV to set
     */
    public void setNameNV(String nameNV) {
        this.nameNV = nameNV;
    }
}
